var index =
[
    [ "Purpose", "index.html#autotoc_md66", null ],
    [ "Installation:", "index.html#autotoc_md67", null ],
    [ "Getting Started", "index.html#autotoc_md68", null ],
    [ "Citing", "index.html#autotoc_md72", null ],
    [ "Contributing", "index.html#autotoc_md73", null ],
    [ "Developer Infrastructure Notes", "infrastructure.html", null ],
    [ "Development Style Guide", "muqstyle.html", [
      [ "C++ style and naming conventions", "muqstyle.html#styleconventions", null ],
      [ "Documentation", "muqstyle.html#documentation", null ]
    ] ]
];