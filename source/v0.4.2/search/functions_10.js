var searchData=
[
  ['qfun_4781',['QFun',['../classmuq_1_1SamplingAlgorithms_1_1DRKernel.html#a0d427a5679fdc9aa8662dea9e8a896fb',1,'muq::SamplingAlgorithms::DRKernel']]],
  ['qoi_4782',['QOI',['../classmuq_1_1SamplingAlgorithms_1_1AbstractSamplingProblem.html#a12cecdea87e30cbbfdf5bc5a63b9666e',1,'muq::SamplingAlgorithms::AbstractSamplingProblem::QOI()'],['../classmuq_1_1SamplingAlgorithms_1_1ParallelAbstractSamplingProblem.html#a08a6ce4116dcc1e7e257840df226291d',1,'muq::SamplingAlgorithms::ParallelAbstractSamplingProblem::QOI()'],['../classmuq_1_1SamplingAlgorithms_1_1SamplingProblem.html#af5a8d26b0e188e445879ce2c4b89ee16',1,'muq::SamplingAlgorithms::SamplingProblem::QOI()']]],
  ['quadpts_4783',['QuadPts',['../classmuq_1_1Approximation_1_1PCEFactory.html#a83119fc46932c690feacdc367d5de03f',1,'muq::Approximation::PCEFactory::QuadPts() const'],['../classmuq_1_1Approximation_1_1PCEFactory.html#ab442ab7e200b49b262582f617345163a',1,'muq::Approximation::PCEFactory::QuadPts(std::shared_ptr&lt; muq::Utilities::MultiIndex &gt; const &amp;quadOrders)']]],
  ['quadrature_4784',['Quadrature',['../classmuq_1_1Approximation_1_1Quadrature.html#a7f8e2cb7a145771a85a4d7024b1f2d28',1,'muq::Approximation::Quadrature']]],
  ['quadraturewrapper_4785',['QuadratureWrapper',['../namespacemuq_1_1Approximation_1_1PythonBindings.html#a19e7ef2a252821ff1559286d064a73ef',1,'muq::Approximation::PythonBindings']]],
  ['query_4786',['Query',['../classmuq_1_1SamplingAlgorithms_1_1PhonebookClient.html#ae2d68ee15f257a1d7d22a606b4b5a305',1,'muq::SamplingAlgorithms::PhonebookClient']]],
  ['query_4787',['query',['../structmuq_1_1Modeling_1_1DynamicKDTreeAdaptor.html#ad4515760da292dc19f7930b725e610d2',1,'muq::Modeling::DynamicKDTreeAdaptor']]]
];
