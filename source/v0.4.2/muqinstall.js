var muqinstall =
[
    [ "Getting MUQ", "muqinstall.html#autotoc_md1", [
      [ "Conda", "muqinstall.html#autotoc_md2", null ]
    ] ],
    [ "Linking against MUQ in c++", "muqinstall.html#autotoc_md3", null ],
    [ "Using MUQ with Docker", "docker.html", [
      [ "Docker", "docker.html#autotoc_md0", null ]
    ] ],
    [ "Building from source", "source_install.html", [
      [ "Building from source", "source_install.html#autotoc_md78", [
        [ "Interested in forward UQ?", "index.html#autotoc_md69", null ],
        [ "Want to tackle Bayesian inverse problems?", "index.html#autotoc_md70", null ],
        [ "Getting Connected", "index.html#autotoc_md71", null ],
        [ "Want to help develop MUQ?", "index.html#autotoc_md74", null ],
        [ "Find a bug?", "index.html#autotoc_md75", null ],
        [ "Want a new feature?", "index.html#autotoc_md76", null ],
        [ "Developer Information", "index.html#autotoc_md77", [
          [ "Git", "infrastructure.html#autotoc_md4", null ],
          [ "Testing", "infrastructure.html#autotoc_md5", null ],
          [ "CI", "infrastructure.html#autotoc_md6", null ],
          [ "Website", "infrastructure.html#autotoc_md7", null ],
          [ "Doxygen", "infrastructure.html#autotoc_md8", null ],
          [ "Docker (in progress)", "infrastructure.html#autotoc_md9", null ],
          [ "Conda", "infrastructure.html#autotoc_md10", null ]
        ] ],
        [ "QuickStart", "source_install.html#autotoc_md79", null ],
        [ "Configuring the Build", "source_install.html#autotoc_md80", [
          [ "Handling Dependencies", "source_install.html#autotoc_md81", null ],
          [ "Example CMake configurations:", "source_install.html#autotoc_md82", null ],
          [ "Compile Groups", "source_install.html#autotoc_md83", null ]
        ] ],
        [ "Compiling", "source_install.html#autotoc_md84", null ],
        [ "Testing", "source_install.html#autotoc_md85", null ]
      ] ]
    ] ]
];