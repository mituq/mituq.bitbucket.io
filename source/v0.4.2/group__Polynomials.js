var group__Polynomials =
[
    [ "BasisExpansion", "classmuq_1_1Approximation_1_1BasisExpansion.html", [
      [ "BasisExpansion", "classmuq_1_1Approximation_1_1BasisExpansion.html#a67d60ba792844e7256ac1dc0b5cca0bc", null ],
      [ "BasisExpansion", "classmuq_1_1Approximation_1_1BasisExpansion.html#a35eec13d2eb6deeaf2fb27a7e92b7e2e", null ],
      [ "BasisExpansion", "classmuq_1_1Approximation_1_1BasisExpansion.html#a871367a89d1a1e5fc970aa05a7d37f75", null ],
      [ "~BasisExpansion", "classmuq_1_1Approximation_1_1BasisExpansion.html#ac6e389a9ee017324551fabf297dc9e64", null ],
      [ "BuildDerivMatrix", "classmuq_1_1Approximation_1_1BasisExpansion.html#a1b7656e19d9511077a71104bc2aeacf4", null ],
      [ "BuildVandermonde", "classmuq_1_1Approximation_1_1BasisExpansion.html#a32c81ce2f7d15df446264be7fa3de285", null ],
      [ "EvaluateImpl", "classmuq_1_1Approximation_1_1BasisExpansion.html#abeeab0e23ac2e9d62f93ba01ca3ec6e8", null ],
      [ "FromHDF5", "classmuq_1_1Approximation_1_1BasisExpansion.html#a2b32121a7640d29a0ec327504224ba1d", null ],
      [ "FromHDF5", "classmuq_1_1Approximation_1_1BasisExpansion.html#a29ae7c5d8707c0a9e7d15bb688f3d13b", null ],
      [ "GetAllDerivs", "classmuq_1_1Approximation_1_1BasisExpansion.html#a8b550bd4f97f12bfb17756dfe3e15f87", null ],
      [ "GetAllTerms", "classmuq_1_1Approximation_1_1BasisExpansion.html#a0e45268dd91663d913c34b6376bd4688", null ],
      [ "GetCoeffs", "classmuq_1_1Approximation_1_1BasisExpansion.html#a696f415ad1807b48dac1ee8abeb809bc", null ],
      [ "GetHessians", "classmuq_1_1Approximation_1_1BasisExpansion.html#a041e614ed0a75f6822bd610d4efc8d6b", null ],
      [ "GetInputSizes", "classmuq_1_1Approximation_1_1BasisExpansion.html#aac65ba1daabab637f14c3909127b35d5", null ],
      [ "GetOutputSizes", "classmuq_1_1Approximation_1_1BasisExpansion.html#a42ccbed90b86ed4a1eb9f397c629a0d8", null ],
      [ "JacobianImpl", "classmuq_1_1Approximation_1_1BasisExpansion.html#a1694f848aa53dbcd9ab49c1795956e53", null ],
      [ "Multis", "classmuq_1_1Approximation_1_1BasisExpansion.html#a4212ac9147c724650a397595af9d9c80", null ],
      [ "NumTerms", "classmuq_1_1Approximation_1_1BasisExpansion.html#a3f0914b862aa73b0ac9a88b14cab8cd9", null ],
      [ "ProcessCoeffs", "classmuq_1_1Approximation_1_1BasisExpansion.html#a08837fc9fe3b135d3d058d0d0a597594", null ],
      [ "SecondDerivative", "classmuq_1_1Approximation_1_1BasisExpansion.html#a5b997c379548da09568a6703d59b491e", null ],
      [ "SecondDerivative", "classmuq_1_1Approximation_1_1BasisExpansion.html#a998fa238ef9a2b4168e5b30b01c16663", null ],
      [ "SetCoeffs", "classmuq_1_1Approximation_1_1BasisExpansion.html#add94fc066aa2affc471808a7cdabb19d", null ],
      [ "ToHDF5", "classmuq_1_1Approximation_1_1BasisExpansion.html#a2e6c73cab32a8890362f6f9ddb5cd348", null ],
      [ "ToHDF5", "classmuq_1_1Approximation_1_1BasisExpansion.html#afb8aca5c09d8a9a10a0035809335665d", null ],
      [ "MonotoneExpansion", "classmuq_1_1Approximation_1_1BasisExpansion.html#a179ce409a4b204b1e292caeabdbb2c8e", null ],
      [ "basisComps", "classmuq_1_1Approximation_1_1BasisExpansion.html#ab65a3e503d051718bc340415ec78fbab", null ],
      [ "coeffs", "classmuq_1_1Approximation_1_1BasisExpansion.html#a2e6ad6ba538919c746faedb47d32173e", null ],
      [ "multis", "classmuq_1_1Approximation_1_1BasisExpansion.html#a298b214451c43757a067d5a18f1cbc81", null ]
    ] ],
    [ "HermiteFunction", "classmuq_1_1Approximation_1_1HermiteFunction.html", [
      [ "HermiteFunction", "classmuq_1_1Approximation_1_1HermiteFunction.html#aaa336c6ba98e03c2ed22596ddf921890", null ],
      [ "~HermiteFunction", "classmuq_1_1Approximation_1_1HermiteFunction.html#a1076c78dceb84272e752b1e70a7f4fde", null ],
      [ "BasisEvaluate", "classmuq_1_1Approximation_1_1HermiteFunction.html#a30abbbe910975bd2aa584fe790d58c4d", null ],
      [ "DerivativeEvaluate", "classmuq_1_1Approximation_1_1HermiteFunction.html#adb89d8e6124a9762a06838352e3b8a56", null ],
      [ "nChoosek", "classmuq_1_1Approximation_1_1HermiteFunction.html#acd49ea25788afabdf55c12b0248581b1", null ],
      [ "polyBase", "classmuq_1_1Approximation_1_1HermiteFunction.html#ab49d6f92f25d3332c805dc13d4a9fb11", null ]
    ] ],
    [ "IndexScalarBasis", "classIndexScalarBasis.html", null ],
    [ "Jacobi", "classmuq_1_1Approximation_1_1Jacobi.html", [
      [ "Jacobi", "classmuq_1_1Approximation_1_1Jacobi.html#ade41febc6ffbfdd063d056b0bc0a0f37", null ],
      [ "~Jacobi", "classmuq_1_1Approximation_1_1Jacobi.html#a2c8c7b3dfffe1cd5a693984801bc56f8", null ],
      [ "ak", "classmuq_1_1Approximation_1_1Jacobi.html#a5934c34724197281a893c8b0c3b8b697", null ],
      [ "bk", "classmuq_1_1Approximation_1_1Jacobi.html#acd0b064237cedd83dd71555b59875a13", null ],
      [ "ck", "classmuq_1_1Approximation_1_1Jacobi.html#a52b94304f69e7f459d842f89b4bf4eb3", null ],
      [ "DerivativeEvaluate", "classmuq_1_1Approximation_1_1Jacobi.html#ad6f429ea57d54f277399f8bb1a42ac06", null ],
      [ "Normalization", "classmuq_1_1Approximation_1_1Jacobi.html#abee295b16fc68b8750ee29e2e9a15080", null ],
      [ "phi0", "classmuq_1_1Approximation_1_1Jacobi.html#aa36c75524f99f1b571fcee226376d9a6", null ],
      [ "phi1", "classmuq_1_1Approximation_1_1Jacobi.html#a728290bbfeb71b5783e7f19149d72248", null ],
      [ "a", "classmuq_1_1Approximation_1_1Jacobi.html#ae1ccade82c3019f09dfda385971b9f8c", null ],
      [ "b", "classmuq_1_1Approximation_1_1Jacobi.html#aef89069555e6d6cbfa1ed121a1d89a03", null ]
    ] ],
    [ "Laguerre", "classmuq_1_1Approximation_1_1Laguerre.html", [
      [ "Laguerre", "classmuq_1_1Approximation_1_1Laguerre.html#a91553def5889a7448aa4914be28db027", null ],
      [ "~Laguerre", "classmuq_1_1Approximation_1_1Laguerre.html#acd1471f3af82e055b6af388891a480e5", null ],
      [ "ak", "classmuq_1_1Approximation_1_1Laguerre.html#a000da6502f8864f573386c5e0da6504c", null ],
      [ "bk", "classmuq_1_1Approximation_1_1Laguerre.html#a1e313425dff209f627547b24206c8e02", null ],
      [ "ck", "classmuq_1_1Approximation_1_1Laguerre.html#abd7db143b5400110430c5d5d4a014991", null ],
      [ "DerivativeEvaluate", "classmuq_1_1Approximation_1_1Laguerre.html#afce3e6e91274c7f221e10ac644034f92", null ],
      [ "Normalization", "classmuq_1_1Approximation_1_1Laguerre.html#a9895a77a2e195ab67c99caa330a4330b", null ],
      [ "phi0", "classmuq_1_1Approximation_1_1Laguerre.html#ab1d55e6c9e9195b52051b403994d79f1", null ],
      [ "phi1", "classmuq_1_1Approximation_1_1Laguerre.html#a5a9e0a62c4e2aa8952f68bed23704cdd", null ],
      [ "a", "classmuq_1_1Approximation_1_1Laguerre.html#a56cf84548286ee959711191487c67b9b", null ]
    ] ],
    [ "Legendre", "classmuq_1_1Approximation_1_1Legendre.html", [
      [ "Legendre", "classmuq_1_1Approximation_1_1Legendre.html#ad737127b539de70105d61faa44af5697", null ],
      [ "~Legendre", "classmuq_1_1Approximation_1_1Legendre.html#a283bc361c6329ba311521476226e15cc", null ],
      [ "ak", "classmuq_1_1Approximation_1_1Legendre.html#a0e6c64271bbe5637835741cd5d5cafa7", null ],
      [ "bk", "classmuq_1_1Approximation_1_1Legendre.html#a3a28ae43bb81f91a7f88d6d47c3f1fed", null ],
      [ "ck", "classmuq_1_1Approximation_1_1Legendre.html#a726e4586f946ca77559f446ad0c99dbc", null ],
      [ "DerivativeEvaluate", "classmuq_1_1Approximation_1_1Legendre.html#a1ee70dc4727fccc9b3f723f13b09bbdd", null ],
      [ "Normalization", "classmuq_1_1Approximation_1_1Legendre.html#a894c15f48a6da94cb6d1aba595469003", null ],
      [ "phi0", "classmuq_1_1Approximation_1_1Legendre.html#ad721bd4f564415fbd5617c46e2d57c53", null ],
      [ "phi1", "classmuq_1_1Approximation_1_1Legendre.html#a38dd3f91569de9452b22b479266b4fd8", null ]
    ] ],
    [ "Monomial", "classmuq_1_1Approximation_1_1Monomial.html", [
      [ "Monomial", "classmuq_1_1Approximation_1_1Monomial.html#ac80d5b20f63ff48cfbf4d67cacacf4f7", null ],
      [ "~Monomial", "classmuq_1_1Approximation_1_1Monomial.html#a7c56cfe0292638732d60169d960f877c", null ],
      [ "BasisEvaluate", "classmuq_1_1Approximation_1_1Monomial.html#ad7c995a9212a72321c76da1d4815d4b3", null ],
      [ "DerivativeEvaluate", "classmuq_1_1Approximation_1_1Monomial.html#aa888b3f64cb515e4c1d4c576652e4588", null ]
    ] ],
    [ "OrthogonalPolynomial", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html", [
      [ "OrthogonalPolynomial", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#a8d47b8f8de33cce00a02fdae0175de53", null ],
      [ "~OrthogonalPolynomial", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#a4f5e484fbbfa327a7f03cc65bd1c89d2", null ],
      [ "ak", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#ac97334abfa190bff11e246bdb7f82d22", null ],
      [ "BasisEvaluate", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#a554a22cb68cd8bfd274fb0e6717a2ca6", null ],
      [ "bk", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#adf6afa52ee10978cbcd8f79ef8dc3cde", null ],
      [ "ck", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#ad8e97b8b3a74b4b030c2ea3443f708ba", null ],
      [ "Construct", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#ab151439f9b99804b71bba1480774aad6", null ],
      [ "EvaluateAllTerms", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#aeb86d0c3c1570aaf13d9766c3a8ad53b", null ],
      [ "Normalization", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#a1e5b8ed5ab13588618b4dc9781f9c8da", null ],
      [ "phi0", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#ab4124c46fc5438930d4ecf6c50fc92c9", null ],
      [ "phi1", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#aa90cfcd85657fa0556fefb982e079072", null ],
      [ "GaussQuadrature", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#ab9e06076a96d3d2518f60fa0184260ca", null ]
    ] ],
    [ "PhysicistHermite", "classPhysicistHermite.html", null ],
    [ "ProbabilistHermite", "classProbabilistHermite.html", null ]
];