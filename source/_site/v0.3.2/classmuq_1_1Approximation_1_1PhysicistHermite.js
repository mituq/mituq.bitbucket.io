var classmuq_1_1Approximation_1_1PhysicistHermite =
[
    [ "PhysicistHermite", "classmuq_1_1Approximation_1_1PhysicistHermite.html#a46419a8fffd72dee246aaf800d525e35", null ],
    [ "~PhysicistHermite", "classmuq_1_1Approximation_1_1PhysicistHermite.html#a33860341d70283aef1da044f9c2eebbd", null ],
    [ "ak", "classmuq_1_1Approximation_1_1PhysicistHermite.html#a2ae5fa5197c0ec880a93eec54ed02590", null ],
    [ "bk", "classmuq_1_1Approximation_1_1PhysicistHermite.html#adb8e0e737597d327279c5594be2254cb", null ],
    [ "ck", "classmuq_1_1Approximation_1_1PhysicistHermite.html#adf5478410eae9a5a1ac246165f101c66", null ],
    [ "DerivativeEvaluate", "classmuq_1_1Approximation_1_1PhysicistHermite.html#a261db3cab8ae47ad4f111e256b32cf52", null ],
    [ "Normalization", "classmuq_1_1Approximation_1_1PhysicistHermite.html#aee273d514691fa06ca0fd23ac547ea45", null ],
    [ "phi0", "classmuq_1_1Approximation_1_1PhysicistHermite.html#ab7f9d2358cd816d356d9f335ab35e1db", null ],
    [ "phi1", "classmuq_1_1Approximation_1_1PhysicistHermite.html#aa2da77033d47d772016b25c7f2e3c580", null ]
];