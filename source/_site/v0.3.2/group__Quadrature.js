var group__Quadrature =
[
    [ "GaussQuadrature", "classmuq_1_1Approximation_1_1GaussQuadrature.html", [
      [ "GaussQuadrature", "classmuq_1_1Approximation_1_1GaussQuadrature.html#a21659bdf86aeef67ffdfcbda88595f2f", null ],
      [ "~GaussQuadrature", "classmuq_1_1Approximation_1_1GaussQuadrature.html#af5f30363ca5aa0fbc5831f1d40c5ee85", null ],
      [ "GaussQuadrature", "classmuq_1_1Approximation_1_1GaussQuadrature.html#a46f14c301907e6b49292b08d09d8ef6e", null ],
      [ "GaussQuadrature", "classmuq_1_1Approximation_1_1GaussQuadrature.html#a67575d92062014452bd42008ca6e26a0", null ],
      [ "Compute", "classmuq_1_1Approximation_1_1GaussQuadrature.html#a0e5d9316044cecabf5d99cb05b447daf", null ],
      [ "Exactness", "classmuq_1_1Approximation_1_1GaussQuadrature.html#ad0765f5969d143f69d15beaaf8346f3e", null ],
      [ "poly", "classmuq_1_1Approximation_1_1GaussQuadrature.html#ab661f148996b46ccdecfdf86aecb7fdc", null ],
      [ "polyOrder", "classmuq_1_1Approximation_1_1GaussQuadrature.html#a17a54d74f183c955186f19b186982f26", null ]
    ] ],
    [ "ClenshawCurtisQuadrature", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html", [
      [ "ClenshawCurtisQuadrature", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#ac99c242b906ae524e3a5d248edb7a66c", null ],
      [ "~ClenshawCurtisQuadrature", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#ab27f2057cd2d6245b100d7172492a63c", null ],
      [ "Compute", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#aea592b9e9e22c3c2678a408be9a1be01", null ],
      [ "Exactness", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#aa81424e20f0fa966d9c5ecec7d7170b1", null ],
      [ "IndexToNumPoints", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#ace4baa4e341133fd5f09731f63d7a10c", null ],
      [ "nested", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#a969b297a57452b272ef3e5d626aa26dd", null ],
      [ "pi", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#af427222dc1d8e3d0d814a233dc33e968", null ]
    ] ],
    [ "ExponentialGrowthQuadrature", "classmuq_1_1Approximation_1_1ExponentialGrowthQuadrature.html", [
      [ "ExponentialGrowthQuadrature", "classmuq_1_1Approximation_1_1ExponentialGrowthQuadrature.html#aac25538fc7b7fda01ecf60b652badd47", null ],
      [ "~ExponentialGrowthQuadrature", "classmuq_1_1Approximation_1_1ExponentialGrowthQuadrature.html#a1d08b7d4d2a580d0371b5ef95c1e263c", null ],
      [ "Compute", "classmuq_1_1Approximation_1_1ExponentialGrowthQuadrature.html#a6b957bb7b23c9ed975054b35b22477bc", null ],
      [ "Exactness", "classmuq_1_1Approximation_1_1ExponentialGrowthQuadrature.html#af2b8853768723a9dacedd00e79e9bc8b", null ],
      [ "Points", "classmuq_1_1Approximation_1_1ExponentialGrowthQuadrature.html#a9ef87e2879ebccd404611fcf3ab6ffd2", null ],
      [ "Weights", "classmuq_1_1Approximation_1_1ExponentialGrowthQuadrature.html#a50ddb62d97cf81d088353289cc692c2f", null ],
      [ "otherQuad", "classmuq_1_1Approximation_1_1ExponentialGrowthQuadrature.html#a048970a2542fb6ace7a00d1e619abcba", null ]
    ] ],
    [ "FullTensorQuadrature", "classmuq_1_1Approximation_1_1FullTensorQuadrature.html", [
      [ "FullTensorQuadrature", "classmuq_1_1Approximation_1_1FullTensorQuadrature.html#adf7e34d1546ba3f66812aa3dd8b67c8e", null ],
      [ "FullTensorQuadrature", "classmuq_1_1Approximation_1_1FullTensorQuadrature.html#a6c9b8dc07c00b6ff96ac5518d95af43e", null ],
      [ "FullTensorQuadrature", "classmuq_1_1Approximation_1_1FullTensorQuadrature.html#a72bc8354bff3c20e20599279850113e1", null ],
      [ "~FullTensorQuadrature", "classmuq_1_1Approximation_1_1FullTensorQuadrature.html#a1ea0acd9e3001153b6d8fcf0703dfabb", null ],
      [ "Compute", "classmuq_1_1Approximation_1_1FullTensorQuadrature.html#aecb5765fadcb8a91b389e83cdc3c22dc", null ],
      [ "Compute", "classmuq_1_1Approximation_1_1FullTensorQuadrature.html#a1b1e9a89c3d5ab434d5458bef20ea332", null ],
      [ "Exactness", "classmuq_1_1Approximation_1_1FullTensorQuadrature.html#a43b5c1a48c320397d5a419eaff885ee8", null ],
      [ "rules", "classmuq_1_1Approximation_1_1FullTensorQuadrature.html#a96cbb4a97078bc291f84dc8921eef1d5", null ]
    ] ],
    [ "GaussPattersonQuadrature", "classmuq_1_1Approximation_1_1GaussPattersonQuadrature.html", [
      [ "GaussPattersonQuadrature", "classmuq_1_1Approximation_1_1GaussPattersonQuadrature.html#a0ed0ab4f77c7c14a5a5c91eda0a4fbdc", null ],
      [ "~GaussPattersonQuadrature", "classmuq_1_1Approximation_1_1GaussPattersonQuadrature.html#a3422f533beabe10d7f04b568088beab2", null ],
      [ "Compute", "classmuq_1_1Approximation_1_1GaussPattersonQuadrature.html#abd9ba2e9fd041b6df9c867a6d1dbd5d7", null ],
      [ "Exactness", "classmuq_1_1Approximation_1_1GaussPattersonQuadrature.html#a60306a142981eeb8e3075c95387f4129", null ]
    ] ],
    [ "Quadrature", "classmuq_1_1Approximation_1_1Quadrature.html", [
      [ "Quadrature", "classmuq_1_1Approximation_1_1Quadrature.html#a7f8e2cb7a145771a85a4d7024b1f2d28", null ],
      [ "~Quadrature", "classmuq_1_1Approximation_1_1Quadrature.html#af1304d3f79d8508baab90d22d649b762", null ],
      [ "Compute", "classmuq_1_1Approximation_1_1Quadrature.html#a34ac5ea0851782f83bed2101096958b4", null ],
      [ "Compute", "classmuq_1_1Approximation_1_1Quadrature.html#a9896973503a2b692016d0cc014faea25", null ],
      [ "Dim", "classmuq_1_1Approximation_1_1Quadrature.html#a943068b7e3ee0ae66d8e7e09d1e016db", null ],
      [ "Exactness", "classmuq_1_1Approximation_1_1Quadrature.html#a3ea8c6a294f23b81b3bc8e20cb131530", null ],
      [ "Points", "classmuq_1_1Approximation_1_1Quadrature.html#a570d33e930d3e173dc1bdffff959663f", null ],
      [ "Weights", "classmuq_1_1Approximation_1_1Quadrature.html#a8d7871843470b3d4944bc6e4c69f61ad", null ],
      [ "dim", "classmuq_1_1Approximation_1_1Quadrature.html#a3e3498b469d800bfdeff115059768877", null ],
      [ "pts", "classmuq_1_1Approximation_1_1Quadrature.html#a9c584565e56ccf89bb1854dffc5880c5", null ],
      [ "wts", "classmuq_1_1Approximation_1_1Quadrature.html#adadc54d61f54d97868d38845cb1011bb", null ]
    ] ],
    [ "SmolyakQuadrature", "classmuq_1_1Approximation_1_1SmolyakQuadrature.html", [
      [ "SmolyakQuadrature", "classmuq_1_1Approximation_1_1SmolyakQuadrature.html#a45dab9e0cb7c4c54ca3009c62d8c8e4f", null ],
      [ "SmolyakQuadrature", "classmuq_1_1Approximation_1_1SmolyakQuadrature.html#aa8be8463826b985994fc57b44e537ed6", null ],
      [ "~SmolyakQuadrature", "classmuq_1_1Approximation_1_1SmolyakQuadrature.html#abcf5606db5c43151c6eee978675c8ca9", null ],
      [ "BuildMultis", "classmuq_1_1Approximation_1_1SmolyakQuadrature.html#ae7b52691ea890e9833f5c8d7d4f99f84", null ],
      [ "Compute", "classmuq_1_1Approximation_1_1SmolyakQuadrature.html#a8b7d3fe326132f6ff526c42f9bea7e02", null ],
      [ "Compute", "classmuq_1_1Approximation_1_1SmolyakQuadrature.html#a524e03cfff9441e9a16791286d482945", null ],
      [ "Compute", "classmuq_1_1Approximation_1_1SmolyakQuadrature.html#a2897953b8063d2a33a9ec6d7567bd791", null ],
      [ "ComputeWeights", "classmuq_1_1Approximation_1_1SmolyakQuadrature.html#ae2cb61b984a88a4026c2f6b7f462f811", null ],
      [ "UpdateWeights", "classmuq_1_1Approximation_1_1SmolyakQuadrature.html#a718c19d84af8a67b68be61ed0b91c9e6", null ],
      [ "scalarRules", "classmuq_1_1Approximation_1_1SmolyakQuadrature.html#a0e64978f269c648355d54a217187fb76", null ]
    ] ]
];