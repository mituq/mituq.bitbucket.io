/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "MUQ", "index.html", [
    [ "MUQ: MIT Uncertainty Quantification Library", "index.html", null ],
    [ "Installation", "muqinstall.html", "muqinstall" ],
    [ "Development Style Guide", "muqstyle.html", [
      [ "C++ style and naming conventions", "muqstyle.html#styleconventions", null ],
      [ "Documentation", "muqstyle.html#documentation", null ]
    ] ],
    [ "MUQ Infrastructure", "md__Users_rdcrlmdp_Documents_Repositories_muq-all_muq2_documentation_infrastructure.html", null ],
    [ "paper", "md__Users_rdcrlmdp_Documents_Repositories_muq-all_muq2_joss_paper.html", null ],
    [ "Descriptions", "md__Users_rdcrlmdp_Documents_Repositories_muq-all_muq2_SupportScripts_docker_Descriptions.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"AMProposal_8cpp.html",
"DelayedRejection_2python_2EllipticInference_8py.html#a6dd8c4e5e315ca31ced57507f2c7db1e",
"KernelBase_8h.html",
"OTF2Tracer_8h.html#ae8643a321356f52a41b83005b8c8e2dbaa39476daf539e1b99312f3e376b0ce89",
"SamplingAlgorithms_2python_2AllClassWrappers_8h.html#a7d2e03a068f0d97f0f6cff236b516c62",
"classCustomModPiece_1_1DiffusionEquation.html#aefabc384f821bc9db3e94e543d61a098",
"classmuq_1_1Approximation_1_1IndexedScalarBasis.html",
"classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a16ce8da16f0d46dad645df5d947cdcf9",
"classmuq_1_1Modeling_1_1AnyAlgebra.html#a4eec46c313468b760d3afce59e13443e",
"classmuq_1_1Modeling_1_1FlannCache.html#a8248b86e538df230b4bf322edd4bf519",
"classmuq_1_1Modeling_1_1ModPiece.html#a20f12e7031aaf4a8e9a89ccd5e68323f",
"classmuq_1_1Modeling_1_1SumOperator.html#a3836c72cd684f9f723f71836af05676e",
"classmuq_1_1Optimization_1_1Optimizer.html#a8fa1f70270fe51ace4f69b90b8443c8a",
"classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a8480ff2149b8517855f42ad14837d876",
"classmuq_1_1SamplingAlgorithms_1_1SamplingStateIdentity.html#a64dd903b31485b98dc0f23435713f754",
"classmuq_1_1Utilities_1_1MultiIndexSet.html#a171a5266e74871e6cbd00724a17ced23",
"dir_e05d7e2b1ecd646af5bb94391405f3b5.html",
"namespacemembers_d.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';