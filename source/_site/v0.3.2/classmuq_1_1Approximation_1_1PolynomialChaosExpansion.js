var classmuq_1_1Approximation_1_1PolynomialChaosExpansion =
[
    [ "PolynomialChaosExpansion", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a8cffa5c17b4228c1532d7943e2cf3bf7", null ],
    [ "PolynomialChaosExpansion", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a54c8594d2fa60b3f9f3059dd22424fad", null ],
    [ "PolynomialChaosExpansion", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a16ce8da16f0d46dad645df5d947cdcf9", null ],
    [ "PolynomialChaosExpansion", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#add4aef0e6622e49684f4157c9d86da03", null ],
    [ "~PolynomialChaosExpansion", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a70ebcb7c88aa4ba82130ba242a34c48b", null ],
    [ "ComputeWeightedSum", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a6084acd607a09a226084fa62516f75d7", null ],
    [ "ComputeWeightedSum", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a87c5b7305a87ec2fab5ac34e3c280ff4", null ],
    [ "Covariance", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a26817c4d532d929364d7cca5717814f0", null ],
    [ "GetNormalizationVec", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a4d6127163c6ea5cf63ebd6d5db691074", null ],
    [ "Magnitude", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a5dae033f31fbd36257cc2f97f104c7f4", null ],
    [ "MainSensitivity", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a99cd6eef4cbf7befc6b4a512a2a7f983", null ],
    [ "Mean", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a4ca3a54c8b13cf0df6571c80f9ca9b25", null ],
    [ "SobolSensitivity", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#aee652757aa909547ab4a703117ed3510", null ],
    [ "SobolSensitivity", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#ae8f666ae7bedf27d0afb3a501386ff43", null ],
    [ "TotalSensitivity", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a22fb08e61dfcd015050b4bb5564e6dc3", null ],
    [ "TotalSensitivity", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a6d253f164b98a69dff35aeb21042e0b7", null ],
    [ "Variance", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a582d6790abe64fe8c3dc24ba5728c8f6", null ],
    [ "PCEFactory", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#affa6fa43ae16a03bc332c85b42108e54", null ]
];