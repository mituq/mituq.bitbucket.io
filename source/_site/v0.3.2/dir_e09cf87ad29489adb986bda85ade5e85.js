var dir_e09cf87ad29489adb986bda85ade5e85 =
[
    [ "LocalRegression.h", "LocalRegression_8h.html", [
      [ "LocalRegression", "classmuq_1_1Approximation_1_1LocalRegression.html", "classmuq_1_1Approximation_1_1LocalRegression" ]
    ] ],
    [ "Regression.h", "Regression_8h.html", [
      [ "Regression", "classmuq_1_1Approximation_1_1Regression.html", "classmuq_1_1Approximation_1_1Regression" ],
      [ "PoisednessCost", "classmuq_1_1Approximation_1_1Regression_1_1PoisednessCost.html", "classmuq_1_1Approximation_1_1Regression_1_1PoisednessCost" ],
      [ "PoisednessConstraint", "classmuq_1_1Approximation_1_1Regression_1_1PoisednessConstraint.html", "classmuq_1_1Approximation_1_1Regression_1_1PoisednessConstraint" ]
    ] ]
];