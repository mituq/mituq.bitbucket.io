var classmuq_1_1Approximation_1_1OrthogonalPolynomial =
[
    [ "OrthogonalPolynomial", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#a8d47b8f8de33cce00a02fdae0175de53", null ],
    [ "~OrthogonalPolynomial", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#a4f5e484fbbfa327a7f03cc65bd1c89d2", null ],
    [ "ak", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#ac97334abfa190bff11e246bdb7f82d22", null ],
    [ "BasisEvaluate", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#a554a22cb68cd8bfd274fb0e6717a2ca6", null ],
    [ "bk", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#adf6afa52ee10978cbcd8f79ef8dc3cde", null ],
    [ "ck", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#ad8e97b8b3a74b4b030c2ea3443f708ba", null ],
    [ "Construct", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#ab151439f9b99804b71bba1480774aad6", null ],
    [ "EvaluateAllTerms", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#aeb86d0c3c1570aaf13d9766c3a8ad53b", null ],
    [ "Normalization", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#a1e5b8ed5ab13588618b4dc9781f9c8da", null ],
    [ "phi0", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#ab4124c46fc5438930d4ecf6c50fc92c9", null ],
    [ "phi1", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#aa90cfcd85657fa0556fefb982e079072", null ],
    [ "GaussQuadrature", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html#ab9e06076a96d3d2518f60fa0184260ca", null ]
];