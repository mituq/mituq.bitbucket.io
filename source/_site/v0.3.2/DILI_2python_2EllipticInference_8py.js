var DILI_2python_2EllipticInference_8py =
[
    [ "ax1", "DILI_2python_2EllipticInference_8py.html#a83d40c3e8a16b700bfd5525dda35ed98", null ],
    [ "ax2", "DILI_2python_2EllipticInference_8py.html#aa6c42fed897d355e82fd065af1c8efd1", null ],
    [ "csOpts", "DILI_2python_2EllipticInference_8py.html#a75a89f27784f0b0ea450ecf3b22513bd", null ],
    [ "edgeColor", "DILI_2python_2EllipticInference_8py.html#a2893f2cce1b79ab63d513b343fb15405", null ],
    [ "eigOpts", "DILI_2python_2EllipticInference_8py.html#a3cc6927fc21904676e16570b26ac36c3", null ],
    [ "fig3", "DILI_2python_2EllipticInference_8py.html#ac9ac1322113661ed502e8bf6f28ade8c", null ],
    [ "figsize", "DILI_2python_2EllipticInference_8py.html#ae6c0127adbdc94d6388f20c2255518e0", null ],
    [ "gs", "DILI_2python_2EllipticInference_8py.html#a0a387d604ae6e0f86a3861a047612a9f", null ],
    [ "lisOpts", "DILI_2python_2EllipticInference_8py.html#a6bf214b2c3a5485c8a991bd25d9f0c77", null ],
    [ "lisVals", "DILI_2python_2EllipticInference_8py.html#a006cf2c41cfb460fe57f9dd6cc786b5f", null ],
    [ "lisVecs", "DILI_2python_2EllipticInference_8py.html#aa3622ec3da21c8f2b39e234defb7d770", null ],
    [ "markersize", "DILI_2python_2EllipticInference_8py.html#a222654bfbc28eeb6ab0ca445b29a4e6d", null ],
    [ "nrows", "DILI_2python_2EllipticInference_8py.html#abd80bbd71859d1f4249d97aa181b8d9f", null ],
    [ "obsIndEnd", "DILI_2python_2EllipticInference_8py.html#a4543bfb6a8527db98546197a0c34e5fb", null ],
    [ "obsIndStart", "DILI_2python_2EllipticInference_8py.html#abb188464ac1757062a334ea05d265eea", null ],
    [ "obsMod", "DILI_2python_2EllipticInference_8py.html#a2a83fae177d3390a5e58ab511746f303", null ],
    [ "obsSkip", "DILI_2python_2EllipticInference_8py.html#a51ca8b295e4b877e84a968bf83487106", null ],
    [ "rechargeVal", "DILI_2python_2EllipticInference_8py.html#a6e22366bf7c8d6c950697c0d221983c8", null ],
    [ "xs", "DILI_2python_2EllipticInference_8py.html#ae452a9d9955b956c5e67ffd5f465b4b7", null ]
];