var PlotResults_8py =
[
    [ "fin", "PlotResults_8py.html#a30ad04be718127bfd8f269dfb6cef7f8", null ],
    [ "label", "PlotResults_8py.html#a6b2f7786360558f8db4e6088f738b1f1", null ],
    [ "linewidth", "PlotResults_8py.html#a493b94eebaacfe348fcabedae28d966b", null ],
    [ "markersize", "PlotResults_8py.html#a41c648a3eb93d4219de4bce9c335828b", null ],
    [ "strainData", "PlotResults_8py.html#a46123ce19c92e68b9ae01610fc52816d", null ],
    [ "strainPred", "PlotResults_8py.html#aa74667d4a9569b1bd49b56a68a7e7fa6", null ],
    [ "strainUnits", "PlotResults_8py.html#a440350f1d1d57b5b00d5e03048a2994e", null ],
    [ "stressData", "PlotResults_8py.html#a0182f1f0a57dfa39d68292930a5c5117", null ],
    [ "stressPred", "PlotResults_8py.html#ab6d06f56aa5b908ce46834afacb45af9", null ],
    [ "stressUnits", "PlotResults_8py.html#a792df20c4b74e26bd6b52aef94e12e8a", null ]
];