var muqinstall =
[
    [ "Getting MUQ", "muqinstall.html#autotoc_md1", [
      [ "Conda", "muqinstall.html#autotoc_md2", null ]
    ] ],
    [ "Linking against MUQ in c++", "muqinstall.html#autotoc_md3", null ],
    [ "Using MUQ with Docker", "docker.html", [
      [ "Docker", "docker.html#autotoc_md0", null ]
    ] ],
    [ "Building from source", "source_install.html", [
      [ "Building from source", "source_install.html#autotoc_md49", [
        [ "Interested in forward UQ?", "index.html#autotoc_md40", null ],
        [ "Want to tackle Bayesian inverse problems?", "index.html#autotoc_md41", null ],
        [ "Getting Connected", "index.html#autotoc_md42", null ],
        [ "Want to help develop MUQ?", "index.html#autotoc_md45", null ],
        [ "Find a bug?", "index.html#autotoc_md46", null ],
        [ "Want a new feature?", "index.html#autotoc_md47", null ],
        [ "Developer Information", "index.html#autotoc_md48", [
          [ "Git", "infrastructure.html#autotoc_md4", null ],
          [ "Testing", "infrastructure.html#autotoc_md5", null ],
          [ "CI", "infrastructure.html#autotoc_md6", null ],
          [ "Website", "infrastructure.html#autotoc_md7", null ],
          [ "Doxygen", "infrastructure.html#autotoc_md8", null ],
          [ "Docker (in progress)", "infrastructure.html#autotoc_md9", null ],
          [ "Conda", "infrastructure.html#autotoc_md10", null ]
        ] ],
        [ "QuickStart", "source_install.html#autotoc_md50", null ],
        [ "Configuring the Build", "source_install.html#autotoc_md51", [
          [ "Handling Dependencies", "source_install.html#autotoc_md52", null ],
          [ "Example CMake configurations:", "source_install.html#autotoc_md53", null ],
          [ "Compile Groups", "source_install.html#autotoc_md54", null ]
        ] ],
        [ "Compiling", "source_install.html#autotoc_md55", null ],
        [ "Testing", "source_install.html#autotoc_md56", null ]
      ] ]
    ] ]
];