var group__mcmc =
[
    [ "Getting Started: Defining an MCMC Algorithm", "group__mcmcalg.html", null ],
    [ "Dimension-Independent MCMC", "group__disamp.html", null ],
    [ "Markov Chain Diagnostics", "group__mcmcdiag.html", "group__mcmcdiag" ],
    [ "Multi-Index MCMC", "group__MIMCMC.html", "group__MIMCMC" ],
    [ "MCMC Proposal Distributions", "group__MCMCProposals.html", "group__MCMCProposals" ],
    [ "MCMC Kernels", "group__MCMCKernels.html", "group__MCMCKernels" ]
];