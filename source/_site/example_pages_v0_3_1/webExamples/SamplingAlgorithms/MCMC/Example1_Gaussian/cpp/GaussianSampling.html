<!-- head.hmtl -->
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <title>Simple MCMC | MUQ by </title>
    <meta name="description" content="MCMC sampling from a simple Gaussian distribution. Custom target distributions can be supplied."/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#157878">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Titillium+Web:300' type='text/css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Arimo:400,700' type='text/css'>

    <link rel="stylesheet" href="https://mituq.bitbucket.io/source/_site/assets/css/style.css">
    <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
    extensions: ["tex2jax.js","TeX/AMSmath.js","TeX/AMSsymbols.js"],
    jax: ["input/TeX","output/HTML-CSS"],
    tex: {
      equationNumbers: { autoNumber: "all" },
      tagSide: "right"
    },
    tex2jax: {
      inlineMath: [ ['$','$'], ["\\(","\\)"] ],
      displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
      processEscapes: true
    }
  });
</script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/latest.js?config=TeX-MML-AM_CHTML' async></script>
</head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57389527-4', 'auto');
  ga('send', 'pageview');

</script>


<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #2A3E48;">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
        </button>
        <a class="navbar-brand" href="https://mituq.bitbucket.io/source/_site">MUQ</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          
            <li><a href="https://mituq.bitbucket.io/source/_site/index.html">Home</a></li>
          
            <li><a href="https://mituq.bitbucket.io/source/_site/examples.html">Examples</a></li>
          
          <li><a href="https://mituq.bitbucket.io/source/_site/latest/index.html">Documentation</a></li>
          <li><a href="https://bitbucket.org/mituq/muq2">Git It!</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>

    <section class="page-header">
      <h1 class="project-name">MUQ</h1>
      <h2 class="project-tagline">The MIT Uncertainty Quantification Library</h2>
    </section>


    <div class="container" role="main">
    <div class="row">
    <div class="col-md-9 col-sm-12">

        <h1><small class="text-muted">Example</small> </br> Simple MCMC<h1>
<blockquote class="blockquote"><p class="mb-0">MCMC sampling from a simple Gaussian distribution. Custom target distributions can be supplied.</p></blockquote>
</br>




<h2>Overview</h2>

<p>The goal of this example is to demonstrate the use of MUQ's MCMC stack by sampling
a simple bivariate Gaussian density.  To keep things as simple as possible, we
employ a Metropolis-Hastings transition kernel with a simple random walk proposal.
The idea is to introduce the MUQ MCMC workflow without additional the additional
complexities that come from more challenging target densities or more complicated
MCMC algorithms.</p>

<h3>Background</h3>

<p>Let $x$ denote a random variable taking values in a space $\mathcal{X}$, and let $\pi(x)$ denote the
probability density of $x$.  In many cases, we cannot compute expectations with
respect to $\pi(x)$ analytically, and we need to resort to some sort of numerical
integration.  Typically, such approaches approximate an expectation $\mathbb{E}_x \left[f(x)\right]$,
through some sort of weighted sum that takes the form
$$
\mathbb{E}_x\left[f(x)\right] \approx \sum_{k=1}^K w_k f\left(x^{(k)}\right).
$$
In standard Monte Carlo procedures, the weights are constant $w_k=\frac{1}{K}$ and
points $x^{(k)}$ are independent samples of $\pi(x)$.   However, generating
independent samples is not always possible for general $\pi(x)$.  Markov chain
Monte Carlo is one way to get around this.   The basic idea is to create a sequence
of points (e.g., a chain) that are correlated, but for large $K$, can still be used in a Monte
Carlo approximation.  We further restrict that the chain is Markov, which simply means that $x^{(k)}$
depends only on the previous step in the chain $x^{(k-1)}$.</p>

<h4>Transition Kernels</h4>

<p>The transition from $x^{(k-1)}$ to $x^{(k)}$ is controlled by a probability distribution
over $\mathcal{X}$ called the transition kernel.  This distribution is consturcted
to ensure that the chain can be used to form a Monte Carlo approximation as $K\rightarrow \infty$.
More precisely, the transition kernel is constructed to ensure that the chain forms
a stationary random process with stationary distribution $\pi(x)$ and is ergodic,
which ensures the sample mean will converge to the true expecation almost surely
as $K\rightarrow \infty$.  Note that establishing a central limit theorem and
studying the variance of the MCMC estimator requires additional technical conditions
on the transition kernel.  See <a href=https://www.springer.com/us/book/9780387212395>Robert and Casella, <i>Monte Carlo Statistical Methods</i></a>
for more details.</p>

<h4>Metropolis-Hastings Rule</h4>

<p>While not the only option, the Metropolis-Hastings rule is one of the most common
methods for constructing an appropriate transition kernel.  The idea is to start
with a proposal distribution $q(x | x^{(k-1)})$ and then "correct" the proposal
with an accept/reject step to ensure ergodicity.  The basic procedure is as follows</p>

<ol>
<li><p>Generate a sample $x^\prime$ from the proposal distribution
$$
x^\prime \sim q(x | x^{(k-1)}).
$$</p></li>
<li><p>Compute the acceptance probability $\gamma$
$$
\gamma = \frac{\pi(x^\prime)}{\pi(x^{(k-1)})} \frac{q(x^{(k-1)} | x^\prime)}{q(x^\prime | x^{(k-1)})}.
$$</p></li>
<li><p>Accept the proposed step $x^\prime$ as the next state with probability $\gamma$
$$
x^{(k)} = \left\{ \begin{array}{lll} x^\prime &amp; \text{with probability} &amp; \gamma\\ x^{(k-1)} &amp; \text{with probability} &amp; 1.0-\gamma\end{array}\right\}.
$$</p></li>
</ol>

<h4>Proposal Distributions</h4>

<p>Clearly, the proposal density $q(x | x^{(k-1)})$ is a key component of the Metropolis-Hastings
transition kernel.  Fortunately though, there is incredible flexibility in the
choice of proposal; the Metropolis-Hastings correction step ensures, at least
asymptotically, that the resulting chain will be ergodic.   While not
the most efficient, a common choice of proposal is an isotropic Gaussian distribution
centered at $x^{(k-1)}$.  The resulting algorithm is commonly referred to as
the "Random Walk Metropolis" (RWM) algorithm.  Below, we will use the RWM
algorithm in MUQ to sample a simple bivariate Gaussian target density $\pi(x)$.</p>

<h3>MCMC in MUQ</h3>

<p>The MCMC classes in MUQ are analogous to the mathematical components of an MCMC
algorithm: there is a base class representing the chain, another base class representing
the transition kernel, and for Metropolis-Hastings, a third base class representing
the proposal distribution.  The RWM algorithm can be constructed by combining an
instance of the "SingleChainMCMC" chain class, with an instance of the "MHKernel"
transition kernel class, and an instance of the "RandomWalk" proposal class.  In
later examples, the flexibility of this structure will become clear, as we construct
algorithms of increasing complexity by simply exchanging each of these components
with alternative chains, kernels, and proposals.</p>

<h2>Problem Description</h2>

<p>Use the Random Walk Metropolis algorithm to sample a two-dimensional normal
distribution with mean
$$
\mu = \left[\begin{array}{c} 1.0\\ 2.0\end{array}\right],
$$
and covariance
$$
\Sigma = \left[\begin{array}{cc} 1.0 &amp; 0.8 \\ 0.8 &amp; 1.5 \end{array}\right].
$$</p>



<h2>Implementation</h2>

<p>To sample the Gaussian target, the code needs to do four things:</p>

<ol>
<li><p>Define the target density and set up a sampling problem.</p></li>
<li><p>Construct the RWM algorithm.</p></li>
<li><p>Run the MCMC algorithm.</p></li>
<li><p>Analyze the results.</p></li>
</ol>

<h3>Include statements</h3>

<p>Include the necessary header files from MUQ and elsewhere.  Notice our use of the
<a href=https://www.boost.org/doc/libs/1_65_1/doc/html/property_tree.html>boost::property_tree class</a>.
We use property tree's to pass in algorithm parameters, and even to define the
chain, kernel, and proposal themselves.</p>


<pre class="prettyprint lang-cpp">
#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/Density.h"

#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SingleChainMCMC.h"
#include "MUQ/SamplingAlgorithms/MCMCFactory.h"

#include &lt;boost/property_tree/ptree.hpp&gt;

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;


int main(){
</pre>

<h3>1. Define the target density and set up sampling problem</h3>

<p>MUQ has extensive tools for combining many model compoenents into larger
  more complicated models.  The AbstractSamplingProblem base class and its
  children, like the SamplingProblem class, define the interface between
  sampling algorithms like MCMC and the models and densities they work with.</p>

<p>Here, we create a very simple target density and then construct a SamplingProblem
  directly from the density.</p>



<p>Define the Target Density:</p>


<pre class="prettyprint lang-cpp">
  Eigen::VectorXd mu(2);
  mu &lt;&lt; 1.0, 2.0;

  Eigen::MatrixXd cov(2,2);
  cov &lt;&lt; 1.0, 0.8,
         0.8, 1.5;

  auto targetDensity = std::make_shared&lt;Gaussian&gt;(mu, cov)-&gt;AsDensity(); // standard normal Gaussian
</pre>

<p>Create the Sampling Problem:</p>


<pre class="prettyprint lang-cpp">
  auto problem = std::make_shared&lt;SamplingProblem&gt;(targetDensity);
</pre>

<h3>2. Construct the RWM algorithm</h3>

<p>One of the easiest ways to define an MCMC algorithm is to put all of the algorithm
  parameters, including the kernel and proposal definitions, in a property tree
  and then let MUQ construct each of the algorithm components: chain, kernel, and
  proposal.</p>

<p>The boost property tree will have the following entries:</p>

<ul>
<li>NumSamples : 10000</li>
<li>KernelList "Kernel1"</li>
<li>Kernel1
<ul>
<li>Method : "MHKernel"</li>
<li>Proposal : "MyProposal"</li>
<li>MyProposal
<ul>
<li>Method : "MHProposal"</li>
<li>ProposalVariance : 0.5</li>
</ul></li>
</ul></li>
</ul>

<p>At the base level, we specify the number of steps in the chain with the entry "NumSamples".
  Note that this number includes any burnin samples.   The kernel is then defined
  in the "KernelList" entry.  The value, "Kernel1", specifies a block in the
  property tree with the kernel definition.  In the "Kernel1" block, we set the
  kernel to "MHKernel," which specifies that we want to use the Metropolis-Hastings
  kernel.  We also tell MUQ to use the "MyProposal" block to define the proposal.
  The proposal method is specified as "MHProposal", which is the random walk
  proposal used in the RWM algorithm, and the proposal variance is set to 0.5.</p>


<pre class="prettyprint lang-cpp">
  // parameters for the sampler
  pt::ptree pt;
  pt.put("NumSamples", 1e4); // number of MCMC steps
  pt.put("BurnIn", 1e3);
  pt.put("PrintLevel",3);
  pt.put("KernelList", "Kernel1"); // Name of block that defines the transition kernel
  pt.put("Kernel1.Method","MHKernel");  // Name of the transition kernel class
  pt.put("Kernel1.Proposal", "MyProposal"); // Name of block defining the proposal distribution
  pt.put("Kernel1.MyProposal.Method", "MHProposal"); // Name of proposal class
  pt.put("Kernel1.MyProposal.ProposalVariance", 2.5); // Variance of the isotropic MH proposal
</pre>

<p>Once the algorithm parameters are specified, we can pass them to the CreateSingleChain
  function of the MCMCFactory class to create an instance of the MCMC algorithm we defined in the
  property tree.</p>


<pre class="prettyprint lang-cpp">
  Eigen::VectorXd startPt = mu;
  auto mcmc = MCMCFactory::CreateSingleChain(pt, problem);
</pre>

<h3>3. Run the MCMC algorithm</h3>

<p>We are now ready to run the MCMC algorithm.  Here we start the chain at the
  target densities mean.   The resulting samples are returned in an instance
  of the SampleCollection class, which internally holds the steps in the MCMC chain
  as a vector of weighted SamplingState's.</p>


<pre class="prettyprint lang-cpp">
  std::shared_ptr&lt;SampleCollection&gt; samps = mcmc-&gt;Run(startPt);
</pre>

<h3>4. Analyze the results</h3>

<p>When looking at the entries in a SampleCollection, it is important to note that
  the states stored by a SampleCollection are weighted even in the MCMC setting.
  When a proposal $x^\prime$ is rejected, instead of making a copy of $x^{(k-1)}$
  for $x^{(k)}$, the weight on $x^{(k-1)}$ is simply incremented.  This is useful
  for large chains in high dimensional parameter spaces, where storing all duplicates
  could quickly consume available memory.</p>

<p>The SampleCollection class provides several functions for computing sample moments.
  For example, here we compute the mean, variance, and third central moment.
  While the third moment is actually a tensor, here we only return the marginal
  values, i.e., $\mathbb{E}_x[(x_i-\mu_i)^3]$ for each $i$.</p>


<pre class="prettyprint lang-cpp">
  Eigen::VectorXd sampMean = samps-&gt;Mean();
  std::cout &lt;&lt; "\nSample Mean = \n" &lt;&lt; sampMean.transpose() &lt;&lt; std::endl;

  Eigen::VectorXd sampVar = samps-&gt;Variance();
  std::cout &lt;&lt; "\nSample Variance = \n" &lt;&lt; sampVar.transpose() &lt;&lt; std::endl;

  Eigen::MatrixXd sampCov = samps-&gt;Covariance();
  std::cout &lt;&lt; "\nSample Covariance = \n" &lt;&lt; sampCov &lt;&lt; std::endl;

  Eigen::VectorXd sampMom3 = samps-&gt;CentralMoment(3);
  std::cout &lt;&lt; "\nSample Third Moment = \n" &lt;&lt; sampMom3 &lt;&lt; std::endl &lt;&lt; std::endl;

  return 0;
}
</pre>
<h1>Complete Code</h1>

<pre class="prettyprint lang-cpp">
#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/Density.h"

#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SingleChainMCMC.h"
#include "MUQ/SamplingAlgorithms/MCMCFactory.h"

#include &lt;boost/property_tree/ptree.hpp&gt;

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;


int main(){

  Eigen::VectorXd mu(2);
  mu &lt;&lt; 1.0, 2.0;

  Eigen::MatrixXd cov(2,2);
  cov &lt;&lt; 1.0, 0.8,
         0.8, 1.5;

  auto targetDensity = std::make_shared&lt;Gaussian&gt;(mu, cov)-&gt;AsDensity(); // standard normal Gaussian

  auto problem = std::make_shared&lt;SamplingProblem&gt;(targetDensity);

  // parameters for the sampler
  pt::ptree pt;
  pt.put("NumSamples", 1e4); // number of MCMC steps
  pt.put("BurnIn", 1e3);
  pt.put("PrintLevel",3);
  pt.put("KernelList", "Kernel1"); // Name of block that defines the transition kernel
  pt.put("Kernel1.Method","MHKernel");  // Name of the transition kernel class
  pt.put("Kernel1.Proposal", "MyProposal"); // Name of block defining the proposal distribution
  pt.put("Kernel1.MyProposal.Method", "MHProposal"); // Name of proposal class
  pt.put("Kernel1.MyProposal.ProposalVariance", 2.5); // Variance of the isotropic MH proposal

  Eigen::VectorXd startPt = mu;
  auto mcmc = MCMCFactory::CreateSingleChain(pt, problem);

  std::shared_ptr&lt;SampleCollection&gt; samps = mcmc-&gt;Run(startPt);

  Eigen::VectorXd sampMean = samps-&gt;Mean();
  std::cout &lt;&lt; "\nSample Mean = \n" &lt;&lt; sampMean.transpose() &lt;&lt; std::endl;

  Eigen::VectorXd sampVar = samps-&gt;Variance();
  std::cout &lt;&lt; "\nSample Variance = \n" &lt;&lt; sampVar.transpose() &lt;&lt; std::endl;

  Eigen::MatrixXd sampCov = samps-&gt;Covariance();
  std::cout &lt;&lt; "\nSample Covariance = \n" &lt;&lt; sampCov &lt;&lt; std::endl;

  Eigen::VectorXd sampMom3 = samps-&gt;CentralMoment(3);
  std::cout &lt;&lt; "\nSample Third Moment = \n" &lt;&lt; sampMom3 &lt;&lt; std::endl &lt;&lt; std::endl;

  return 0;
}
</pre>

        <footer class="site-footer">

</footer>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>


    </div>
    <div class="col-md-3 col-sm-12">
      <div class="panel panel-default">
<div class="panel-heading"><img height=30px src="images/Slack_Mark_Web.png" alt="Slack LOGO"> MUQ is on Slack! </div>

<ul class="list-group">
<li class="list-group-item">
Join our slack workspace to connect with other users, get help, and discuss new features.</br>
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-md btn-block" data-toggle="modal" data-target="#slackModal">Join Us</button>
</li>
</ul>

<!-- Slack signup Modal -->
<div id="slackModal" class="modal fade" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Join the MUQ Slack Workspace</h4>
    </div>

    <div class="modal-body">

      <!-- Main form -->
      <form method="POST">

        <div class="form-group">
          <label for="firstNameInput">First Name</label>
          <input type="text" class="form-control" id="firstNameInput" name="first" placeholder="Thomas"  />
        </div>

        <div class="form-group">
          <label for="lastNameInput">Last Name</label>
          <input type="text" class="form-control" id="lastNameInput" name="last" placeholder="Bayes"  />
        </div>

        <div class="form-group">
          <label for="emailInput">Email Address</label>
          <input type="text" class="form-control" id="emailInput" name="mail" placeholder="reverend.bayes@likelihood.com"  />
        </div>

      <div class="g-recaptcha" data-sitekey="6LftPNgUAAAAAJN_INwyLN7aunawWrpD4LlQjokD" data-callback="enableSubmitSlack"></div>

      <h5 id="response"></h5>
      <input type="submit" class="btn btn-success btn-send" name="slackSubmit" id="slackSubmitButton" value="Sign me up!" disabled/>

      </form>

    <script>
      function enableSubmitSlack() {
          var bt = document.getElementById('slackSubmitButton');
          bt.disabled = false;
      }

      (() => {
        const form = document.querySelector('form');
        const submitResponse = document.querySelector('#response');
        const formURL = 'https://us-central1-api-project-238359040999.cloudfunctions.net/muq_slack_request';
        var slackModal = document.getElementById("slackModal");
        var repeatModal = document.getElementById("slackDuplicateEmailModal");

        form.onsubmit = e => {
          e.preventDefault();

          // Capture the form data
          let data = {};
          Array.from(form).map(input => (data[input.id] = input.value));
          console.log('Sending: ', JSON.stringify(data));
          submitResponse.innerHTML = 'Sending...'

          // Create the AJAX request
          var xhr = new XMLHttpRequest();
          xhr.open(form.method, formURL, true);
          xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
          xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

          // Send the collected data as JSON
          xhr.send(JSON.stringify(data));

          xhr.onloadend = response => {
            if (response.target.status === 200) {


              if( response.target.responseText.startsWith("Error") ){

                if( response.target.responseText.startsWith("Error. Email address") ){
                  form.reset();
                  submitResponse.innerHTML = ''
                  $('#slackModal').modal('hide');
                  $('#slackDuplicateEmailModal').modal('show');

                }else if (response.target.responseText.startsWith("Error. Invalid email address")) {
                  submitResponse.innerHTML = 'Invalid email address.  Please try again.';
                }else{
                  form.reset();
                  submitResponse.innerHTML = ''
                  $('#slackModal').modal('hide');
                  $('#slackUnsuccessfulModal').modal('show');
                }
              }else{
                form.reset();
                submitResponse.innerHTML = ''
                $('#slackModal').modal('hide');
                $('#slackSuccessfulModal').modal('show');
              }

            } else {
              form.reset();
              submitResponse.innerHTML = ''
              $('#slackModal').modal('hide');
              $('#slackUnsuccessfulModal').modal('show');

              console.error(JSON.parse(response));
            }
          };
        };
      })();
    </script>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>

  </div>
</div>
</div>

<!-- Slack duplicate email -->
<div id="slackDuplicateEmailModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Error:</h4>
    <p>Could not send Slack invite because the email already exists in the MUQ workspace.</p>
  </div>

  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>

</div>
</div>
</div>

<!-- Slack duplicate email -->
<div id="slackSuccessfulModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Success!</h4>
    <p>You should see a slack invitation in your email soon.</p>
  </div>

  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>

</div>
</div>
</div>

<!-- Slack duplicate email -->
<div id="slackUnsuccessfulModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Error:</h4>
    <p>Oops...  Something happened and we weren't able to send the Slack invitation.</p>
  </div>

  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>

</div>
</div>
</div>

</div>

      <div class="panel panel-default">
        <div class="panel-heading">Test Status</div>
        <ul class="list-group">
        <li class="list-group-item">
        <a href='https://bitbucket.org/mituq/muq2/addon/pipelines/home'><img src='https://img.shields.io/bitbucket/pipelines/mituq/muq2/master'></a>
        </li>
        </ul>
      </div>

      <div class="panel panel-default">
      <div class="panel-heading">Acknowledgments</div>
      <div class="panel-body">

        <div>
          <p style="float: left;"><img src="https://mituq.bitbucket.io/source/_site/images/nsf_logo.gif" alt="NSF Logo" hspace="5" align="left" height="70" width="70"></p>
          <p style="font-size: 1.0rem;">This material is based upon work supported by the National Science Foundation under Grant No. 1550487.</p>
          <p style="font-size: 1.0rem;">Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation.</p>
        </div>

        <div>
            <p style="float: left;"><img src="https://mituq.bitbucket.io/source/_site/images/doe_logo.png" alt="DOE Logo" hspace="5" align="left" height="70" width="70"></p>
            <p style="font-size: 1.0rem;">This material is based upon work supported by the US Department of Energy, Office of Advanced Scientific Computing Research, SciDAC (Scientific Discovery through Advanced Computing) program under awards DE-SC0007099 and DE-SC0021226, for the QUEST and <a href="https://fastmath-scidac.llnl.gov/">FASTMath</a> SciDAC Institutes. </p>
        </div>
      </div>
      </div>


    </div>
    </div>
    </div>

  </body>
</html>
