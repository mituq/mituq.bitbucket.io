<!-- head.hmtl -->
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <title>Basic Metropolis-in-Gibbs Sampling | MUQ by </title>
    <meta name="description" content="Metropolis-in-Gibbs sampling of a simple hierarchical Bayesian model with Gaussian-Inverse Gamma target density."/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#157878">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Titillium+Web:300' type='text/css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Arimo:400,700' type='text/css'>

    <link rel="stylesheet" href="https://mituq.bitbucket.io/source/_site/assets/css/style.css">
    <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
    extensions: ["tex2jax.js","TeX/AMSmath.js","TeX/AMSsymbols.js"],
    jax: ["input/TeX","output/HTML-CSS"],
    tex: {
      equationNumbers: { autoNumber: "all" },
      tagSide: "right"
    },
    tex2jax: {
      inlineMath: [ ['$','$'], ["\\(","\\)"] ],
      displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
      processEscapes: true
    }
  });
</script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/latest.js?config=TeX-MML-AM_CHTML' async></script>
</head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57389527-4', 'auto');
  ga('send', 'pageview');

</script>


<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #2A3E48;">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
        </button>
        <a class="navbar-brand" href="https://mituq.bitbucket.io/source/_site">MUQ</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          
            <li><a href="https://mituq.bitbucket.io/source/_site/index.html">Home</a></li>
          
            <li><a href="https://mituq.bitbucket.io/source/_site/examples.html">Examples</a></li>
          
          <li><a href="https://mituq.bitbucket.io/source/_site/latest/index.html">Documentation</a></li>
          <li><a href="https://bitbucket.org/mituq/muq2">Git It!</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>

    <section class="page-header">
      <h1 class="project-name">MUQ</h1>
      <h2 class="project-tagline">The MIT Uncertainty Quantification Library</h2>
    </section>


    <div class="container" role="main">
    <div class="row">
    <div class="col-md-9 col-sm-12">

        <h1><small class="text-muted">Example</small> </br> Basic Metropolis-in-Gibbs Sampling<h1>
<blockquote class="blockquote"><p class="mb-0">Metropolis-in-Gibbs sampling of a simple hierarchical Bayesian model with Gaussian-Inverse Gamma target density.</p></blockquote>
</br>




<h2>Overview</h2>

<p>The goal of this example is to demonstrate MCMC sampling with block updates.
The problem is to sample a Gaussian distribution where the variance of
the Gaussian is a random variable endowed with an inverse Gamma distribution.
Thus, there are two "blocks" of interest: the Gaussian random variable and the
variance hyperparameter.  We will sample the joint distribution of both parameters
by constructing a Metropolis-Within-Gibbs sampler.</p>

<h3>Problem Formulation</h3>

<p>Let $x$ denote the Gaussian random variable and $\sigma^2$ its
variance, which follows an Inverse Gamma distribution.  Notice that the joint
density $\pi(x,\sigma)$ can be expanded in two ways:
$$
\pi(x,\sigma^2) = \pi(x | \sigma^2)\pi(\sigma^2)
$$
and
$$
\pi(x,\sigma^2) = \pi(\sigma^2 | x)\pi(x).
$$
We will use this to simplify the Metropolis-Hastings acceptance ratios below.</p>

<p>Now consider a two stage Metropolis-Hastings algorithm.  In the first stage
we take a step in $x$ with a fixed value of $\sigma^2$ and in the second stage
we take a step in $\sigma^2$ with a fixed value of $x$.  Using the Metropolis-Hastings
rule for each stage, the algorithm is given by</p>

<ol>
<li><p>Update $x$</p>

<p>a. Propose a step in the $x$ block, $x^\prime \sim q(x | x^{(k-1)}, \sigma^{(k-1)})$</p>

<p>b. Compute the acceptance probability using the expanded joint density
$$\begin{eqnarray}
\gamma &amp;=&amp; \frac{\pi(x^\prime | \sigma^{(k-1)})\pi(\sigma^{(k-1)})}{\pi(x^{(k-1)} | \sigma^{(k-1)}) \pi(\sigma^{(k-1)})} \frac{q(x^{(k-1)} | x^\prime, \sigma^{(k-1)})}{q(x^\prime | x^{(k-1)}, \sigma^{(k-1)})} \\
     &amp;=&amp; \frac{\pi(x^\prime | \sigma^{(k-1)})}{\pi(x^{(k-1)} | \sigma^{(k-1)})} \frac{q(x^{(k-1)} | x^\prime, \sigma^{(k-1)})}{q(x^\prime | x^{(k-1)}, \sigma^{(k-1)})}
\end{eqnarray}$$</p>

<p>c. Take the step in the $x$ block: $x^{(k)} = x^\prime$ with probability $\min(1,\gamma)$, else $x^{(k)} = x^{(k-1)}$</p></li>
<li><p>Update $\sigma^2$</p>

<p>a. Propose a step in the $\sigma^2$ block, $\sigma^\prime \sim q(\sigma | x^{(k)}, \sigma^{(k-1)})$</p>

<p>b. Compute the acceptance probability using the expanded joint density
$$\begin{eqnarray}
\gamma &amp;=&amp; \frac{\pi(\sigma^\prime | x^{(k)})\pi(x^{(k)})}{\pi(\sigma^{(k-1)} | x^{(k)}) \pi(x^{(k)})} \frac{q(\sigma^{(k-1)} | \sigma^\prime, x^{(k)})}{q(\sigma^\prime | \sigma^{(k-1)}, x^{(k)})}. \\
     &amp;=&amp; \frac{\pi(\sigma^\prime | x^{(k)})}{\pi(\sigma^{(k-1)} | x^{(k)})} \frac{q(\sigma^{(k-1)} | \sigma^\prime, x^{(k)})}{q(\sigma^\prime | \sigma^{(k-1)}, x^{(k)})}.
\end{eqnarray}$$</p>

<p>c. Take the step in the $\sigma^2$ block: $\sigma^{(k)} = \sigma^\prime$ with probability $\min(1,\gamma)$, else $\sigma^{(k)} = \sigma^{(k-1)}$</p></li>
</ol>

<p>The extra complexity of this two stage approach is warranted when one or both of the block proposals
$q(\sigma^\prime | \sigma^{(k-1)}, x^{(k)})$ and $q(x^\prime | x^{(k-1)}, \sigma^{(k-1)})$
can be chosen to match the condtiional target densities $\pi(\sigma^\prime | x^{(k)})$
and $\pi(x^\prime | \sigma^{(k-1)})$.  When $\pi(x | \sigma^2)$ is Gaussian
and $\pi(\sigma^2)$ is Inverse Gamma, as is the case in this example, the conditional target distribution $\pi(\sigma^2 | x)$ can be sampled
directly, allowing us to choose $q(\sigma^\prime | \sigma^{(k-1)}, x^{(k)}) = \pi(\sigma^\prime | x^{(k)})$.
This guarantees an acceptance probability of one for the $\sigma^2$ update.  Notice
that in this example, $\pi(x^\prime | \sigma^{(k-1)})$ is Gaussian and could also
be sampled directly.  For illustrative purposes however, we will mix a random
walk proposal on $x$ with an independent Inverse Gamma proposal on $\sigma^2$.</p>



<h2>Implementation</h2>

<p>To sample the Gaussian target, the code needs to do four things:</p>

<ol>
<li><p>Define the joint Gaussian-Inverse Gamma target density with two inputs and set up a sampling problem.</p></li>
<li><p>Construct the blockwise Metropolis-Hastings algorithm with a mix of random walk and Inverse Gamma proposals.</p></li>
<li><p>Run the MCMC algorithm.</p></li>
<li><p>Analyze the results.</p></li>
</ol>

<h3>Include statements</h3>


<pre class="prettyprint lang-cpp">
#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/InverseGamma.h"
#include "MUQ/Modeling/Distributions/Density.h"
#include "MUQ/Modeling/Distributions/DensityProduct.h"

#include "MUQ/Modeling/LinearAlgebra/IdentityOperator.h"
#include "MUQ/Modeling/ReplicateOperator.h"
#include "MUQ/Modeling/WorkGraph.h"
#include "MUQ/Modeling/ModGraphPiece.h"

#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SingleChainMCMC.h"
#include "MUQ/SamplingAlgorithms/MCMCFactory.h"

#include &lt;boost/property_tree/ptree.hpp&gt;

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;


int main(){
</pre>

<h3>1. Define the joint Gaussian-Inverse Gamma target density</h3>

<p>Here we need to construct the joint density $\pi(x | \sigma^2)\pi(\sigma^2)$.
  Combining models model components in MUQ is accomplished by creating a
  WorkGraph, adding components the graph as nodes, and then adding edges to
  connect the components and construct the more complicated model.  Once constructed,
  our graph should look like:</p>

<p><center>
    <img src="DocFiles/GraphImage.png" alt="Model Graph" style="width: 400px;"/>
  </center></p>

<p>Dashed nodes in the image correspond to model inputs and nodes with solid borders
  represent model components represented through a child of the ModPiece class.
  The following code creates each of the components and then adds them on to the
  graph.  Notice that when the ModPiece's are added to the graph, a node name is
  specified.  Later, we will use these names to identify structure in the graph
  that can be exploited to generate the Inverse Gamma proposal.  Note that the
  node names used below correspond to the names used in the figure above.</p>


<pre class="prettyprint lang-cpp">
  auto varPiece = std::make_shared&lt;IdentityOperator&gt;(1);
</pre>

<p>In previous examples, the only input to the Gaussian density was the parameter
  $x$ itself.  Here however, the variance is also an input.  The Gaussian class
  provides an enum for defining this type of extra input.  Avaialable options are</p>

<ul>
<li><code>Gaussian::Mean</code> The mean should be included as an input</li>
<li><code>Gaussian::DiagCovariance</code> The covariance diagonal is an input</li>
<li><code>Gaussian::DiagPrecision</code> The precision diagonal is an input</li>
<li><code>Gaussian::FullCovariance</code> The full covariance is an input (unraveled as a vector)</li>
<li><p><code>Gaussian::FullPrecision</code> The full precision is an input (unraveled as a vector)</p>

<p>The <code>|</code> operator can be used to combine options.  For example, if both the mean and
diagonal covariance will be inputs, then we could pass <code>Gaussian::Mean | Gaussian::DiagCovariance</code>
to the Gaussian constructor.</p>

<p>In our case, only the diagonal covariance will be an input, so we can simply use <code>Gaussian::DiagCovariance</code>.</p></li>
</ul>


<pre class="prettyprint lang-cpp">
  Eigen::VectorXd mu(2);
  mu &lt;&lt; 1.0, 2.0;

  auto gaussDens = std::make_shared&lt;Gaussian&gt;(mu, Gaussian::DiagCovariance)-&gt;AsDensity();

  std::cout &lt;&lt; "Gaussian piece has " &lt;&lt; gaussDens-&gt;inputSizes.size()
            &lt;&lt; " inputs with sizes " &lt;&lt; gaussDens-&gt;inputSizes.transpose() &lt;&lt; std::endl;
</pre>

<p>Here we construct the Inverse Gamma distribution $\pi(\sigma^2)$</p>


<pre class="prettyprint lang-cpp">
  const double alpha = 2.5;
  const double beta = 1.0;

  auto varDens = std::make_shared&lt;InverseGamma&gt;(alpha,beta)-&gt;AsDensity();
</pre>

<p>To define the product $\pi(x|\sigma^2)\pi(\sigma^2)$, we will use the DensityProduct class.</p>


<pre class="prettyprint lang-cpp">
  auto prodDens = std::make_shared&lt;DensityProduct&gt;(2);
</pre>

<p>The Gaussian density used here is two dimensional with the same variance in each
dimension.  The Gaussian ModPiece thus requires a two dimensional vector to define
the diagonal covariance.  To support that, we need to replicate the 1D vector
returned by the "varPiece" IdentityOperator.   The ReplicateOperator class
provides this functionality.</p>


<pre class="prettyprint lang-cpp">
  auto replOp = std::make_shared&lt;ReplicateOperator&gt;(1,2);

  auto graph = std::make_shared&lt;WorkGraph&gt;();

  graph-&gt;AddNode(gaussDens, "Gaussian Density");
  graph-&gt;AddNode(varPiece, "Variance");
  graph-&gt;AddNode(varDens, "Variance Density");
  graph-&gt;AddNode(prodDens, "Joint Density");
  graph-&gt;AddNode(replOp, "Replicated Variance");

  graph-&gt;AddEdge("Variance", 0, "Replicated Variance", 0);
  graph-&gt;AddEdge("Replicated Variance", 0, "Gaussian Density", 1);
  graph-&gt;AddEdge("Variance", 0, "Variance Density", 0);

  graph-&gt;AddEdge("Gaussian Density", 0, "Joint Density", 0);
  graph-&gt;AddEdge("Variance Density", 0, "Joint Density", 1);
</pre>

<h4>Visualize the graph</h4>

<p>To check to make sure we constructed the graph correctly, we will employ the
  WorkGraph::Visualize function.  This function generates an image in the folder
  where the executable was run.  The result will look like the image below.  Looking
  at this image closely, we see that, with the exception of the "Replicate Variance"
  node, the structure matches what we expect.</p>


<pre class="prettyprint lang-cpp">
  graph-&gt;Visualize("DensityGraph.png");
</pre>

<p><center>
    <img src="DensityGraph.png" alt="MUQ-Generated Graph" style="width: 600px;"/>
  </center></p>



<h4>Construct the joint density model and sampling problem</h4>

<p>Here we wrap the graph into a single ModPiece that can be used to construct the
  sampling problem.</p>


<pre class="prettyprint lang-cpp">
  auto jointDens = graph-&gt;CreateModPiece("Joint Density");

  auto problem = std::make_shared&lt;SamplingProblem&gt;(jointDens);
</pre>

<h3>2. Construct the blockwise Metropolis-Hastings algorithm</h3>

<p>The entire two-block MCMC algorithm described above can be specified using the
same boost property tree approach used to specify a single chain algorithm.  The
only difference is the number of kernels specified in the "KernelList" ptree
entry.   Here, two other ptree blocks are specified "Kernel1" and "Kernel2".</p>

<p>"Kernel1" specifies the transition kernel used to update the Gaussian variable
$x$.  Here, it is the same random walk Metropolis (RWM) algorithm used in the
first MCMC example.</p>

<p>"Kernel2" specifies the transition kernel used to udpate the variance $\sigma^2$.
It could also employ a random walk proposal, but here we use to Inverse Gamma
proposal, which requires knowledge about both $\pi(x | \sigma^2)$ and $\pi(\sigma^2)$.
To pass this information to the proposal, we specify which nodes in the WorkGraph
correspond to the Gaussian and Inverse Gamma densities.</p>


<pre class="prettyprint lang-cpp">
  pt::ptree pt;
  pt.put("NumSamples", 1e5); // number of MCMC steps
  pt.put("BurnIn", 1e4);
  pt.put("PrintLevel",3);
  pt.put("KernelList", "Kernel1,Kernel2"); // Name of block that defines the transition kernel

  pt.put("Kernel1.Method","MHKernel");  // Name of the transition kernel class
  pt.put("Kernel1.Proposal", "MyProposal"); // Name of block defining the proposal distribution
  pt.put("Kernel1.MyProposal.Method", "CrankNicolsonProposal"); // Name of proposal class
  pt.put("Kernel1.MyProposal.ProposalVariance", 0.5); // Variance of the isotropic MH proposal
  pt.put("Kernel1.MyProposal.Beta", 0.25); // Variance of the isotropic MH proposal
  pt.put("Kernel1.MyProposal.PriorNode", "Gaussian Density");

  pt.put("Kernel2.Method","MHKernel");  // Name of the transition kernel class
  pt.put("Kernel2.Proposal", "GammaProposal"); // Name of block defining the proposal distribution

  pt.put("Kernel2.GammaProposal.Method", "InverseGammaProposal");
  pt.put("Kernel2.GammaProposal.InverseGammaNode", "Variance Density");
  pt.put("Kernel2.GammaProposal.GaussianNode", "Gaussian Density");
</pre>

<p>Once the algorithm parameters are specified, we can pass them to the CreateSingleChain
  function of the MCMCFactory class to create an instance of the MCMC algorithm we defined in the
  property tree.</p>


<pre class="prettyprint lang-cpp">
  auto mcmc = MCMCFactory::CreateSingleChain(pt, problem);
</pre>

<h3>3. Run the MCMC algorithm</h3>

<p>We are now ready to run the MCMC algorithm.  Here we start the chain at the
  target densities mean.   The resulting samples are returned in an instance
  of the SampleCollection class, which internally holds the steps in the MCMC chain
  as a vector of weighted SamplingState's.</p>


<pre class="prettyprint lang-cpp">
  std::vector&lt;Eigen::VectorXd&gt; startPt(2);
  startPt.at(0) = mu; // Start the Gaussian block at the mean
  startPt.at(1) = Eigen::VectorXd::Ones(1); // Set the starting value of the variance to 1

  std::shared_ptr&lt;SampleCollection&gt; samps = mcmc-&gt;Run(startPt);
</pre>

<h3>4. Analyze the results</h3>


<pre class="prettyprint lang-cpp">
  Eigen::VectorXd sampMean = samps-&gt;Mean();
  std::cout &lt;&lt; "Sample Mean = \n" &lt;&lt; sampMean.transpose() &lt;&lt; std::endl;

  Eigen::VectorXd sampVar = samps-&gt;Variance();
  std::cout &lt;&lt; "\nSample Variance = \n" &lt;&lt; sampVar.transpose() &lt;&lt; std::endl;

  Eigen::MatrixXd sampCov = samps-&gt;Covariance();
  std::cout &lt;&lt; "\nSample Covariance = \n" &lt;&lt; sampCov &lt;&lt; std::endl;

  Eigen::VectorXd sampMom3 = samps-&gt;CentralMoment(3);
  std::cout &lt;&lt; "\nSample Third Moment = \n" &lt;&lt; sampMom3 &lt;&lt; std::endl &lt;&lt; std::endl;

  return 0;
}
</pre>
<h1>Complete Code</h1>

<pre class="prettyprint lang-cpp">
#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/InverseGamma.h"
#include "MUQ/Modeling/Distributions/Density.h"
#include "MUQ/Modeling/Distributions/DensityProduct.h"

#include "MUQ/Modeling/LinearAlgebra/IdentityOperator.h"
#include "MUQ/Modeling/ReplicateOperator.h"
#include "MUQ/Modeling/WorkGraph.h"
#include "MUQ/Modeling/ModGraphPiece.h"

#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SingleChainMCMC.h"
#include "MUQ/SamplingAlgorithms/MCMCFactory.h"

#include &lt;boost/property_tree/ptree.hpp&gt;

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;


int main(){

  auto varPiece = std::make_shared&lt;IdentityOperator&gt;(1);

  Eigen::VectorXd mu(2);
  mu &lt;&lt; 1.0, 2.0;

  auto gaussDens = std::make_shared&lt;Gaussian&gt;(mu, Gaussian::DiagCovariance)-&gt;AsDensity();

  std::cout &lt;&lt; "Gaussian piece has " &lt;&lt; gaussDens-&gt;inputSizes.size()
            &lt;&lt; " inputs with sizes " &lt;&lt; gaussDens-&gt;inputSizes.transpose() &lt;&lt; std::endl;

  const double alpha = 2.5;
  const double beta = 1.0;

  auto varDens = std::make_shared&lt;InverseGamma&gt;(alpha,beta)-&gt;AsDensity();

  auto prodDens = std::make_shared&lt;DensityProduct&gt;(2);

  auto replOp = std::make_shared&lt;ReplicateOperator&gt;(1,2);

  auto graph = std::make_shared&lt;WorkGraph&gt;();

  graph-&gt;AddNode(gaussDens, "Gaussian Density");
  graph-&gt;AddNode(varPiece, "Variance");
  graph-&gt;AddNode(varDens, "Variance Density");
  graph-&gt;AddNode(prodDens, "Joint Density");
  graph-&gt;AddNode(replOp, "Replicated Variance");

  graph-&gt;AddEdge("Variance", 0, "Replicated Variance", 0);
  graph-&gt;AddEdge("Replicated Variance", 0, "Gaussian Density", 1);
  graph-&gt;AddEdge("Variance", 0, "Variance Density", 0);

  graph-&gt;AddEdge("Gaussian Density", 0, "Joint Density", 0);
  graph-&gt;AddEdge("Variance Density", 0, "Joint Density", 1);

  graph-&gt;Visualize("DensityGraph.png");
  auto jointDens = graph-&gt;CreateModPiece("Joint Density");

  auto problem = std::make_shared&lt;SamplingProblem&gt;(jointDens);

  pt::ptree pt;
  pt.put("NumSamples", 1e5); // number of MCMC steps
  pt.put("BurnIn", 1e4);
  pt.put("PrintLevel",3);
  pt.put("KernelList", "Kernel1,Kernel2"); // Name of block that defines the transition kernel

  pt.put("Kernel1.Method","MHKernel");  // Name of the transition kernel class
  pt.put("Kernel1.Proposal", "MyProposal"); // Name of block defining the proposal distribution
  pt.put("Kernel1.MyProposal.Method", "CrankNicolsonProposal"); // Name of proposal class
  pt.put("Kernel1.MyProposal.ProposalVariance", 0.5); // Variance of the isotropic MH proposal
  pt.put("Kernel1.MyProposal.Beta", 0.25); // Variance of the isotropic MH proposal
  pt.put("Kernel1.MyProposal.PriorNode", "Gaussian Density");

  pt.put("Kernel2.Method","MHKernel");  // Name of the transition kernel class
  pt.put("Kernel2.Proposal", "GammaProposal"); // Name of block defining the proposal distribution

  pt.put("Kernel2.GammaProposal.Method", "InverseGammaProposal");
  pt.put("Kernel2.GammaProposal.InverseGammaNode", "Variance Density");
  pt.put("Kernel2.GammaProposal.GaussianNode", "Gaussian Density");

  auto mcmc = MCMCFactory::CreateSingleChain(pt, problem);

  std::vector&lt;Eigen::VectorXd&gt; startPt(2);
  startPt.at(0) = mu; // Start the Gaussian block at the mean
  startPt.at(1) = Eigen::VectorXd::Ones(1); // Set the starting value of the variance to 1

  std::shared_ptr&lt;SampleCollection&gt; samps = mcmc-&gt;Run(startPt);

  Eigen::VectorXd sampMean = samps-&gt;Mean();
  std::cout &lt;&lt; "Sample Mean = \n" &lt;&lt; sampMean.transpose() &lt;&lt; std::endl;

  Eigen::VectorXd sampVar = samps-&gt;Variance();
  std::cout &lt;&lt; "\nSample Variance = \n" &lt;&lt; sampVar.transpose() &lt;&lt; std::endl;

  Eigen::MatrixXd sampCov = samps-&gt;Covariance();
  std::cout &lt;&lt; "\nSample Covariance = \n" &lt;&lt; sampCov &lt;&lt; std::endl;

  Eigen::VectorXd sampMom3 = samps-&gt;CentralMoment(3);
  std::cout &lt;&lt; "\nSample Third Moment = \n" &lt;&lt; sampMom3 &lt;&lt; std::endl &lt;&lt; std::endl;

  return 0;
}
</pre>

        <footer class="site-footer">

</footer>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>


    </div>
    <div class="col-md-3 col-sm-12">
      <div class="panel panel-default">
<div class="panel-heading"><img height=30px src="images/Slack_Mark_Web.png" alt="Slack LOGO"> MUQ is on Slack! </div>

<ul class="list-group">
<li class="list-group-item">
Join our slack workspace to connect with other users, get help, and discuss new features.</br>
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-md btn-block" data-toggle="modal" data-target="#slackModal">Join Us</button>
</li>
</ul>

<!-- Slack signup Modal -->
<div id="slackModal" class="modal fade" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Join the MUQ Slack Workspace</h4>
    </div>

    <div class="modal-body">

      <!-- Main form -->
      <form method="POST">

        <div class="form-group">
          <label for="firstNameInput">First Name</label>
          <input type="text" class="form-control" id="firstNameInput" name="first" placeholder="Thomas"  />
        </div>

        <div class="form-group">
          <label for="lastNameInput">Last Name</label>
          <input type="text" class="form-control" id="lastNameInput" name="last" placeholder="Bayes"  />
        </div>

        <div class="form-group">
          <label for="emailInput">Email Address</label>
          <input type="text" class="form-control" id="emailInput" name="mail" placeholder="reverend.bayes@likelihood.com"  />
        </div>

      <div class="g-recaptcha" data-sitekey="6LftPNgUAAAAAJN_INwyLN7aunawWrpD4LlQjokD" data-callback="enableSubmitSlack"></div>

      <h5 id="response"></h5>
      <input type="submit" class="btn btn-success btn-send" name="slackSubmit" id="slackSubmitButton" value="Sign me up!" disabled/>

      </form>

    <script>
      function enableSubmitSlack() {
          var bt = document.getElementById('slackSubmitButton');
          bt.disabled = false;
      }

      (() => {
        const form = document.querySelector('form');
        const submitResponse = document.querySelector('#response');
        const formURL = 'https://us-central1-api-project-238359040999.cloudfunctions.net/muq_slack_request';
        var slackModal = document.getElementById("slackModal");
        var repeatModal = document.getElementById("slackDuplicateEmailModal");

        form.onsubmit = e => {
          e.preventDefault();

          // Capture the form data
          let data = {};
          Array.from(form).map(input => (data[input.id] = input.value));
          console.log('Sending: ', JSON.stringify(data));
          submitResponse.innerHTML = 'Sending...'

          // Create the AJAX request
          var xhr = new XMLHttpRequest();
          xhr.open(form.method, formURL, true);
          xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
          xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

          // Send the collected data as JSON
          xhr.send(JSON.stringify(data));

          xhr.onloadend = response => {
            if (response.target.status === 200) {


              if( response.target.responseText.startsWith("Error") ){

                if( response.target.responseText.startsWith("Error. Email address") ){
                  form.reset();
                  submitResponse.innerHTML = ''
                  $('#slackModal').modal('hide');
                  $('#slackDuplicateEmailModal').modal('show');

                }else if (response.target.responseText.startsWith("Error. Invalid email address")) {
                  submitResponse.innerHTML = 'Invalid email address.  Please try again.';
                }else{
                  form.reset();
                  submitResponse.innerHTML = ''
                  $('#slackModal').modal('hide');
                  $('#slackUnsuccessfulModal').modal('show');
                }
              }else{
                form.reset();
                submitResponse.innerHTML = ''
                $('#slackModal').modal('hide');
                $('#slackSuccessfulModal').modal('show');
              }

            } else {
              form.reset();
              submitResponse.innerHTML = ''
              $('#slackModal').modal('hide');
              $('#slackUnsuccessfulModal').modal('show');

              console.error(JSON.parse(response));
            }
          };
        };
      })();
    </script>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>

  </div>
</div>
</div>

<!-- Slack duplicate email -->
<div id="slackDuplicateEmailModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Error:</h4>
    <p>Could not send Slack invite because the email already exists in the MUQ workspace.</p>
  </div>

  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>

</div>
</div>
</div>

<!-- Slack duplicate email -->
<div id="slackSuccessfulModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Success!</h4>
    <p>You should see a slack invitation in your email soon.</p>
  </div>

  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>

</div>
</div>
</div>

<!-- Slack duplicate email -->
<div id="slackUnsuccessfulModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Error:</h4>
    <p>Oops...  Something happened and we weren't able to send the Slack invitation.</p>
  </div>

  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>

</div>
</div>
</div>

</div>

      <div class="panel panel-default">
        <div class="panel-heading">Test Status</div>
        <ul class="list-group">
        <li class="list-group-item">
        <a href='https://bitbucket.org/mituq/muq2/addon/pipelines/home'><img src='https://img.shields.io/bitbucket/pipelines/mituq/muq2/master'></a>
        </li>
        </ul>
      </div>

      <div class="panel panel-default">
      <div class="panel-heading">Acknowledgments</div>
      <div class="panel-body">

        <div>
          <p style="float: left;"><img src="https://mituq.bitbucket.io/source/_site/images/nsf_logo.gif" alt="NSF Logo" hspace="5" align="left" height="70" width="70"></p>
          <p style="font-size: 1.0rem;">This material is based upon work supported by the National Science Foundation under Grant No. 1550487.</p>
          <p style="font-size: 1.0rem;">Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation.</p>
        </div>

        <div>
            <p style="float: left;"><img src="https://mituq.bitbucket.io/source/_site/images/doe_logo.png" alt="DOE Logo" hspace="5" align="left" height="70" width="70"></p>
            <p style="font-size: 1.0rem;">This material is based upon work supported by the US Department of Energy, Office of Advanced Scientific Computing Research, SciDAC (Scientific Discovery through Advanced Computing) program under awards DE-SC0007099 and DE-SC0021226, for the QUEST and <a href="https://fastmath-scidac.llnl.gov/">FASTMath</a> SciDAC Institutes. </p>
        </div>
      </div>
      </div>


    </div>
    </div>
    </div>

  </body>
</html>
