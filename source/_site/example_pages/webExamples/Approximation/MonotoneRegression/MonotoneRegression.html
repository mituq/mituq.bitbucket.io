<!-- head.hmtl -->
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <title>Monotone Regression | MUQ by </title>
    <meta name="description" content="Regression of stress-strain data with guaranteed monotonocity of the learned relationship."/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#157878">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Titillium+Web:300' type='text/css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Arimo:400,700' type='text/css'>

    <link rel="stylesheet" href="https://mituq.bitbucket.io/source/_site/assets/css/style.css">
    <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
    extensions: ["tex2jax.js","TeX/AMSmath.js","TeX/AMSsymbols.js"],
    jax: ["input/TeX","output/HTML-CSS"],
    tex: {
      equationNumbers: { autoNumber: "all" },
      tagSide: "right"
    },
    tex2jax: {
      inlineMath: [ ['$','$'], ["\\(","\\)"] ],
      displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
      processEscapes: true
    }
  });
</script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/latest.js?config=TeX-MML-AM_CHTML' async></script>
</head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57389527-4', 'auto');
  ga('send', 'pageview');

</script>


<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #2A3E48;">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
        </button>
        <a class="navbar-brand" href="https://mituq.bitbucket.io/source/_site">MUQ</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          
            <li><a href="https://mituq.bitbucket.io/source/_site/index.html">Home</a></li>
          
            <li><a href="https://mituq.bitbucket.io/source/_site/examples.html">Examples</a></li>
          
          <li><a href="https://mituq.bitbucket.io/source/_site/latest/index.html">Documentation</a></li>
          <li><a href="https://bitbucket.org/mituq/muq2">Git It!</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>

    <section class="page-header">
      <h1 class="project-name">MUQ</h1>
      <h2 class="project-tagline">The MIT Uncertainty Quantification Library</h2>
    </section>


    <div class="container" role="main">
    <div class="row">
    <div class="col-md-9 col-sm-12">

        <h1><small class="text-muted">Example</small> </br> Monotone Regression<h1>
<blockquote class="blockquote"><p class="mb-0">Regression of stress-strain data with guaranteed monotonocity of the learned relationship.</p></blockquote>
</br>




<h2>Overview:</h2>

<p>The goal of this example is to fit a monotone function to stress-strain
data obtained during the 2017 Sandia Fracture Challenge.  For our area of
interest, the stress should be a monotonically increasing function of the
strain.</p>

<p>The MonotoneExpansion class provides a way of characterizing
monotone functions and can be fit to data in a least squares sense using
the Gauss-Newton algorithm, which is implemented in the <code>FitData</code> function
below.</p>


<pre class="prettyprint lang-cpp">
#include "MUQ/Approximation/Polynomials/BasisExpansion.h"
#include "MUQ/Approximation/Polynomials/MonotoneExpansion.h"

#include "MUQ/Approximation/Polynomials/Legendre.h"
#include "MUQ/Utilities/MultiIndices/MultiIndexFactory.h"

#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Utilities/HDF5/H5Object.h"

#include &lt;Eigen/Dense&gt;

using namespace muq::Approximation;
using namespace muq::Utilities;

/** Reads SFC3 stress strain data. */
std::pair&lt;Eigen::VectorXd, Eigen::VectorXd&gt; ReadData()
{
  auto f = muq::Utilities::OpenFile("data/LTA01.h5");

  std::cout &lt;&lt; "Reading strain data..." &lt;&lt; std::endl;
  std::cout &lt;&lt; "  Units: " &lt;&lt; std::string(f["/Strain"].attrs["Units"]) &lt;&lt; std::endl;
  std::cout &lt;&lt; "  Size:  " &lt;&lt; f["/Strain"].rows() &lt;&lt; std::endl;

  Eigen::VectorXd strain = f["/Strain"];

  std::cout &lt;&lt; "Reading stress data..." &lt;&lt; std::endl;
  std::cout &lt;&lt; "  Units: " &lt;&lt; std::string(f["/Stress"].attrs["Units"]) &lt;&lt; std::endl;
  std::cout &lt;&lt; "  Size:  " &lt;&lt; f["/Stress"].rows() &lt;&lt; std::endl;

  Eigen::VectorXd stress = f["/Stress"];

  return std::make_pair(strain, stress);
};


std::shared_ptr&lt;MonotoneExpansion&gt; SetupExpansion(unsigned order)
{
    auto poly = std::make_shared&lt;Legendre&gt;();
    std::vector&lt;std::shared_ptr&lt;IndexedScalarBasis&gt;&gt; bases(1, poly);
    std::shared_ptr&lt;MultiIndexSet&gt; multis = MultiIndexFactory::CreateTotalOrder(1, order);

    Eigen::MatrixXd polyCoeffs = Eigen::MatrixXd::Zero(1,multis-&gt;Size());
    polyCoeffs(0,1) = 500.0; // add an initial linear trend

    auto polyBase = std::make_shared&lt;BasisExpansion&gt;(bases, multis, polyCoeffs);
    auto expansion = std::make_shared&lt;MonotoneExpansion&gt;(polyBase);

    return expansion;
}

/** Find parameters of the montone expansion that minimize the L2 norm between
    the predictions and observations, i.e., solve the nonlinear least squares
    problem for the parameters describing the monotone function.
*/
void FitData(Eigen::VectorXd             const& x,
             Eigen::VectorXd             const& y,
             std::shared_ptr&lt;MonotoneExpansion&gt; expansion)
{
  Eigen::VectorXd coeffs = expansion-&gt;GetCoeffs();

  Eigen::VectorXd preds(x.size());
  Eigen::VectorXd newPreds(x.size());
  Eigen::VectorXd newCoeffs;

  Eigen::VectorXd resid, newResid, step, xslice;
  Eigen::MatrixXd jac(x.size(), coeffs.size());

  const int maxLineIts = 10;
  const int maxIts = 20;

  // Use the current monotone parameterization to make predictions at every point
  for(int k=0; k&lt;x.size(); ++k){
    xslice = x.segment(k,1);
    preds(k) = boost::any_cast&lt;Eigen::VectorXd&gt;(expansion-&gt;Evaluate(xslice,coeffs).at(0))(0);
  }

  resid = y-preds;
  double sse = resid.squaredNorm();

  for(int i=0; i&lt;maxIts; ++i){

    // Compute the jacobian at the current point
    for(int k=0; k&lt;x.size(); ++k){
      xslice = x.segment(k,1);
      jac.row(k) = boost::any_cast&lt;Eigen::MatrixXd&gt;(expansion-&gt;Jacobian(1,0,xslice,coeffs));
    }

    // Compute the Gauss-Newton step
    step = jac.colPivHouseholderQr().solve(resid);
    newCoeffs = coeffs + step;

    // Use the current monotone parameterization to make predictions at every point
    for(int k=0; k&lt;x.size(); ++k){
      xslice = x.segment(k,1);
      newPreds(k) = boost::any_cast&lt;Eigen::VectorXd&gt;(expansion-&gt;Evaluate(xslice,newCoeffs).at(0))(0);
    }

    newResid = y-newPreds;
    double newsse = newResid.squaredNorm();

    // Backtracing line search to guarantee a sufficient descent
    int lineIt = 0;
    while((newsse &gt; sse - 1e-7)&&(lineIt&lt;maxLineIts)){
      step *= 0.5;
      newCoeffs = coeffs + step;

      // Compute the residuals at the new point
      for(int k=0; k&lt;x.size(); ++k){
        xslice = x.segment(k,1);
        newPreds(k) = boost::any_cast&lt;Eigen::VectorXd&gt;(expansion-&gt;Evaluate(xslice,newCoeffs).at(0))(0);
      }

      newResid = y-newPreds;
      newsse = newResid.squaredNorm();
    }

    if(lineIt == maxLineIts){
      std::cout &lt;&lt; "WARNING: Line search failed, terminating Gauss-Newton optimizer." &lt;&lt; std::endl;
      return;
    }

    // The line search was successful, so update the coefficients and residuals
    coeffs = newCoeffs;
    preds = newPreds;
    sse = newsse;
    resid = newResid;

    std::cout &lt;&lt; "Iteration " &lt;&lt; i &lt;&lt; ", SSE = "&lt;&lt; sse &lt;&lt; std::endl;
  }
}

void WriteResults(Eigen::VectorXd             const& x,
                  std::shared_ptr&lt;MonotoneExpansion&gt; expansion)
{
  Eigen::VectorXd preds(x.size());
  Eigen::VectorXd xslice;

  for(int k=0; k&lt;x.size(); ++k){
    xslice = x.segment(k,1);
    preds(k) = boost::any_cast&lt;Eigen::VectorXd&gt;(expansion-&gt;Evaluate(xslice).at(0))(0);
  }

  auto f = muq::Utilities::OpenFile("results/StressPredictions.h5");
  f["/Strain"] = x;
  f["/Stress"] = preds;
}


int main()
{

  Eigen::VectorXd strain, stress;
  std::tie(strain, stress) = ReadData();

  unsigned polyOrder = 7;
  auto expansion = SetupExpansion(polyOrder);

  FitData(strain, stress, expansion);

  WriteResults(strain, expansion);

  return 0;
};
</pre>
<h1>Complete Code</h1>

<pre class="prettyprint lang-cpp">
#include "MUQ/Approximation/Polynomials/BasisExpansion.h"
#include "MUQ/Approximation/Polynomials/MonotoneExpansion.h"

#include "MUQ/Approximation/Polynomials/Legendre.h"
#include "MUQ/Utilities/MultiIndices/MultiIndexFactory.h"

#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Utilities/HDF5/H5Object.h"

#include &lt;Eigen/Dense&gt;

using namespace muq::Approximation;
using namespace muq::Utilities;

/** Reads SFC3 stress strain data. */
std::pair&lt;Eigen::VectorXd, Eigen::VectorXd&gt; ReadData()
{
  auto f = muq::Utilities::OpenFile("data/LTA01.h5");

  std::cout &lt;&lt; "Reading strain data..." &lt;&lt; std::endl;
  std::cout &lt;&lt; "  Units: " &lt;&lt; std::string(f["/Strain"].attrs["Units"]) &lt;&lt; std::endl;
  std::cout &lt;&lt; "  Size:  " &lt;&lt; f["/Strain"].rows() &lt;&lt; std::endl;

  Eigen::VectorXd strain = f["/Strain"];

  std::cout &lt;&lt; "Reading stress data..." &lt;&lt; std::endl;
  std::cout &lt;&lt; "  Units: " &lt;&lt; std::string(f["/Stress"].attrs["Units"]) &lt;&lt; std::endl;
  std::cout &lt;&lt; "  Size:  " &lt;&lt; f["/Stress"].rows() &lt;&lt; std::endl;

  Eigen::VectorXd stress = f["/Stress"];

  return std::make_pair(strain, stress);
};


std::shared_ptr&lt;MonotoneExpansion&gt; SetupExpansion(unsigned order)
{
    auto poly = std::make_shared&lt;Legendre&gt;();
    std::vector&lt;std::shared_ptr&lt;IndexedScalarBasis&gt;&gt; bases(1, poly);
    std::shared_ptr&lt;MultiIndexSet&gt; multis = MultiIndexFactory::CreateTotalOrder(1, order);

    Eigen::MatrixXd polyCoeffs = Eigen::MatrixXd::Zero(1,multis-&gt;Size());
    polyCoeffs(0,1) = 500.0; // add an initial linear trend

    auto polyBase = std::make_shared&lt;BasisExpansion&gt;(bases, multis, polyCoeffs);
    auto expansion = std::make_shared&lt;MonotoneExpansion&gt;(polyBase);

    return expansion;
}

/** Find parameters of the montone expansion that minimize the L2 norm between
    the predictions and observations, i.e., solve the nonlinear least squares
    problem for the parameters describing the monotone function.
*/
void FitData(Eigen::VectorXd             const& x,
             Eigen::VectorXd             const& y,
             std::shared_ptr&lt;MonotoneExpansion&gt; expansion)
{
  Eigen::VectorXd coeffs = expansion-&gt;GetCoeffs();

  Eigen::VectorXd preds(x.size());
  Eigen::VectorXd newPreds(x.size());
  Eigen::VectorXd newCoeffs;

  Eigen::VectorXd resid, newResid, step, xslice;
  Eigen::MatrixXd jac(x.size(), coeffs.size());

  const int maxLineIts = 10;
  const int maxIts = 20;

  // Use the current monotone parameterization to make predictions at every point
  for(int k=0; k&lt;x.size(); ++k){
    xslice = x.segment(k,1);
    preds(k) = boost::any_cast&lt;Eigen::VectorXd&gt;(expansion-&gt;Evaluate(xslice,coeffs).at(0))(0);
  }

  resid = y-preds;
  double sse = resid.squaredNorm();

  for(int i=0; i&lt;maxIts; ++i){

    // Compute the jacobian at the current point
    for(int k=0; k&lt;x.size(); ++k){
      xslice = x.segment(k,1);
      jac.row(k) = boost::any_cast&lt;Eigen::MatrixXd&gt;(expansion-&gt;Jacobian(1,0,xslice,coeffs));
    }

    // Compute the Gauss-Newton step
    step = jac.colPivHouseholderQr().solve(resid);
    newCoeffs = coeffs + step;

    // Use the current monotone parameterization to make predictions at every point
    for(int k=0; k&lt;x.size(); ++k){
      xslice = x.segment(k,1);
      newPreds(k) = boost::any_cast&lt;Eigen::VectorXd&gt;(expansion-&gt;Evaluate(xslice,newCoeffs).at(0))(0);
    }

    newResid = y-newPreds;
    double newsse = newResid.squaredNorm();

    // Backtracing line search to guarantee a sufficient descent
    int lineIt = 0;
    while((newsse &gt; sse - 1e-7)&&(lineIt&lt;maxLineIts)){
      step *= 0.5;
      newCoeffs = coeffs + step;

      // Compute the residuals at the new point
      for(int k=0; k&lt;x.size(); ++k){
        xslice = x.segment(k,1);
        newPreds(k) = boost::any_cast&lt;Eigen::VectorXd&gt;(expansion-&gt;Evaluate(xslice,newCoeffs).at(0))(0);
      }

      newResid = y-newPreds;
      newsse = newResid.squaredNorm();
    }

    if(lineIt == maxLineIts){
      std::cout &lt;&lt; "WARNING: Line search failed, terminating Gauss-Newton optimizer." &lt;&lt; std::endl;
      return;
    }

    // The line search was successful, so update the coefficients and residuals
    coeffs = newCoeffs;
    preds = newPreds;
    sse = newsse;
    resid = newResid;

    std::cout &lt;&lt; "Iteration " &lt;&lt; i &lt;&lt; ", SSE = "&lt;&lt; sse &lt;&lt; std::endl;
  }
}

void WriteResults(Eigen::VectorXd             const& x,
                  std::shared_ptr&lt;MonotoneExpansion&gt; expansion)
{
  Eigen::VectorXd preds(x.size());
  Eigen::VectorXd xslice;

  for(int k=0; k&lt;x.size(); ++k){
    xslice = x.segment(k,1);
    preds(k) = boost::any_cast&lt;Eigen::VectorXd&gt;(expansion-&gt;Evaluate(xslice).at(0))(0);
  }

  auto f = muq::Utilities::OpenFile("results/StressPredictions.h5");
  f["/Strain"] = x;
  f["/Stress"] = preds;
}


int main()
{

  Eigen::VectorXd strain, stress;
  std::tie(strain, stress) = ReadData();

  unsigned polyOrder = 7;
  auto expansion = SetupExpansion(polyOrder);

  FitData(strain, stress, expansion);

  WriteResults(strain, expansion);

  return 0;
};
</pre>

        <footer class="site-footer">

</footer>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>


    </div>
    <div class="col-md-3 col-sm-12">
      <div class="panel panel-default">
<div class="panel-heading"><img height=30px src="images/Slack_Mark_Web.png" alt="Slack LOGO"> MUQ is on Slack! </div>

<ul class="list-group">
<li class="list-group-item">
Join our slack workspace to connect with other users, get help, and discuss new features.</br>
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-md btn-block" data-toggle="modal" data-target="#slackModal">Join Us</button>
</li>
</ul>

<!-- Slack signup Modal -->
<div id="slackModal" class="modal fade" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Join the MUQ Slack Workspace</h4>
    </div>

    <div class="modal-body">

      <!-- Main form -->
      <form method="POST">

        <div class="form-group">
          <label for="firstNameInput">First Name</label>
          <input type="text" class="form-control" id="firstNameInput" name="first" placeholder="Thomas"  />
        </div>

        <div class="form-group">
          <label for="lastNameInput">Last Name</label>
          <input type="text" class="form-control" id="lastNameInput" name="last" placeholder="Bayes"  />
        </div>

        <div class="form-group">
          <label for="emailInput">Email Address</label>
          <input type="text" class="form-control" id="emailInput" name="mail" placeholder="reverend.bayes@likelihood.com"  />
        </div>

      <div class="g-recaptcha" data-sitekey="6LftPNgUAAAAAJN_INwyLN7aunawWrpD4LlQjokD" data-callback="enableSubmitSlack"></div>

      <h5 id="response"></h5>
      <input type="submit" class="btn btn-success btn-send" name="slackSubmit" id="slackSubmitButton" value="Sign me up!" disabled/>

      </form>

    <script>
      function enableSubmitSlack() {
          var bt = document.getElementById('slackSubmitButton');
          bt.disabled = false;
      }

      (() => {
        const form = document.querySelector('form');
        const submitResponse = document.querySelector('#response');
        const formURL = 'https://us-central1-api-project-238359040999.cloudfunctions.net/muq_slack_request';
        var slackModal = document.getElementById("slackModal");
        var repeatModal = document.getElementById("slackDuplicateEmailModal");

        form.onsubmit = e => {
          e.preventDefault();

          // Capture the form data
          let data = {};
          Array.from(form).map(input => (data[input.id] = input.value));
          console.log('Sending: ', JSON.stringify(data));
          submitResponse.innerHTML = 'Sending...'

          // Create the AJAX request
          var xhr = new XMLHttpRequest();
          xhr.open(form.method, formURL, true);
          xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
          xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

          // Send the collected data as JSON
          xhr.send(JSON.stringify(data));

          xhr.onloadend = response => {
            if (response.target.status === 200) {


              if( response.target.responseText.startsWith("Error") ){

                if( response.target.responseText.startsWith("Error. Email address") ){
                  form.reset();
                  submitResponse.innerHTML = ''
                  $('#slackModal').modal('hide');
                  $('#slackDuplicateEmailModal').modal('show');

                }else if (response.target.responseText.startsWith("Error. Invalid email address")) {
                  submitResponse.innerHTML = 'Invalid email address.  Please try again.';
                }else{
                  form.reset();
                  submitResponse.innerHTML = ''
                  $('#slackModal').modal('hide');
                  $('#slackUnsuccessfulModal').modal('show');
                }
              }else{
                form.reset();
                submitResponse.innerHTML = ''
                $('#slackModal').modal('hide');
                $('#slackSuccessfulModal').modal('show');
              }

            } else {
              form.reset();
              submitResponse.innerHTML = ''
              $('#slackModal').modal('hide');
              $('#slackUnsuccessfulModal').modal('show');

              console.error(JSON.parse(response));
            }
          };
        };
      })();
    </script>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>

  </div>
</div>
</div>

<!-- Slack duplicate email -->
<div id="slackDuplicateEmailModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Error:</h4>
    <p>Could not send Slack invite because the email already exists in the MUQ workspace.</p>
  </div>

  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>

</div>
</div>
</div>

<!-- Slack duplicate email -->
<div id="slackSuccessfulModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Success!</h4>
    <p>You should see a slack invitation in your email soon.</p>
  </div>

  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>

</div>
</div>
</div>

<!-- Slack duplicate email -->
<div id="slackUnsuccessfulModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Error:</h4>
    <p>Oops...  Something happened and we weren't able to send the Slack invitation.</p>
  </div>

  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>

</div>
</div>
</div>

</div>

      <div class="panel panel-default">
        <div class="panel-heading">Test Status</div>
        <ul class="list-group">
        <li class="list-group-item">
        <a href='https://bitbucket.org/mituq/muq2/addon/pipelines/home'><img src='https://img.shields.io/bitbucket/pipelines/mituq/muq2/master'></a>
        </li>
        </ul>
      </div>

      <div class="panel panel-default">
      <div class="panel-heading">Acknowledgments</div>
      <div class="panel-body">

        <div>
          <p style="float: left;"><img src="https://mituq.bitbucket.io/source/_site/images/nsf_logo.gif" alt="NSF Logo" hspace="5" align="left" height="70" width="70"></p>
          <p style="font-size: 1.0rem;">This material is based upon work supported by the National Science Foundation under Grant No. 1550487.</p>
          <p style="font-size: 1.0rem;">Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation.</p>
        </div>

        <div>
            <p style="float: left;"><img src="https://mituq.bitbucket.io/source/_site/images/doe_logo.png" alt="DOE Logo" hspace="5" align="left" height="70" width="70"></p>
            <p style="font-size: 1.0rem;">This material is based upon work supported by the US Department of Energy, Office of Advanced Scientific Computing Research, SciDAC (Scientific Discovery through Advanced Computing) program under awards DE-SC0007099 and DE-SC0021226, for the QUEST and <a href="https://fastmath-scidac.llnl.gov/">FASTMath</a> SciDAC Institutes. </p>
        </div>
      </div>
      </div>


    </div>
    </div>
    </div>

  </body>
</html>
