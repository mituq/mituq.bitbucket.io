var indexSectionsWithContent =
{
  0: ":abcdefghijklmnopqrstuvwxz~",
  1: "abcdefghijklmnopqrstuvwxz",
  2: "cmnpu",
  3: "abcdefghijklmnopqrstuvwz",
  4: "abcdefghijklmnopqrstuvwxz~",
  5: "abcdefghijklmnopqrstuvwxz",
  6: "abcdefgijklmnoprstuv",
  7: "cefimptv",
  8: "abcdefghijklmnopqrstuvw",
  9: ":bdfgmoprsw",
  10: "cdefgmpqsu",
  11: "bdiou"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "groups",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Modules",
  11: "Pages"
};

