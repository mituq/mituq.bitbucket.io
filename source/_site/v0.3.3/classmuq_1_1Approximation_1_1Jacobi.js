var classmuq_1_1Approximation_1_1Jacobi =
[
    [ "Jacobi", "classmuq_1_1Approximation_1_1Jacobi.html#ade41febc6ffbfdd063d056b0bc0a0f37", null ],
    [ "~Jacobi", "classmuq_1_1Approximation_1_1Jacobi.html#a2c8c7b3dfffe1cd5a693984801bc56f8", null ],
    [ "ak", "classmuq_1_1Approximation_1_1Jacobi.html#a5934c34724197281a893c8b0c3b8b697", null ],
    [ "bk", "classmuq_1_1Approximation_1_1Jacobi.html#acd0b064237cedd83dd71555b59875a13", null ],
    [ "ck", "classmuq_1_1Approximation_1_1Jacobi.html#a52b94304f69e7f459d842f89b4bf4eb3", null ],
    [ "DerivativeEvaluate", "classmuq_1_1Approximation_1_1Jacobi.html#ad6f429ea57d54f277399f8bb1a42ac06", null ],
    [ "Normalization", "classmuq_1_1Approximation_1_1Jacobi.html#abee295b16fc68b8750ee29e2e9a15080", null ],
    [ "phi0", "classmuq_1_1Approximation_1_1Jacobi.html#aa36c75524f99f1b571fcee226376d9a6", null ],
    [ "phi1", "classmuq_1_1Approximation_1_1Jacobi.html#a728290bbfeb71b5783e7f19149d72248", null ],
    [ "a", "classmuq_1_1Approximation_1_1Jacobi.html#ae1ccade82c3019f09dfda385971b9f8c", null ],
    [ "b", "classmuq_1_1Approximation_1_1Jacobi.html#aef89069555e6d6cbfa1ed121a1d89a03", null ]
];