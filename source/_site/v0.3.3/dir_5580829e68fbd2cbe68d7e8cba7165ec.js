var dir_5580829e68fbd2cbe68d7e8cba7165ec =
[
    [ "CustomGaussianProposal", "dir_aeb74e558792123c486ea2722d223e91.html", "dir_aeb74e558792123c486ea2722d223e91" ],
    [ "DelayedRejection", "dir_6dded4ee49e36fb89e059918e71ba28e.html", "dir_6dded4ee49e36fb89e059918e71ba28e" ],
    [ "DILI", "dir_0cb1631d7f1a7f77dc5ae1bafa383b72.html", "dir_0cb1631d7f1a7f77dc5ae1bafa383b72" ],
    [ "Example1_Gaussian", "dir_5b69289ad8759f83fae31749db514bcb.html", "dir_5b69289ad8759f83fae31749db514bcb" ],
    [ "Example2_GaussianInverseGamma", "dir_3a3ec8c69ad832820f65b279b2d4591e.html", "dir_3a3ec8c69ad832820f65b279b2d4591e" ],
    [ "Example3_MultilevelGaussian", "dir_0435f6eb0dfde10694fd9084accc9cce.html", "dir_0435f6eb0dfde10694fd9084accc9cce" ],
    [ "Example4_MultiindexGaussian", "dir_b7352b5eca7a93fb011a328505503b6b.html", "dir_b7352b5eca7a93fb011a328505503b6b" ]
];