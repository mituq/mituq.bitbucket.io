var searchData=
[
  ['qfun',['QFun',['../classmuq_1_1SamplingAlgorithms_1_1DRKernel.html#a0d427a5679fdc9aa8662dea9e8a896fb',1,'muq::SamplingAlgorithms::DRKernel']]],
  ['qoi',['QOI',['../classmuq_1_1SamplingAlgorithms_1_1AbstractSamplingProblem.html#a12cecdea87e30cbbfdf5bc5a63b9666e',1,'muq::SamplingAlgorithms::AbstractSamplingProblem::QOI()'],['../classmuq_1_1SamplingAlgorithms_1_1SamplingProblem.html#af5a8d26b0e188e445879ce2c4b89ee16',1,'muq::SamplingAlgorithms::SamplingProblem::QOI()']]],
  ['quadpts',['QuadPts',['../classmuq_1_1Approximation_1_1PCEFactory.html#a83119fc46932c690feacdc367d5de03f',1,'muq::Approximation::PCEFactory::QuadPts() const'],['../classmuq_1_1Approximation_1_1PCEFactory.html#ab442ab7e200b49b262582f617345163a',1,'muq::Approximation::PCEFactory::QuadPts(std::shared_ptr&lt; muq::Utilities::MultiIndex &gt; const &amp;quadOrders)']]],
  ['quadrature',['Quadrature',['../classmuq_1_1Approximation_1_1Quadrature.html#a7f8e2cb7a145771a85a4d7024b1f2d28',1,'muq::Approximation::Quadrature']]],
  ['quadraturewrapper',['QuadratureWrapper',['../namespacemuq_1_1Approximation_1_1PythonBindings.html#a19e7ef2a252821ff1559286d064a73ef',1,'muq::Approximation::PythonBindings']]],
  ['query',['query',['../structmuq_1_1Modeling_1_1DynamicKDTreeAdaptor.html#ad4515760da292dc19f7930b725e610d2',1,'muq::Modeling::DynamicKDTreeAdaptor']]]
];
