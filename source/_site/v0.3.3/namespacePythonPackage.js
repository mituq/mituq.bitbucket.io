var namespacePythonPackage =
[
    [ "Approximation", "namespacePythonPackage_1_1Approximation.html", null ],
    [ "Modeling", "namespacePythonPackage_1_1Modeling.html", null ],
    [ "Optimization", "namespacePythonPackage_1_1Optimization.html", null ],
    [ "SamplingAlgorithms", "namespacePythonPackage_1_1SamplingAlgorithms.html", null ],
    [ "Utilities", "namespacePythonPackage_1_1Utilities.html", null ]
];