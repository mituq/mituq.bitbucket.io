var classmuq_1_1Modeling_1_1InverseGamma =
[
    [ "InverseGamma", "classmuq_1_1Modeling_1_1InverseGamma.html#aafb4b8e930d1734a42489dbc485f0be0", null ],
    [ "InverseGamma", "classmuq_1_1Modeling_1_1InverseGamma.html#a82fff7809a3594e2ee1364b1f4afd1a1", null ],
    [ "~InverseGamma", "classmuq_1_1Modeling_1_1InverseGamma.html#a38f32670a1ec87c1ebdc4ace6b46d83d", null ],
    [ "ComputeConstant", "classmuq_1_1Modeling_1_1InverseGamma.html#a9d59678df0c8da2b3fc976dc4ee6a5f5", null ],
    [ "LogDensityImpl", "classmuq_1_1Modeling_1_1InverseGamma.html#ab39999c22c4b548feb5f892bd3f2feed", null ],
    [ "SampleImpl", "classmuq_1_1Modeling_1_1InverseGamma.html#ad95513364af5337fe6c381fdec984dcc", null ],
    [ "alpha", "classmuq_1_1Modeling_1_1InverseGamma.html#a5bb36bbebd6c890fb8a51c761dec1adb", null ],
    [ "beta", "classmuq_1_1Modeling_1_1InverseGamma.html#a5a2c3b7620c62d6a65fe22c3bd7ef09f", null ],
    [ "logConst", "classmuq_1_1Modeling_1_1InverseGamma.html#a9e996d741a70f742ac507ddb17fb450a", null ]
];