var classmuq_1_1Utilities_1_1LinearOperator =
[
    [ "LinearOperator", "classmuq_1_1Utilities_1_1LinearOperator.html#afb1dea59908972bde7f3dcdca5c8d51f", null ],
    [ "~LinearOperator", "classmuq_1_1Utilities_1_1LinearOperator.html#a2552a44ea5025dccf90d6163a2223b3d", null ],
    [ "Apply", "classmuq_1_1Utilities_1_1LinearOperator.html#ac9d20a2db4aefbea255531d49b877f24", null ],
    [ "Apply", "classmuq_1_1Utilities_1_1LinearOperator.html#ad607088541876d882f1a927d9256bd28", null ],
    [ "ApplyTranspose", "classmuq_1_1Utilities_1_1LinearOperator.html#ae76fcbbda94d7afd28f38e68657b9f65", null ]
];