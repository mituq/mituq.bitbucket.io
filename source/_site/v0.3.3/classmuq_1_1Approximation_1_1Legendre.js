var classmuq_1_1Approximation_1_1Legendre =
[
    [ "Legendre", "classmuq_1_1Approximation_1_1Legendre.html#ad737127b539de70105d61faa44af5697", null ],
    [ "~Legendre", "classmuq_1_1Approximation_1_1Legendre.html#a283bc361c6329ba311521476226e15cc", null ],
    [ "ak", "classmuq_1_1Approximation_1_1Legendre.html#a0e6c64271bbe5637835741cd5d5cafa7", null ],
    [ "bk", "classmuq_1_1Approximation_1_1Legendre.html#a3a28ae43bb81f91a7f88d6d47c3f1fed", null ],
    [ "ck", "classmuq_1_1Approximation_1_1Legendre.html#a726e4586f946ca77559f446ad0c99dbc", null ],
    [ "DerivativeEvaluate", "classmuq_1_1Approximation_1_1Legendre.html#a1ee70dc4727fccc9b3f723f13b09bbdd", null ],
    [ "Normalization", "classmuq_1_1Approximation_1_1Legendre.html#a894c15f48a6da94cb6d1aba595469003", null ],
    [ "phi0", "classmuq_1_1Approximation_1_1Legendre.html#ad721bd4f564415fbd5617c46e2d57c53", null ],
    [ "phi1", "classmuq_1_1Approximation_1_1Legendre.html#a38dd3f91569de9452b22b479266b4fd8", null ]
];