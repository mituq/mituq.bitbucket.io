var dir_85018323d0241051b6bb402d87eca822 =
[
    [ "AdaptiveSmolyakQuadrature.h", "AdaptiveSmolyakQuadrature_8h.html", [
      [ "AdaptiveSmolyakQuadrature", "classmuq_1_1Approximation_1_1AdaptiveSmolyakQuadrature.html", "classmuq_1_1Approximation_1_1AdaptiveSmolyakQuadrature" ]
    ] ],
    [ "ClenshawCurtisQuadrature.h", "ClenshawCurtisQuadrature_8h.html", null ],
    [ "ExponentialGrowthQuadrature.h", "ExponentialGrowthQuadrature_8h.html", null ],
    [ "FullTensorQuadrature.h", "FullTensorQuadrature_8h.html", null ],
    [ "GaussPattersonQuadrature.h", "GaussPattersonQuadrature_8h.html", null ],
    [ "GaussQuadrature.h", "GaussQuadrature_8h.html", null ],
    [ "Quadrature.h", "Quadrature_8h.html", null ],
    [ "SmolyakQuadrature.h", "SmolyakQuadrature_8h.html", null ]
];