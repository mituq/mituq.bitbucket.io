var namespacemuq =
[
    [ "Approximation", "namespacemuq_1_1Approximation.html", "namespacemuq_1_1Approximation" ],
    [ "Inference", "namespacemuq_1_1Inference.html", "namespacemuq_1_1Inference" ],
    [ "Modeling", "namespacemuq_1_1Modeling.html", "namespacemuq_1_1Modeling" ],
    [ "Optimization", "namespacemuq_1_1Optimization.html", "namespacemuq_1_1Optimization" ],
    [ "SamplingAlgorithms", "namespacemuq_1_1SamplingAlgorithms.html", "namespacemuq_1_1SamplingAlgorithms" ],
    [ "Utilities", "namespacemuq_1_1Utilities.html", "namespacemuq_1_1Utilities" ],
    [ "ExternalLibraryError", "classmuq_1_1ExternalLibraryError.html", "classmuq_1_1ExternalLibraryError" ],
    [ "NotImplementedError", "classmuq_1_1NotImplementedError.html", "classmuq_1_1NotImplementedError" ],
    [ "NotRegisteredError", "classmuq_1_1NotRegisteredError.html", "classmuq_1_1NotRegisteredError" ],
    [ "WrongSizeError", "classmuq_1_1WrongSizeError.html", "classmuq_1_1WrongSizeError" ]
];