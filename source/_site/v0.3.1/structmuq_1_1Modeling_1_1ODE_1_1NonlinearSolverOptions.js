var structmuq_1_1Modeling_1_1ODE_1_1NonlinearSolverOptions =
[
    [ "NonlinearSolverOptions", "structmuq_1_1Modeling_1_1ODE_1_1NonlinearSolverOptions.html#ad1bbaee8e3ba6a81337b1fefe0fc89cc", null ],
    [ "NonlinearSolverOptions", "structmuq_1_1Modeling_1_1ODE_1_1NonlinearSolverOptions.html#a319ae5f70eab1bb503c8a9d007b97f6e", null ],
    [ "SetOptions", "structmuq_1_1Modeling_1_1ODE_1_1NonlinearSolverOptions.html#ac5a48c6661615bb6d2adf5c957b38f59", null ],
    [ "maxIters", "structmuq_1_1Modeling_1_1ODE_1_1NonlinearSolverOptions.html#ad3ced9c91c0a78fe1f26df9f432a2e45", null ],
    [ "method", "structmuq_1_1Modeling_1_1ODE_1_1NonlinearSolverOptions.html#ad5cd7755e4e29f620f196b852e211cbb", null ]
];