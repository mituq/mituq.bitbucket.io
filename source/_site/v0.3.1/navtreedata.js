/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "MUQ", "index.html", [
    [ "MUQ: MIT Uncertainty Quantification Library", "index.html", null ],
    [ "MUQ Installation Guide", "muqinstall.html", null ],
    [ "MUQ Development Style", "muqstyle.html", [
      [ "Uncrustify code formatting", "muqstyle.html#uncrustify", null ],
      [ "C++ style and naming conventions", "muqstyle.html#styleconventions", null ],
      [ "Documentation", "muqstyle.html#documentation", null ]
    ] ],
    [ "Descriptions", "md__Users_rdcrlmdp_Documents_Repositories_muq-all_muq2_SupportScripts_docker_Descriptions.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"AMProposal_8cpp.html",
"DelayedRejection_2python_2EllipticInference_8py.html#a2091282dbdc9ec60be05174468c692b5",
"KroneckerProductOperator_8h.html#a25395788e9cf75e0c3d72039ea0caf1b",
"ParallelAMProposal_8cpp_source.html",
"SumKernel_8cpp.html",
"classmuq_1_1Approximation_1_1ColumnSlice.html#a051e7304deaf986fe7ea19280eccac59",
"classmuq_1_1Approximation_1_1LocalRegression.html#a2a8d2523e8e915169222d8870f3c6a42",
"classmuq_1_1Approximation_1_1SmolyakEstimator.html#aefd86713f08bea9c1c55f30dea5f2b11",
"classmuq_1_1Modeling_1_1ConcatenateOperator.html#ad8c5c91b64869bd7a3fd61e5e67d4075",
"classmuq_1_1Modeling_1_1IdentityPiece.html#a55de756af0a44f282149ae3495e2bd1f",
"classmuq_1_1Modeling_1_1PyGaussianBase.html#a15ac032c6c1819daf8bbf207600cfd45",
"classmuq_1_1Modeling_1_1WorkPiece.html#a672aa406ed921732c65936ef835139c9",
"classmuq_1_1SamplingAlgorithms_1_1DRKernel.html#aee283b22efbc9d0fb88e5f538eb9391f",
"classmuq_1_1SamplingAlgorithms_1_1SLMCMC.html#acf34012b18c658f63dd76bf11f5f0ab4",
"classmuq_1_1Utilities_1_1HDF5File.html#a21ad214e2d04b9993a63b266178486b9",
"dir_7936de9f533306ff83980b4ce81a1bc9.html",
"structPySwigObject.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';