var structmuq_1_1Approximation_1_1SmolyakEstimator_1_1SmolyTerm =
[
    [ "diffWeights", "structmuq_1_1Approximation_1_1SmolyakEstimator_1_1SmolyTerm.html#a3b0bab532fc9f081ae8db40e74af6410", null ],
    [ "evalInds", "structmuq_1_1Approximation_1_1SmolyakEstimator_1_1SmolyTerm.html#aec1bf0810cce271b8a8a9916d3aba8a7", null ],
    [ "isComputed", "structmuq_1_1Approximation_1_1SmolyakEstimator_1_1SmolyTerm.html#a2b385669da7b3eb6b252646a878fcfdf", null ],
    [ "isNeeded", "structmuq_1_1Approximation_1_1SmolyakEstimator_1_1SmolyTerm.html#a8c8a94b338873053a0eb737ff924cea1", null ],
    [ "isOld", "structmuq_1_1Approximation_1_1SmolyakEstimator_1_1SmolyTerm.html#a29ee79601ac6733b5c99041e9a89f770", null ],
    [ "localError", "structmuq_1_1Approximation_1_1SmolyakEstimator_1_1SmolyTerm.html#ad171580c78774ddf24c2c67bd686da99", null ],
    [ "val", "structmuq_1_1Approximation_1_1SmolyakEstimator_1_1SmolyTerm.html#adea411929be0c6b095459f092fd04568", null ],
    [ "weight", "structmuq_1_1Approximation_1_1SmolyakEstimator_1_1SmolyTerm.html#a70812f9e9843a78dd929704a931a1d0e", null ]
];