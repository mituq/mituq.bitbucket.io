var classmuq_1_1Approximation_1_1Regression =
[
    [ "PoisednessConstraint", "classmuq_1_1Approximation_1_1Regression_1_1PoisednessConstraint.html", "classmuq_1_1Approximation_1_1Regression_1_1PoisednessConstraint" ],
    [ "PoisednessCost", "classmuq_1_1Approximation_1_1Regression_1_1PoisednessCost.html", "classmuq_1_1Approximation_1_1Regression_1_1PoisednessCost" ],
    [ "Regression", "classmuq_1_1Approximation_1_1Regression.html#aae89f46ec6f6645e68a6084b833c6dc2", null ],
    [ "CenterPoints", "classmuq_1_1Approximation_1_1Regression.html#a7782cb4c22e2490ac216ec3cec9690ef", null ],
    [ "CenterPoints", "classmuq_1_1Approximation_1_1Regression.html#a92ae05a2ef795de5705f0d38a6a4fbf6", null ],
    [ "CenterPoints", "classmuq_1_1Approximation_1_1Regression.html#a42fc5d90406e4c959a705792eb289ed9", null ],
    [ "ComputeBasisDerivatives", "classmuq_1_1Approximation_1_1Regression.html#a13fade02691f975254b9a5f94fc5305c", null ],
    [ "ComputeCoefficients", "classmuq_1_1Approximation_1_1Regression.html#a5e416b126196f2a09e203fe470ffd8c9", null ],
    [ "ComputeCoefficientsRHS", "classmuq_1_1Approximation_1_1Regression.html#aabaec61c216136644fd7af901d70a02b", null ],
    [ "EvaluateImpl", "classmuq_1_1Approximation_1_1Regression.html#a16b80c41c9cc5dcf810a47017fb45b00", null ],
    [ "Fit", "classmuq_1_1Approximation_1_1Regression.html#aac40109e72d0ee447af2c1c30fbc3771", null ],
    [ "Fit", "classmuq_1_1Approximation_1_1Regression.html#a1f99a8e4152f9d3f8d4297c853854275", null ],
    [ "NumInterpolationPoints", "classmuq_1_1Approximation_1_1Regression.html#a67cb957cd7288b0865ce45a959993afe", null ],
    [ "PoisednessConstant", "classmuq_1_1Approximation_1_1Regression.html#ab024db2f6761a743b434f02ac4ee3ba8", null ],
    [ "VandermondeMatrix", "classmuq_1_1Approximation_1_1Regression.html#abf9f1bca3d4b7c28f53d3efbbdd9fea2", null ],
    [ "alpha", "classmuq_1_1Approximation_1_1Regression.html#a0f8fe31e0c2b1d9db8cab27fb45b3f7f", null ],
    [ "coeff", "classmuq_1_1Approximation_1_1Regression.html#aad1a5b6710b8c1c8232aa0fb9ed05c72", null ],
    [ "currentCenter", "classmuq_1_1Approximation_1_1Regression.html#ac1a0131da6d982acb6a3c32f30b5b4d7", null ],
    [ "currentRadius", "classmuq_1_1Approximation_1_1Regression.html#a021f0eb892555c2b1b123fe7e87247af", null ],
    [ "inputDim", "classmuq_1_1Approximation_1_1Regression.html#a7c8c4f4a8345e4289bc472cfff0bf592", null ],
    [ "multi", "classmuq_1_1Approximation_1_1Regression.html#ad8dbcb2cc95fafbd90a430bcb6b564c6", null ],
    [ "optPt", "classmuq_1_1Approximation_1_1Regression.html#a03ca9df72c178314ca35a65442fb954d", null ],
    [ "order", "classmuq_1_1Approximation_1_1Regression.html#a42af88c10d2c36d262bb1fb00b405e8a", null ],
    [ "poly", "classmuq_1_1Approximation_1_1Regression.html#a5021c97286ec7c5b342bf37029f88ed7", null ]
];