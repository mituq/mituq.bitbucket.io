var index =
[
    [ "Purpose", "index.html#autotoc_md37", null ],
    [ "Installation:", "index.html#autotoc_md38", null ],
    [ "Getting Started", "index.html#autotoc_md39", null ],
    [ "Citing", "index.html#autotoc_md43", null ],
    [ "Contributing", "index.html#autotoc_md44", null ],
    [ "Developer Infrastructure Notes", "infrastructure.html", null ],
    [ "Development Style Guide", "muqstyle.html", [
      [ "C++ style and naming conventions", "muqstyle.html#styleconventions", null ],
      [ "Documentation", "muqstyle.html#documentation", null ]
    ] ]
];