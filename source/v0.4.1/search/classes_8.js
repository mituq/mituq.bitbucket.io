var searchData=
[
  ['identityoperator_2408',['IdentityOperator',['../classmuq_1_1Modeling_1_1IdentityOperator.html',1,'muq::Modeling']]],
  ['identitypiece_2409',['IdentityPiece',['../classmuq_1_1Modeling_1_1IdentityPiece.html',1,'muq::Modeling']]],
  ['importancesampling_2410',['ImportanceSampling',['../classmuq_1_1SamplingAlgorithms_1_1ImportanceSampling.html',1,'muq::SamplingAlgorithms']]],
  ['indexedscalarbasis_2411',['IndexedScalarBasis',['../classmuq_1_1Approximation_1_1IndexedScalarBasis.html',1,'muq::Approximation']]],
  ['indexscalarbasis_2412',['IndexScalarBasis',['../classIndexScalarBasis.html',1,'']]],
  ['infmalaproposal_2413',['InfMALAProposal',['../classmuq_1_1SamplingAlgorithms_1_1InfMALAProposal.html',1,'muq::SamplingAlgorithms']]],
  ['integratoroptions_2414',['IntegratorOptions',['../structmuq_1_1Modeling_1_1ODE_1_1IntegratorOptions.html',1,'muq::Modeling::ODE::IntegratorOptions'],['../classODE_1_1IntegratorOptions.html',1,'IntegratorOptions']]],
  ['interface_2415',['Interface',['../structmuq_1_1Modeling_1_1ODE_1_1Interface.html',1,'muq::Modeling::ODE']]],
  ['inversegamma_2416',['InverseGamma',['../classmuq_1_1Modeling_1_1InverseGamma.html',1,'muq::Modeling']]],
  ['inversegammaproposal_2417',['InverseGammaProposal',['../classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html',1,'muq::SamplingAlgorithms']]],
  ['iskernel_2418',['ISKernel',['../classmuq_1_1SamplingAlgorithms_1_1ISKernel.html',1,'muq::SamplingAlgorithms']]]
];
