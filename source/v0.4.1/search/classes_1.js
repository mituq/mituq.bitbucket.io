var searchData=
[
  ['basisexpansion_2335',['BasisExpansion',['../classmuq_1_1Approximation_1_1BasisExpansion.html',1,'muq::Approximation']]],
  ['blockdataset_2336',['BlockDataset',['../classmuq_1_1Utilities_1_1BlockDataset.html',1,'muq::Utilities']]],
  ['blockdiagonaloperator_2337',['BlockDiagonalOperator',['../classmuq_1_1Modeling_1_1BlockDiagonalOperator.html',1,'muq::Modeling']]],
  ['blockrowoperator_2338',['BlockRowOperator',['../classmuq_1_1Modeling_1_1BlockRowOperator.html',1,'muq::Modeling']]],
  ['boostanyserializer_2339',['BoostAnySerializer',['../classcereal_1_1BoostAnySerializer.html',1,'cereal']]]
];
