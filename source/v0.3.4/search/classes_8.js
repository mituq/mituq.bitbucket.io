var searchData=
[
  ['identityoperator_2154',['IdentityOperator',['../classmuq_1_1Modeling_1_1IdentityOperator.html',1,'muq::Modeling']]],
  ['identitypiece_2155',['IdentityPiece',['../classmuq_1_1Modeling_1_1IdentityPiece.html',1,'muq::Modeling']]],
  ['importancesampling_2156',['ImportanceSampling',['../classmuq_1_1SamplingAlgorithms_1_1ImportanceSampling.html',1,'muq::SamplingAlgorithms']]],
  ['indexedscalarbasis_2157',['IndexedScalarBasis',['../classmuq_1_1Approximation_1_1IndexedScalarBasis.html',1,'muq::Approximation']]],
  ['indexscalarbasis_2158',['IndexScalarBasis',['../classIndexScalarBasis.html',1,'']]],
  ['infmalaproposal_2159',['InfMALAProposal',['../classmuq_1_1SamplingAlgorithms_1_1InfMALAProposal.html',1,'muq::SamplingAlgorithms']]],
  ['integratoroptions_2160',['IntegratorOptions',['../structmuq_1_1Modeling_1_1ODE_1_1IntegratorOptions.html',1,'muq::Modeling::ODE::IntegratorOptions'],['../classODE_1_1IntegratorOptions.html',1,'IntegratorOptions']]],
  ['interface_2161',['Interface',['../structmuq_1_1Modeling_1_1ODE_1_1Interface.html',1,'muq::Modeling::ODE']]],
  ['inversegamma_2162',['InverseGamma',['../classmuq_1_1Modeling_1_1InverseGamma.html',1,'muq::Modeling']]],
  ['inversegammaproposal_2163',['InverseGammaProposal',['../classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html',1,'muq::SamplingAlgorithms']]],
  ['iskernel_2164',['ISKernel',['../classmuq_1_1SamplingAlgorithms_1_1ISKernel.html',1,'muq::SamplingAlgorithms']]]
];
