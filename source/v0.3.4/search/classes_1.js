var searchData=
[
  ['basisexpansion_2084',['BasisExpansion',['../classmuq_1_1Approximation_1_1BasisExpansion.html',1,'muq::Approximation']]],
  ['blockdataset_2085',['BlockDataset',['../classmuq_1_1Utilities_1_1BlockDataset.html',1,'muq::Utilities']]],
  ['blockdiagonaloperator_2086',['BlockDiagonalOperator',['../classmuq_1_1Modeling_1_1BlockDiagonalOperator.html',1,'muq::Modeling']]],
  ['blockrowoperator_2087',['BlockRowOperator',['../classmuq_1_1Modeling_1_1BlockRowOperator.html',1,'muq::Modeling']]],
  ['boostanyserializer_2088',['BoostAnySerializer',['../classcereal_1_1BoostAnySerializer.html',1,'cereal']]]
];
