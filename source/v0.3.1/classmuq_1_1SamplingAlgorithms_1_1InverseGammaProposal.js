var classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal =
[
    [ "InverseGammaProposal", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a94724af04fcd33d1f64080e8c3444cca", null ],
    [ "~InverseGammaProposal", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a056ed4021517f6d73c3556aed1d8febc", null ],
    [ "ExtractAlpha", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#ae8e1ef0779cd759ff8dca1dd6b2a77cc", null ],
    [ "ExtractBeta", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#ac7c33d78dbd9624d8dcf34302c33ea91", null ],
    [ "ExtractGaussInd", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a3ec09d7ccebdcd305de41d9457ee7f9c", null ],
    [ "ExtractInverseGamma", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#af570dd86bb426833fdd04f09a2799c42", null ],
    [ "ExtractMean", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#aae40d90db0437ed6f8cd1e5fe7297811", null ],
    [ "LogDensity", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#ac01193bbebbca17c6b07e99838b3aaac", null ],
    [ "Sample", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a73b37a6a7886e11f2bac337b39a2cf22", null ],
    [ "alpha", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a05599735d7932de9a6e2e4f31b83ed9e", null ],
    [ "beta", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a7d0d56a5c83984daeb35e4f26a9db920", null ],
    [ "gaussBlock", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#aa9878a2636baf60b4b9cbdaad3c3d81d", null ],
    [ "gaussMean", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#ae9395e76ca4f5cc00ba0cc1b6e2f8f07", null ]
];