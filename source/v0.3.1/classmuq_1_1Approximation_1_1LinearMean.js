var classmuq_1_1Approximation_1_1LinearMean =
[
    [ "LinearMean", "classmuq_1_1Approximation_1_1LinearMean.html#a96a9f764cf9de50b947bd11970107105", null ],
    [ "~LinearMean", "classmuq_1_1Approximation_1_1LinearMean.html#a086d460de30672c039774c5d53290f17", null ],
    [ "LinearMean", "classmuq_1_1Approximation_1_1LinearMean.html#ad3b404001f5360dff382603950ff0ea6", null ],
    [ "Clone", "classmuq_1_1Approximation_1_1LinearMean.html#a725e04f4cb9c520c4cfe5650a60a4f1e", null ],
    [ "Evaluate", "classmuq_1_1Approximation_1_1LinearMean.html#aff2ddfb515474b33a27a27a3f80f51e4", null ],
    [ "GetDerivative", "classmuq_1_1Approximation_1_1LinearMean.html#aec9e445680e6270ecf9375ed56fb48c8", null ],
    [ "intercepts", "classmuq_1_1Approximation_1_1LinearMean.html#a3595b95d596e2985a1ffdd5a0539f7b3", null ],
    [ "slopes", "classmuq_1_1Approximation_1_1LinearMean.html#a075d5e1383811168b60eda0fae305c15", null ]
];