var searchData=
[
  ['basisexpansion',['BasisExpansion',['../classmuq_1_1Approximation_1_1BasisExpansion.html',1,'muq::Approximation']]],
  ['beammodel',['BeamModel',['../classBeamModel_1_1BeamModel.html',1,'BeamModel']]],
  ['blockdataset',['BlockDataset',['../classmuq_1_1Utilities_1_1BlockDataset.html',1,'muq::Utilities']]],
  ['blockdiagonaloperator',['BlockDiagonalOperator',['../classmuq_1_1Modeling_1_1BlockDiagonalOperator.html',1,'muq::Modeling']]],
  ['blockrowoperator',['BlockRowOperator',['../classmuq_1_1Modeling_1_1BlockRowOperator.html',1,'muq::Modeling']]],
  ['boostanyserializer',['BoostAnySerializer',['../classcereal_1_1BoostAnySerializer.html',1,'cereal']]]
];
