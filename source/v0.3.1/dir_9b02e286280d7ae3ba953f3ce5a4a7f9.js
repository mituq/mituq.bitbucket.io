var dir_9b02e286280d7ae3ba953f3ce5a4a7f9 =
[
    [ "BasisExpansion.h", "BasisExpansion_8h.html", null ],
    [ "HermiteFunction.h", "HermiteFunction_8h.html", null ],
    [ "IndexedScalarBasis.h", "IndexedScalarBasis_8h.html", "IndexedScalarBasis_8h" ],
    [ "Jacobi.h", "Jacobi_8h.html", null ],
    [ "Laguerre.h", "Laguerre_8h.html", null ],
    [ "Legendre.h", "Legendre_8h.html", null ],
    [ "Monomial.h", "Monomial_8h.html", null ],
    [ "MonotoneExpansion.h", "MonotoneExpansion_8h.html", null ],
    [ "OrthogonalPolynomial.h", "OrthogonalPolynomial_8h.html", null ],
    [ "PhysicistHermite.h", "PhysicistHermite_8h.html", [
      [ "PhysicistHermite", "classmuq_1_1Approximation_1_1PhysicistHermite.html", "classmuq_1_1Approximation_1_1PhysicistHermite" ]
    ] ],
    [ "ProbabilistHermite.h", "ProbabilistHermite_8h.html", [
      [ "ProbabilistHermite", "classmuq_1_1Approximation_1_1ProbabilistHermite.html", "classmuq_1_1Approximation_1_1ProbabilistHermite" ]
    ] ]
];