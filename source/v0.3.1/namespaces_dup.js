var namespaces_dup =
[
    [ "BeamModel", "namespaceBeamModel.html", null ],
    [ "cereal", "namespacecereal.html", null ],
    [ "Cpp2Html", "namespaceCpp2Html.html", null ],
    [ "CrankNicolson", "namespaceCrankNicolson.html", null ],
    [ "CustomModPiece", "namespaceCustomModPiece.html", null ],
    [ "EllipticInference", "namespaceEllipticInference.html", null ],
    [ "ModPieceMemory", "namespaceModPieceMemory.html", null ],
    [ "muq", "namespacemuq.html", "namespacemuq" ],
    [ "ODE", "namespaceODE.html", null ],
    [ "PlotResults", "namespacePlotResults.html", null ],
    [ "PlotSamples", "namespacePlotSamples.html", null ],
    [ "PreProcess", "namespacePreProcess.html", null ],
    [ "ProcessData", "namespaceProcessData.html", null ],
    [ "pybind11", "namespacepybind11.html", "namespacepybind11" ],
    [ "pymuqModeling", "namespacepymuqModeling.html", "namespacepymuqModeling" ],
    [ "PythonPackage", "namespacePythonPackage.html", "namespacePythonPackage" ],
    [ "TheisInference", "namespaceTheisInference.html", null ],
    [ "TheisModel", "namespaceTheisModel.html", null ]
];