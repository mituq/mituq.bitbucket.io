var group__MeanFunctions =
[
    [ "MeanFunctionBase", "classmuq_1_1Approximation_1_1MeanFunctionBase.html", [
      [ "MeanFunctionBase", "classmuq_1_1Approximation_1_1MeanFunctionBase.html#a88c02777719d6b4ad97c81e5513de096", null ],
      [ "~MeanFunctionBase", "classmuq_1_1Approximation_1_1MeanFunctionBase.html#a4736b8629267e3b4ed1e9dff74489d38", null ],
      [ "Clone", "classmuq_1_1Approximation_1_1MeanFunctionBase.html#ab034800143dfc581d94986765d28eab4", null ],
      [ "Evaluate", "classmuq_1_1Approximation_1_1MeanFunctionBase.html#afd8f157f9eeea6e97ef8bac46e954d01", null ],
      [ "GetDerivative", "classmuq_1_1Approximation_1_1MeanFunctionBase.html#a28361055c1569236ce583d0f03f35f05", null ],
      [ "GetPtr", "classmuq_1_1Approximation_1_1MeanFunctionBase.html#ae0dd9979b65c8c1b949f59f6cc1a76a7", null ],
      [ "coDim", "classmuq_1_1Approximation_1_1MeanFunctionBase.html#a3aad5ac366ff51a576af9615a6741b5d", null ],
      [ "inputDim", "classmuq_1_1Approximation_1_1MeanFunctionBase.html#a3867fae12eb13c1d34b0a4a6028857a4", null ]
    ] ],
    [ "ConstantMean", "classConstantMean.html", null ],
    [ "LinearTransformMean", "classmuq_1_1Approximation_1_1LinearTransformMean.html", [
      [ "LinearTransformMean", "classmuq_1_1Approximation_1_1LinearTransformMean.html#a76e7d63c992c2555e427f8f46c8b0de4", null ],
      [ "~LinearTransformMean", "classmuq_1_1Approximation_1_1LinearTransformMean.html#a63067763acdb090d42c5ecce3b34b1b7", null ],
      [ "Clone", "classmuq_1_1Approximation_1_1LinearTransformMean.html#a3dd6841c74f2747ee667a01dd0550a49", null ],
      [ "Evaluate", "classmuq_1_1Approximation_1_1LinearTransformMean.html#aa540a1ebf8ae09c2b0ae2141b6c837d0", null ],
      [ "GetDerivative", "classmuq_1_1Approximation_1_1LinearTransformMean.html#a810736067aeb3e81a7636a76d5b38db6", null ],
      [ "A", "classmuq_1_1Approximation_1_1LinearTransformMean.html#a5ed15948f5826850c60609fa4fc9fbe0", null ],
      [ "otherMean", "classmuq_1_1Approximation_1_1LinearTransformMean.html#a89f49d1c8fb90d6c2cc5c8e56d2504b2", null ]
    ] ],
    [ "SumMean", "classmuq_1_1Approximation_1_1SumMean.html", [
      [ "SumMean", "classmuq_1_1Approximation_1_1SumMean.html#ad7a9f1369b35207cf3c2b8bd3eaff77a", null ],
      [ "~SumMean", "classmuq_1_1Approximation_1_1SumMean.html#ae4ff15e7926abc82e4dbe382f2b83067", null ],
      [ "Clone", "classmuq_1_1Approximation_1_1SumMean.html#af09b87a213258bb82001ac52b35df4bc", null ],
      [ "Evaluate", "classmuq_1_1Approximation_1_1SumMean.html#ae93ee6b976ff8a6861dad34e7843b732", null ],
      [ "GetDerivative", "classmuq_1_1Approximation_1_1SumMean.html#a5255ad55111c560d2131827455642cd5", null ],
      [ "mu1", "classmuq_1_1Approximation_1_1SumMean.html#aa8e0a8f429c9c6464fd3847cdb961d6d", null ],
      [ "mu2", "classmuq_1_1Approximation_1_1SumMean.html#aec2537d6bbdc81af8f741080115d962f", null ]
    ] ]
];