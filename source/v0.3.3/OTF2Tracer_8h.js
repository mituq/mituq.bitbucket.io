var OTF2Tracer_8h =
[
    [ "OTF2TracerBase", "classmuq_1_1Utilities_1_1OTF2TracerBase.html", "classmuq_1_1Utilities_1_1OTF2TracerBase" ],
    [ "OTF2TracerDummy", "classmuq_1_1Utilities_1_1OTF2TracerDummy.html", "classmuq_1_1Utilities_1_1OTF2TracerDummy" ],
    [ "OTF2Tracer", "OTF2Tracer_8h.html#a6499e11c8b95ea5214825d48950d3e3f", null ],
    [ "TracerRegions", "OTF2Tracer_8h.html#ae8643a321356f52a41b83005b8c8e2db", [
      [ "Setup", "OTF2Tracer_8h.html#ae8643a321356f52a41b83005b8c8e2dbab4e1f9ddd79f8bf55f3b82a5c10a7345", null ],
      [ "Finalize", "OTF2Tracer_8h.html#ae8643a321356f52a41b83005b8c8e2dbaf5450eff994b69208493eb5c5c233c00", null ],
      [ "PhonebookBusy", "OTF2Tracer_8h.html#ae8643a321356f52a41b83005b8c8e2dba3b0a2459ac077c4a5fddcd01f7004784", null ],
      [ "BurnIn", "OTF2Tracer_8h.html#ae8643a321356f52a41b83005b8c8e2dbae6a890a0ff7ea33ffb36af8ea04ef5ee", null ],
      [ "Sampling", "OTF2Tracer_8h.html#ae8643a321356f52a41b83005b8c8e2dbaa39476daf539e1b99312f3e376b0ce89", null ],
      [ "CollectorBusy", "OTF2Tracer_8h.html#ae8643a321356f52a41b83005b8c8e2dbad77e744e15387f1ec143338885a9b1a3", null ],
      [ "RetrievingProposal", "OTF2Tracer_8h.html#ae8643a321356f52a41b83005b8c8e2dba814873107352a686f11a3ff920d541bb", null ],
      [ "FetchingProposal", "OTF2Tracer_8h.html#ae8643a321356f52a41b83005b8c8e2dba3aa6fdf5d2706fb4e7663633a9dd6d48", null ]
    ] ]
];