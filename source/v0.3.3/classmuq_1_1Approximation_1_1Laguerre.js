var classmuq_1_1Approximation_1_1Laguerre =
[
    [ "Laguerre", "classmuq_1_1Approximation_1_1Laguerre.html#a91553def5889a7448aa4914be28db027", null ],
    [ "~Laguerre", "classmuq_1_1Approximation_1_1Laguerre.html#acd1471f3af82e055b6af388891a480e5", null ],
    [ "ak", "classmuq_1_1Approximation_1_1Laguerre.html#a000da6502f8864f573386c5e0da6504c", null ],
    [ "bk", "classmuq_1_1Approximation_1_1Laguerre.html#a1e313425dff209f627547b24206c8e02", null ],
    [ "ck", "classmuq_1_1Approximation_1_1Laguerre.html#abd7db143b5400110430c5d5d4a014991", null ],
    [ "DerivativeEvaluate", "classmuq_1_1Approximation_1_1Laguerre.html#afce3e6e91274c7f221e10ac644034f92", null ],
    [ "Normalization", "classmuq_1_1Approximation_1_1Laguerre.html#a9895a77a2e195ab67c99caa330a4330b", null ],
    [ "phi0", "classmuq_1_1Approximation_1_1Laguerre.html#ab1d55e6c9e9195b52051b403994d79f1", null ],
    [ "phi1", "classmuq_1_1Approximation_1_1Laguerre.html#a5a9e0a62c4e2aa8952f68bed23704cdd", null ],
    [ "a", "classmuq_1_1Approximation_1_1Laguerre.html#a56cf84548286ee959711191487c67b9b", null ]
];