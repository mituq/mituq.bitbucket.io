var group__Exceptions =
[
    [ "NotImplementedError", "classmuq_1_1NotImplementedError.html", [
      [ "NotImplementedError", "classmuq_1_1NotImplementedError.html#a8334e916bc46caf4766d75437b76675d", null ]
    ] ],
    [ "NotRegisteredError", "classmuq_1_1NotRegisteredError.html", [
      [ "NotRegisteredError", "classmuq_1_1NotRegisteredError.html#a4ff2154bfea8e4fcb5dab68a91909d87", null ]
    ] ],
    [ "ExternalLibraryError", "classmuq_1_1ExternalLibraryError.html", [
      [ "ExternalLibraryError", "classmuq_1_1ExternalLibraryError.html#ad5de5253b6a1f7a68513baef6c3376d6", null ]
    ] ],
    [ "WrongSizeError", "classmuq_1_1WrongSizeError.html", [
      [ "WrongSizeError", "classmuq_1_1WrongSizeError.html#a8691fc6b7b6ac8bfd82b74c407060e65", null ]
    ] ]
];