var VariadicMacros_8h =
[
    [ "argument_type", "structargument__type.html", null ],
    [ "argument_type< T(U)>", "structargument__type_3_01T_07U_08_4.html", "structargument__type_3_01T_07U_08_4" ],
    [ "STATIC_VARIADIC_TO_VECTOR", "VariadicMacros_8h.html#a53abc827eddfee0a69bf21f45155862c", null ],
    [ "STATIC_VARIADIC_TO_VECTOR_PART1", "VariadicMacros_8h.html#a859b2f0a6a55d9d070e9b2f1bacf0104", null ],
    [ "STATIC_VARIADIC_TO_VECTOR_PART2", "VariadicMacros_8h.html#a3a0a5633fb1fe18e2fda5355e6b4fabd", null ],
    [ "VARIADIC_TO_REFVECTOR", "VariadicMacros_8h.html#ac0530b0644d1c4a2a10372bd94f44a43", null ],
    [ "VARIADIC_TO_VECTOR", "VariadicMacros_8h.html#a1ad731facc817d81531f1db5b70f1f8a", null ]
];