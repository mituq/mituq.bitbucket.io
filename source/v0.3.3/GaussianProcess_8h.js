var GaussianProcess_8h =
[
    [ "OptInfo", "structmuq_1_1Approximation_1_1OptInfo.html", "structmuq_1_1Approximation_1_1OptInfo" ],
    [ "ZeroMean", "classmuq_1_1Approximation_1_1ZeroMean.html", "classmuq_1_1Approximation_1_1ZeroMean" ],
    [ "LinearMean", "classmuq_1_1Approximation_1_1LinearMean.html", "classmuq_1_1Approximation_1_1LinearMean" ],
    [ "ConstructGP", "GaussianProcess_8h.html#ga22b20205733bbe63496886bac8610386", null ],
    [ "nlopt_obj", "GaussianProcess_8h.html#a9b76219175c5abd212375df84223ebdd", null ],
    [ "operator*", "GaussianProcess_8h.html#a30acddb3998759f9b88bdb1c2690eaec", null ],
    [ "operator+", "GaussianProcess_8h.html#aad9bd8ea8f4eb21c82c7e3e57a2cdc1d", null ]
];