var classmuq_1_1SamplingAlgorithms_1_1AverageHessian =
[
    [ "AverageHessian", "classmuq_1_1SamplingAlgorithms_1_1AverageHessian.html#af9e9dbbdd88be6e0cda6712569e7c2d9", null ],
    [ "Apply", "classmuq_1_1SamplingAlgorithms_1_1AverageHessian.html#a1a74f58e5f76b794b4351775963b9284", null ],
    [ "ApplyTranspose", "classmuq_1_1SamplingAlgorithms_1_1AverageHessian.html#addc7c0dad47ada7bc02ffd28921d0804", null ],
    [ "newHess", "classmuq_1_1SamplingAlgorithms_1_1AverageHessian.html#a6445ae365763d07048180de03237a0b0", null ],
    [ "numSamps", "classmuq_1_1SamplingAlgorithms_1_1AverageHessian.html#af1a3acff672e7204da10435388c5bb21", null ],
    [ "oldEigVals", "classmuq_1_1SamplingAlgorithms_1_1AverageHessian.html#a88a376513cd1e74c29fb34b104998d87", null ],
    [ "oldU", "classmuq_1_1SamplingAlgorithms_1_1AverageHessian.html#aa1b76f92a79fdb1c3c5ef4481b1e1069", null ],
    [ "oldW", "classmuq_1_1SamplingAlgorithms_1_1AverageHessian.html#acc4eaa37518bb2bdb46810bfc58d96e3", null ]
];