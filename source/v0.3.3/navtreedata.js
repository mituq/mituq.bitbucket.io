var NAVTREE =
[
  [ "MUQ", "index.html", [
    [ "MUQ: MIT Uncertainty Quantification Library", "index.html", null ],
    [ "Installation", "muqinstall.html", "muqinstall" ],
    [ "Development Style Guide", "muqstyle.html", [
      [ "C++ style and naming conventions", "muqstyle.html#styleconventions", null ],
      [ "Documentation", "muqstyle.html#documentation", null ]
    ] ],
    [ "MUQ Infrastructure", "md__opt_atlassian_pipelines_agent_build_documentation_infrastructure.html", null ],
    [ "paper", "md__opt_atlassian_pipelines_agent_build_joss_paper.html", null ],
    [ "Descriptions", "md__opt_atlassian_pipelines_agent_build_SupportScripts_docker_Descriptions.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"AMProposal_8cpp.html",
"DILI_2python_2EllipticInference_8py.html#a6e22366bf7c8d6c950697c0d221983c8",
"Jacobi_8cpp.html",
"NodeNameFinder_8cpp_source.html",
"RootfindingIVP_8h.html",
"classMCSampleProposal.html#a3782444839cb3ede47bfda4d301bd332",
"classmuq_1_1Approximation_1_1IndexedScalarBasis.html#adb67f6bd78f842e4989ab4796ead852a",
"classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html#a6084acd607a09a226084fa62516f75d7",
"classmuq_1_1Modeling_1_1AnyAlgebra.html#a93bc61a5fd97f837e1a8003009e6f11f",
"classmuq_1_1Modeling_1_1FlannCache.html#ada5b117326176d6805c41919b5ec1179",
"classmuq_1_1Modeling_1_1ModPiece.html#a3721c6df37965f23608149d859338605",
"classmuq_1_1Modeling_1_1SundialsAlgebra.html",
"classmuq_1_1SamplingAlgorithms_1_1AMProposal.html#a3091224fae61e3c51f5480a96c553602",
"classmuq_1_1SamplingAlgorithms_1_1LIS2Full.html#a6f34a530cf095c1319bef3db5c93b6c1",
"classmuq_1_1SamplingAlgorithms_1_1SingleChainMCMC.html",
"classmuq_1_1Utilities_1_1MultiIndexSet.html#a6e58445da578cfbc16a9e41bc3043f8f",
"feature__tests_8c.html",
"structmuq_1_1Modeling_1_1DynamicKDTreeAdaptor.html#aa94fc6d64a3283341a0dec332e2e9060"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';