var classmuq_1_1Approximation_1_1LinearTransformMean =
[
    [ "LinearTransformMean", "classmuq_1_1Approximation_1_1LinearTransformMean.html#a76e7d63c992c2555e427f8f46c8b0de4", null ],
    [ "~LinearTransformMean", "classmuq_1_1Approximation_1_1LinearTransformMean.html#a63067763acdb090d42c5ecce3b34b1b7", null ],
    [ "Clone", "classmuq_1_1Approximation_1_1LinearTransformMean.html#a3dd6841c74f2747ee667a01dd0550a49", null ],
    [ "Evaluate", "classmuq_1_1Approximation_1_1LinearTransformMean.html#aa540a1ebf8ae09c2b0ae2141b6c837d0", null ],
    [ "GetDerivative", "classmuq_1_1Approximation_1_1LinearTransformMean.html#a810736067aeb3e81a7636a76d5b38db6", null ],
    [ "A", "classmuq_1_1Approximation_1_1LinearTransformMean.html#a5ed15948f5826850c60609fa4fc9fbe0", null ],
    [ "otherMean", "classmuq_1_1Approximation_1_1LinearTransformMean.html#a89f49d1c8fb90d6c2cc5c8e56d2504b2", null ]
];