var classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature =
[
    [ "ClenshawCurtisQuadrature", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#ac99c242b906ae524e3a5d248edb7a66c", null ],
    [ "~ClenshawCurtisQuadrature", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#ab27f2057cd2d6245b100d7172492a63c", null ],
    [ "Compute", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#aea592b9e9e22c3c2678a408be9a1be01", null ],
    [ "Exactness", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#aa81424e20f0fa966d9c5ecec7d7170b1", null ],
    [ "IndexToNumPoints", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#ace4baa4e341133fd5f09731f63d7a10c", null ],
    [ "nested", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#a969b297a57452b272ef3e5d626aa26dd", null ],
    [ "pi", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html#af427222dc1d8e3d0d814a233dc33e968", null ]
];