var classmuq_1_1Approximation_1_1GaussPattersonQuadrature =
[
    [ "GaussPattersonQuadrature", "classmuq_1_1Approximation_1_1GaussPattersonQuadrature.html#a0ed0ab4f77c7c14a5a5c91eda0a4fbdc", null ],
    [ "~GaussPattersonQuadrature", "classmuq_1_1Approximation_1_1GaussPattersonQuadrature.html#a3422f533beabe10d7f04b568088beab2", null ],
    [ "Compute", "classmuq_1_1Approximation_1_1GaussPattersonQuadrature.html#abd9ba2e9fd041b6df9c867a6d1dbd5d7", null ],
    [ "Exactness", "classmuq_1_1Approximation_1_1GaussPattersonQuadrature.html#a60306a142981eeb8e3075c95387f4129", null ]
];