var modules =
[
    [ "Gaussian Processes", "group__GaussianProcesses.html", "group__GaussianProcesses" ],
    [ "Polynomials", "group__Polynomials.html", "group__Polynomials" ],
    [ "Quadrature", "group__Quadrature.html", "group__Quadrature" ],
    [ "Modeling", "group__Modeling.html", "group__Modeling" ],
    [ "SamplingAlgorithms", "group__SamplingAlgorithms.html", "group__SamplingAlgorithms" ],
    [ "Exceptions", "group__Exceptions.html", "group__Exceptions" ]
];