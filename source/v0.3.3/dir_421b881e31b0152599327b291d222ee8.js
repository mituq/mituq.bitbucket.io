var dir_421b881e31b0152599327b291d222ee8 =
[
    [ "Approximation", "dir_a9cd744b72026129dcbb0ac0ff5375d9.html", "dir_a9cd744b72026129dcbb0ac0ff5375d9" ],
    [ "Inference", "dir_950b21aafeca9d137b5c6341ed7d5bee.html", "dir_950b21aafeca9d137b5c6341ed7d5bee" ],
    [ "Modeling", "dir_0a77aa6bce8c8bb59d26a4a1207bdc36.html", "dir_0a77aa6bce8c8bb59d26a4a1207bdc36" ],
    [ "Optimization", "dir_9903f05be4b8e7c86d6e4bd2f4b4b087.html", "dir_9903f05be4b8e7c86d6e4bd2f4b4b087" ],
    [ "SamplingAlgorithms", "dir_e83a334867f5e20fbd211672f2823232.html", "dir_e83a334867f5e20fbd211672f2823232" ],
    [ "Utilities", "dir_b28fbe67e8e3f38457725099587995d6.html", "dir_b28fbe67e8e3f38457725099587995d6" ],
    [ "config.h", "config_8h.html", "config_8h" ]
];