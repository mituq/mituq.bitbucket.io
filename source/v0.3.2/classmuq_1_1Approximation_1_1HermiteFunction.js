var classmuq_1_1Approximation_1_1HermiteFunction =
[
    [ "HermiteFunction", "classmuq_1_1Approximation_1_1HermiteFunction.html#aaa336c6ba98e03c2ed22596ddf921890", null ],
    [ "~HermiteFunction", "classmuq_1_1Approximation_1_1HermiteFunction.html#a1076c78dceb84272e752b1e70a7f4fde", null ],
    [ "BasisEvaluate", "classmuq_1_1Approximation_1_1HermiteFunction.html#a30abbbe910975bd2aa584fe790d58c4d", null ],
    [ "DerivativeEvaluate", "classmuq_1_1Approximation_1_1HermiteFunction.html#adb89d8e6124a9762a06838352e3b8a56", null ],
    [ "nChoosek", "classmuq_1_1Approximation_1_1HermiteFunction.html#acd49ea25788afabdf55c12b0248581b1", null ],
    [ "polyBase", "classmuq_1_1Approximation_1_1HermiteFunction.html#ab49d6f92f25d3332c805dc13d4a9fb11", null ]
];