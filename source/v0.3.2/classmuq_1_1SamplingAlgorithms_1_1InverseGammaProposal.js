var classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal =
[
    [ "InverseGammaProposal", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a8480bc5a32eca53f568a56311e1a0481", null ],
    [ "~InverseGammaProposal", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a056ed4021517f6d73c3556aed1d8febc", null ],
    [ "ExtractAlpha", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#ae8e1ef0779cd759ff8dca1dd6b2a77cc", null ],
    [ "ExtractBeta", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#ac7c33d78dbd9624d8dcf34302c33ea91", null ],
    [ "ExtractGaussInfo", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#ac6a2898641ac5a306404227db317cf18", null ],
    [ "ExtractInverseGamma", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#af570dd86bb426833fdd04f09a2799c42", null ],
    [ "ExtractMean", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#aae40d90db0437ed6f8cd1e5fe7297811", null ],
    [ "ExtractVarianceModel", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a7c6d97d06ff2720332242891d377e3d4", null ],
    [ "GetGaussianInput", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a0f0c5773e97852198a18e169459dc0ef", null ],
    [ "LogDensity", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#ac01193bbebbca17c6b07e99838b3aaac", null ],
    [ "Sample", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a73b37a6a7886e11f2bac337b39a2cf22", null ],
    [ "alpha", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a05599735d7932de9a6e2e4f31b83ed9e", null ],
    [ "beta", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a7d0d56a5c83984daeb35e4f26a9db920", null ],
    [ "gaussInfo", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#a8480ff2149b8517855f42ad14837d876", null ],
    [ "gaussMean", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#ae9395e76ca4f5cc00ba0cc1b6e2f8f07", null ],
    [ "varModel", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html#abf6e273144ab84c1a7a4c8eab9949cef", null ]
];