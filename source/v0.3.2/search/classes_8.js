var searchData=
[
  ['identityoperator',['IdentityOperator',['../classmuq_1_1Modeling_1_1IdentityOperator.html',1,'muq::Modeling']]],
  ['identitypiece',['IdentityPiece',['../classmuq_1_1Modeling_1_1IdentityPiece.html',1,'muq::Modeling']]],
  ['importancesampling',['ImportanceSampling',['../classmuq_1_1SamplingAlgorithms_1_1ImportanceSampling.html',1,'muq::SamplingAlgorithms']]],
  ['indexedscalarbasis',['IndexedScalarBasis',['../classmuq_1_1Approximation_1_1IndexedScalarBasis.html',1,'muq::Approximation']]],
  ['indexscalarbasis',['IndexScalarBasis',['../classIndexScalarBasis.html',1,'']]],
  ['infmalaproposal',['InfMALAProposal',['../classmuq_1_1SamplingAlgorithms_1_1InfMALAProposal.html',1,'muq::SamplingAlgorithms']]],
  ['integratoroptions',['IntegratorOptions',['../structmuq_1_1Modeling_1_1ODE_1_1IntegratorOptions.html',1,'muq::Modeling::ODE::IntegratorOptions'],['../classODE_1_1IntegratorOptions.html',1,'ODE::IntegratorOptions']]],
  ['interface',['Interface',['../structmuq_1_1Modeling_1_1ODE_1_1Interface.html',1,'muq::Modeling::ODE']]],
  ['inversegamma',['InverseGamma',['../classmuq_1_1Modeling_1_1InverseGamma.html',1,'muq::Modeling']]],
  ['inversegammaproposal',['InverseGammaProposal',['../classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html',1,'muq::SamplingAlgorithms']]],
  ['iskernel',['ISKernel',['../classmuq_1_1SamplingAlgorithms_1_1ISKernel.html',1,'muq::SamplingAlgorithms']]]
];
