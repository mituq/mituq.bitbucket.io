var dir_17356ec5b34ef9a4a20a8353d41bb99c =
[
    [ "AffineOperator.h", "AffineOperator_8h.html", [
      [ "AffineOperator", "classmuq_1_1Modeling_1_1AffineOperator.html", "classmuq_1_1Modeling_1_1AffineOperator" ]
    ] ],
    [ "AnyAlgebra.h", "AnyAlgebra_8h.html", [
      [ "AnyAlgebra", "classmuq_1_1Modeling_1_1AnyAlgebra.html", "classmuq_1_1Modeling_1_1AnyAlgebra" ]
    ] ],
    [ "AnyAlgebra2.h", "AnyAlgebra2_8h.html", [
      [ "AnyMat", "classmuq_1_1Modeling_1_1AnyMat.html", "classmuq_1_1Modeling_1_1AnyMat" ],
      [ "AnyAlgebra2", "classmuq_1_1Modeling_1_1AnyAlgebra2.html", "classmuq_1_1Modeling_1_1AnyAlgebra2" ]
    ] ],
    [ "AnyVec.h", "AnyVec_8h.html", [
      [ "AnyVec", "classmuq_1_1Modeling_1_1AnyVec.html", "classmuq_1_1Modeling_1_1AnyVec" ]
    ] ],
    [ "BlockDiagonalOperator.h", "BlockDiagonalOperator_8h.html", [
      [ "BlockDiagonalOperator", "classmuq_1_1Modeling_1_1BlockDiagonalOperator.html", "classmuq_1_1Modeling_1_1BlockDiagonalOperator" ]
    ] ],
    [ "BlockRowOperator.h", "BlockRowOperator_8h.html", [
      [ "BlockRowOperator", "classmuq_1_1Modeling_1_1BlockRowOperator.html", "classmuq_1_1Modeling_1_1BlockRowOperator" ]
    ] ],
    [ "CompanionMatrix.h", "CompanionMatrix_8h.html", [
      [ "CompanionMatrix", "classmuq_1_1Modeling_1_1CompanionMatrix.html", "classmuq_1_1Modeling_1_1CompanionMatrix" ]
    ] ],
    [ "ConcatenateOperator.h", "ConcatenateOperator_8h.html", [
      [ "ConcatenateOperator", "classmuq_1_1Modeling_1_1ConcatenateOperator.html", "classmuq_1_1Modeling_1_1ConcatenateOperator" ]
    ] ],
    [ "DiagonalOperator.h", "DiagonalOperator_8h.html", [
      [ "DiagonalOperator", "classmuq_1_1Modeling_1_1DiagonalOperator.html", "classmuq_1_1Modeling_1_1DiagonalOperator" ]
    ] ],
    [ "EigenLinearOperator.h", "EigenLinearOperator_8h.html", null ],
    [ "EigenMatrixAlgebra.h", "EigenMatrixAlgebra_8h.html", null ],
    [ "EigenVectorAlgebra.h", "EigenVectorAlgebra_8h.html", null ],
    [ "GaussianOperator.h", "GaussianOperator_8h.html", [
      [ "GaussianOperator", "classmuq_1_1Modeling_1_1GaussianOperator.html", "classmuq_1_1Modeling_1_1GaussianOperator" ]
    ] ],
    [ "GaussNewtonOperator.h", "GaussNewtonOperator_8h.html", [
      [ "GaussNewtonOperator", "classmuq_1_1Modeling_1_1GaussNewtonOperator.html", "classmuq_1_1Modeling_1_1GaussNewtonOperator" ]
    ] ],
    [ "GeneralizedEigenSolver.h", "GeneralizedEigenSolver_8h.html", [
      [ "GeneralizedEigenSolver", "classmuq_1_1Modeling_1_1GeneralizedEigenSolver.html", "classmuq_1_1Modeling_1_1GeneralizedEigenSolver" ]
    ] ],
    [ "HessianOperator.h", "HessianOperator_8h.html", [
      [ "HessianOperator", "classmuq_1_1Modeling_1_1HessianOperator.html", "classmuq_1_1Modeling_1_1HessianOperator" ]
    ] ],
    [ "IdentityOperator.h", "IdentityOperator_8h.html", [
      [ "IdentityOperator", "classmuq_1_1Modeling_1_1IdentityOperator.html", "classmuq_1_1Modeling_1_1IdentityOperator" ]
    ] ],
    [ "KroneckerProductOperator.h", "KroneckerProductOperator_8h.html", "KroneckerProductOperator_8h" ],
    [ "LinearOperator.h", "Modeling_2LinearAlgebra_2LinearOperator_8h.html", [
      [ "LinearOperatorTypeException", "classmuq_1_1Modeling_1_1LinearOperatorTypeException.html", "classmuq_1_1Modeling_1_1LinearOperatorTypeException" ],
      [ "LinearOperatorFactory", "structmuq_1_1Modeling_1_1LinearOperatorFactory.html", "structmuq_1_1Modeling_1_1LinearOperatorFactory" ],
      [ "LinearOperator", "classmuq_1_1Modeling_1_1LinearOperator.html", "classmuq_1_1Modeling_1_1LinearOperator" ]
    ] ],
    [ "LOBPCG.h", "LOBPCG_8h.html", [
      [ "LOBPCG", "classmuq_1_1Modeling_1_1LOBPCG.html", "classmuq_1_1Modeling_1_1LOBPCG" ]
    ] ],
    [ "LyapunovSolver.h", "LyapunovSolver_8h.html", [
      [ "LyapunovSolver", "classmuq_1_1Modeling_1_1LyapunovSolver.html", "classmuq_1_1Modeling_1_1LyapunovSolver" ]
    ] ],
    [ "ProductOperator.h", "ProductOperator_8h.html", [
      [ "ProductOperator", "classmuq_1_1Modeling_1_1ProductOperator.html", "classmuq_1_1Modeling_1_1ProductOperator" ]
    ] ],
    [ "ScalarAlgebra.h", "ScalarAlgebra_8h.html", [
      [ "ScalarAlgebra", "classmuq_1_1Modeling_1_1ScalarAlgebra.html", "classmuq_1_1Modeling_1_1ScalarAlgebra" ]
    ] ],
    [ "SliceOperator.h", "SliceOperator_8h.html", [
      [ "SliceOperator", "classmuq_1_1Modeling_1_1SliceOperator.html", "classmuq_1_1Modeling_1_1SliceOperator" ]
    ] ],
    [ "SparseLinearOperator.h", "SparseLinearOperator_8h.html", [
      [ "SparseLinearOperator", "classmuq_1_1Modeling_1_1SparseLinearOperator.html", "classmuq_1_1Modeling_1_1SparseLinearOperator" ]
    ] ],
    [ "StochasticEigenSolver.h", "StochasticEigenSolver_8h.html", [
      [ "StochasticEigenSolver", "classmuq_1_1Modeling_1_1StochasticEigenSolver.html", "classmuq_1_1Modeling_1_1StochasticEigenSolver" ]
    ] ],
    [ "SumOperator.h", "SumOperator_8h.html", [
      [ "SumOperator", "classmuq_1_1Modeling_1_1SumOperator.html", "classmuq_1_1Modeling_1_1SumOperator" ]
    ] ],
    [ "SundialsAlgebra.h", "SundialsAlgebra_8h.html", [
      [ "SundialsAlgebra", "classmuq_1_1Modeling_1_1SundialsAlgebra.html", "classmuq_1_1Modeling_1_1SundialsAlgebra" ]
    ] ],
    [ "ZeroOperator.h", "ZeroOperator_8h.html", [
      [ "ZeroOperator", "classmuq_1_1Modeling_1_1ZeroOperator.html", "classmuq_1_1Modeling_1_1ZeroOperator" ]
    ] ]
];