var classmuq_1_1Approximation_1_1ProbabilistHermite =
[
    [ "ProbabilistHermite", "classmuq_1_1Approximation_1_1ProbabilistHermite.html#a8e62e31834e407182c9d5c42e0a4f58f", null ],
    [ "~ProbabilistHermite", "classmuq_1_1Approximation_1_1ProbabilistHermite.html#a616f2f1a089d3f41cfe7ceb9282da8ab", null ],
    [ "ak", "classmuq_1_1Approximation_1_1ProbabilistHermite.html#ab6ff3b8049f65d40a0c472d5e9115663", null ],
    [ "bk", "classmuq_1_1Approximation_1_1ProbabilistHermite.html#a8359f02eb2e2b36429ea9c3715615787", null ],
    [ "ck", "classmuq_1_1Approximation_1_1ProbabilistHermite.html#ac79dc49688e450442cd794d8e82aad14", null ],
    [ "DerivativeEvaluate", "classmuq_1_1Approximation_1_1ProbabilistHermite.html#a81540b20277ed62843f2f8e2b77c0414", null ],
    [ "Normalization", "classmuq_1_1Approximation_1_1ProbabilistHermite.html#ab44a5b9b7f0d55e2d21556344939e7b7", null ],
    [ "phi0", "classmuq_1_1Approximation_1_1ProbabilistHermite.html#a0a5be22578ed25f5f970beec125f0597", null ],
    [ "phi1", "classmuq_1_1Approximation_1_1ProbabilistHermite.html#ada72b78ebca0ace058d7899ada990702", null ]
];