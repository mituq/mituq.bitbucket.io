var Approximation_2python_2wrappers_2AllClassWrappers_8h =
[
    [ "GaussianWrapper", "Approximation_2python_2wrappers_2AllClassWrappers_8h.html#a8a0c03afe0a85c568a0aa2f66c1d8185", null ],
    [ "KernelWrapper", "Approximation_2python_2wrappers_2AllClassWrappers_8h.html#ab1405abae81a7563d7fef5f7ad8b8791", null ],
    [ "KLWrapper", "Approximation_2python_2wrappers_2AllClassWrappers_8h.html#a4f186ccb2765d5d78b30067b039b5d47", null ],
    [ "PolynomialChaosWrapper", "Approximation_2python_2wrappers_2AllClassWrappers_8h.html#a4f320f26cedf5e978c7511f6bd86b640", null ],
    [ "PolynomialsWrapper", "Approximation_2python_2wrappers_2AllClassWrappers_8h.html#a87d9dde82cbaf224558b3035b7078f89", null ],
    [ "QuadratureWrapper", "Approximation_2python_2wrappers_2AllClassWrappers_8h.html#a19e7ef2a252821ff1559286d064a73ef", null ]
];