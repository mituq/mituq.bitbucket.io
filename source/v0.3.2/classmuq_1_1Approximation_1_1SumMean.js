var classmuq_1_1Approximation_1_1SumMean =
[
    [ "SumMean", "classmuq_1_1Approximation_1_1SumMean.html#ad7a9f1369b35207cf3c2b8bd3eaff77a", null ],
    [ "~SumMean", "classmuq_1_1Approximation_1_1SumMean.html#ae4ff15e7926abc82e4dbe382f2b83067", null ],
    [ "Clone", "classmuq_1_1Approximation_1_1SumMean.html#af09b87a213258bb82001ac52b35df4bc", null ],
    [ "Evaluate", "classmuq_1_1Approximation_1_1SumMean.html#ae93ee6b976ff8a6861dad34e7843b732", null ],
    [ "GetDerivative", "classmuq_1_1Approximation_1_1SumMean.html#a5255ad55111c560d2131827455642cd5", null ],
    [ "mu1", "classmuq_1_1Approximation_1_1SumMean.html#aa8e0a8f429c9c6464fd3847cdb961d6d", null ],
    [ "mu2", "classmuq_1_1Approximation_1_1SumMean.html#aec2537d6bbdc81af8f741080115d962f", null ]
];