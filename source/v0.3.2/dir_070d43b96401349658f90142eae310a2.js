var dir_070d43b96401349658f90142eae310a2 =
[
    [ "BasisExpansion.cpp", "BasisExpansion_8cpp.html", null ],
    [ "HermiteFunction.cpp", "HermiteFunction_8cpp.html", null ],
    [ "IndexedScalarBasis.cpp", "IndexedScalarBasis_8cpp.html", null ],
    [ "Jacobi.cpp", "Jacobi_8cpp.html", null ],
    [ "Laguerre.cpp", "Laguerre_8cpp.html", null ],
    [ "Legendre.cpp", "Legendre_8cpp.html", null ],
    [ "Monomial.cpp", "Monomial_8cpp.html", null ],
    [ "MonotoneExpansion.cpp", "MonotoneExpansion_8cpp.html", null ],
    [ "OrthogonalPolynomial.cpp", "OrthogonalPolynomial_8cpp.html", null ],
    [ "PhysicistHermite.cpp", "PhysicistHermite_8cpp.html", null ],
    [ "ProbabilistHermite.cpp", "ProbabilistHermite_8cpp.html", null ]
];