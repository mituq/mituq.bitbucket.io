var classmuq_1_1Approximation_1_1Quadrature =
[
    [ "Quadrature", "classmuq_1_1Approximation_1_1Quadrature.html#a7f8e2cb7a145771a85a4d7024b1f2d28", null ],
    [ "~Quadrature", "classmuq_1_1Approximation_1_1Quadrature.html#af1304d3f79d8508baab90d22d649b762", null ],
    [ "Compute", "classmuq_1_1Approximation_1_1Quadrature.html#a34ac5ea0851782f83bed2101096958b4", null ],
    [ "Compute", "classmuq_1_1Approximation_1_1Quadrature.html#a9896973503a2b692016d0cc014faea25", null ],
    [ "Dim", "classmuq_1_1Approximation_1_1Quadrature.html#a943068b7e3ee0ae66d8e7e09d1e016db", null ],
    [ "Exactness", "classmuq_1_1Approximation_1_1Quadrature.html#a3ea8c6a294f23b81b3bc8e20cb131530", null ],
    [ "Points", "classmuq_1_1Approximation_1_1Quadrature.html#a570d33e930d3e173dc1bdffff959663f", null ],
    [ "Weights", "classmuq_1_1Approximation_1_1Quadrature.html#a8d7871843470b3d4944bc6e4c69f61ad", null ],
    [ "dim", "classmuq_1_1Approximation_1_1Quadrature.html#a3e3498b469d800bfdeff115059768877", null ],
    [ "pts", "classmuq_1_1Approximation_1_1Quadrature.html#a9c584565e56ccf89bb1854dffc5880c5", null ],
    [ "wts", "classmuq_1_1Approximation_1_1Quadrature.html#adadc54d61f54d97868d38845cb1011bb", null ]
];