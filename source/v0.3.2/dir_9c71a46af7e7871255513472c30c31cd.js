var dir_9c71a46af7e7871255513472c30c31cd =
[
    [ "GaussianProcesses", "dir_8a5caa986cd9f7aea421a74d21e77430.html", "dir_8a5caa986cd9f7aea421a74d21e77430" ],
    [ "PolynomialChaos", "dir_c859a4cd5c940c01dea5d77a5e60772a.html", "dir_c859a4cd5c940c01dea5d77a5e60772a" ],
    [ "Polynomials", "dir_070d43b96401349658f90142eae310a2.html", "dir_070d43b96401349658f90142eae310a2" ],
    [ "Quadrature", "dir_98b33c8e2dd2cf93138e02904da0a7c3.html", "dir_98b33c8e2dd2cf93138e02904da0a7c3" ],
    [ "Regression", "dir_29c30c15a2a4516b5a52174feca0a43d.html", "dir_29c30c15a2a4516b5a52174feca0a43d" ]
];