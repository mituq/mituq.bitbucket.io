var classmuq_1_1Approximation_1_1GaussQuadrature =
[
    [ "GaussQuadrature", "classmuq_1_1Approximation_1_1GaussQuadrature.html#a21659bdf86aeef67ffdfcbda88595f2f", null ],
    [ "~GaussQuadrature", "classmuq_1_1Approximation_1_1GaussQuadrature.html#af5f30363ca5aa0fbc5831f1d40c5ee85", null ],
    [ "GaussQuadrature", "classmuq_1_1Approximation_1_1GaussQuadrature.html#a46f14c301907e6b49292b08d09d8ef6e", null ],
    [ "GaussQuadrature", "classmuq_1_1Approximation_1_1GaussQuadrature.html#a67575d92062014452bd42008ca6e26a0", null ],
    [ "Compute", "classmuq_1_1Approximation_1_1GaussQuadrature.html#a0e5d9316044cecabf5d99cb05b447daf", null ],
    [ "Exactness", "classmuq_1_1Approximation_1_1GaussQuadrature.html#ad0765f5969d143f69d15beaaf8346f3e", null ],
    [ "poly", "classmuq_1_1Approximation_1_1GaussQuadrature.html#ab661f148996b46ccdecfdf86aecb7fdc", null ],
    [ "polyOrder", "classmuq_1_1Approximation_1_1GaussQuadrature.html#a17a54d74f183c955186f19b186982f26", null ]
];