var hierarchy =
[
    [ "muq::SamplingAlgorithms::AbstractSamplingProblem", "classmuq_1_1SamplingAlgorithms_1_1AbstractSamplingProblem.html", [
      [ "muq::SamplingAlgorithms::SamplingProblem", "classmuq_1_1SamplingAlgorithms_1_1SamplingProblem.html", [
        [ "muq::SamplingAlgorithms::ExpensiveSamplingProblem", "classmuq_1_1SamplingAlgorithms_1_1ExpensiveSamplingProblem.html", null ]
      ] ]
    ] ],
    [ "muq::Modeling::AnyAlgebra", "classmuq_1_1Modeling_1_1AnyAlgebra.html", null ],
    [ "muq::Modeling::AnyAlgebra2", "classmuq_1_1Modeling_1_1AnyAlgebra2.html", null ],
    [ "muq::Utilities::AnyCast", "classmuq_1_1Utilities_1_1AnyCast.html", null ],
    [ "muq::Utilities::AnyConstCast", "classmuq_1_1Utilities_1_1AnyConstCast.html", null ],
    [ "muq::Modeling::AnyMat", "classmuq_1_1Modeling_1_1AnyMat.html", null ],
    [ "muq::Modeling::AnyVec", "classmuq_1_1Modeling_1_1AnyVec.html", null ],
    [ "muq::Utilities::AnyWriter< T >", "structmuq_1_1Utilities_1_1AnyWriter.html", null ],
    [ "argument_type< T >", "structargument__type.html", null ],
    [ "argument_type< T(U)>", "structargument__type_3_01T_07U_08_4.html", null ],
    [ "muq::Utilities::Attribute", "classmuq_1_1Utilities_1_1Attribute.html", null ],
    [ "muq::Utilities::AttributeList", "classmuq_1_1Utilities_1_1AttributeList.html", null ],
    [ "muq::Utilities::BlockDataset", "classmuq_1_1Utilities_1_1BlockDataset.html", null ],
    [ "cereal::BoostAnySerializer< Archive >", "classcereal_1_1BoostAnySerializer.html", null ],
    [ "muq::Approximation::ColumnSlice< MatType >", "classmuq_1_1Approximation_1_1ColumnSlice.html", null ],
    [ "ConstantMean", "classConstantMean.html", null ],
    [ "muq::Optimization::Optimization::CostHelper", "structmuq_1_1Optimization_1_1Optimization_1_1CostHelper.html", null ],
    [ "DenseLinearOperator", "classDenseLinearOperator.html", null ],
    [ "muq::Modeling::DependentEdgePredicate", "classmuq_1_1Modeling_1_1DependentEdgePredicate.html", null ],
    [ "muq::Modeling::DependentPredicate", "classmuq_1_1Modeling_1_1DependentPredicate.html", null ],
    [ "muq::Modeling::DynamicKDTreeAdaptor< Distance, IndexType >", "structmuq_1_1Modeling_1_1DynamicKDTreeAdaptor.html", null ],
    [ "enable_shared_from_this", null, [
      [ "muq::Approximation::KernelBase", "classmuq_1_1Approximation_1_1KernelBase.html", [
        [ "muq::Approximation::KernelImpl< ConstantKernel >", "classmuq_1_1Approximation_1_1KernelImpl.html", [
          [ "muq::Approximation::ConstantKernel", "classmuq_1_1Approximation_1_1ConstantKernel.html", null ]
        ] ],
        [ "muq::Approximation::KernelImpl< MaternKernel >", "classmuq_1_1Approximation_1_1KernelImpl.html", [
          [ "muq::Approximation::MaternKernel", "classmuq_1_1Approximation_1_1MaternKernel.html", null ]
        ] ],
        [ "muq::Approximation::KernelImpl< SquaredExpKernel >", "classmuq_1_1Approximation_1_1KernelImpl.html", [
          [ "muq::Approximation::SquaredExpKernel", "classmuq_1_1Approximation_1_1SquaredExpKernel.html", null ]
        ] ],
        [ "muq::Approximation::KernelImpl< WhiteNoiseKernel >", "classmuq_1_1Approximation_1_1KernelImpl.html", [
          [ "muq::Approximation::WhiteNoiseKernel", "classmuq_1_1Approximation_1_1WhiteNoiseKernel.html", null ]
        ] ],
        [ "muq::Approximation::ConcatenateKernel", "classmuq_1_1Approximation_1_1ConcatenateKernel.html", null ],
        [ "muq::Approximation::KernelImpl< ChildType >", "classmuq_1_1Approximation_1_1KernelImpl.html", null ],
        [ "muq::Approximation::LinearTransformKernel", "classmuq_1_1Approximation_1_1LinearTransformKernel.html", null ],
        [ "muq::Approximation::ProductKernel", "classmuq_1_1Approximation_1_1ProductKernel.html", null ],
        [ "muq::Approximation::SumKernel", "classmuq_1_1Approximation_1_1SumKernel.html", null ]
      ] ],
      [ "muq::Approximation::MeanFunctionBase", "classmuq_1_1Approximation_1_1MeanFunctionBase.html", [
        [ "muq::Approximation::LinearMean", "classmuq_1_1Approximation_1_1LinearMean.html", null ],
        [ "muq::Approximation::LinearTransformMean< LinearOperator >", "classmuq_1_1Approximation_1_1LinearTransformMean.html", null ],
        [ "muq::Approximation::SumMean", "classmuq_1_1Approximation_1_1SumMean.html", null ],
        [ "muq::Approximation::ZeroMean", "classmuq_1_1Approximation_1_1ZeroMean.html", null ]
      ] ],
      [ "muq::Approximation::ObservationInformation", "classmuq_1_1Approximation_1_1ObservationInformation.html", [
        [ "muq::Approximation::DerivativeObservation", "classmuq_1_1Approximation_1_1DerivativeObservation.html", null ]
      ] ],
      [ "muq::Approximation::Regression", "classmuq_1_1Approximation_1_1Regression.html", null ],
      [ "muq::Modeling::Distribution", "classmuq_1_1Modeling_1_1Distribution.html", [
        [ "muq::Modeling::DensityBase", "classmuq_1_1Modeling_1_1DensityBase.html", [
          [ "muq::Modeling::Density", "classmuq_1_1Modeling_1_1Density.html", null ],
          [ "muq::Modeling::DensityProduct", "classmuq_1_1Modeling_1_1DensityProduct.html", null ]
        ] ],
        [ "muq::Modeling::GaussianBase", "classmuq_1_1Modeling_1_1GaussianBase.html", [
          [ "muq::Modeling::Gaussian", "classmuq_1_1Modeling_1_1Gaussian.html", null ],
          [ "muq::Modeling::PyGaussianBase", "classmuq_1_1Modeling_1_1PyGaussianBase.html", null ]
        ] ],
        [ "muq::Modeling::InverseGamma", "classmuq_1_1Modeling_1_1InverseGamma.html", null ],
        [ "muq::Modeling::PyDistribution", "classmuq_1_1Modeling_1_1PyDistribution.html", null ],
        [ "muq::Modeling::RandomVariable", "classmuq_1_1Modeling_1_1RandomVariable.html", null ],
        [ "muq::Modeling::UniformBox", "classmuq_1_1Modeling_1_1UniformBox.html", null ]
      ] ],
      [ "muq::SamplingAlgorithms::MCMCProposal", "classmuq_1_1SamplingAlgorithms_1_1MCMCProposal.html", [
        [ "muq::SamplingAlgorithms::AMProposal", "classmuq_1_1SamplingAlgorithms_1_1AMProposal.html", [
          [ "muq::SamplingAlgorithms::ParallelAMProposal", "classmuq_1_1SamplingAlgorithms_1_1ParallelAMProposal.html", null ]
        ] ],
        [ "muq::SamplingAlgorithms::CrankNicolsonProposal", "classmuq_1_1SamplingAlgorithms_1_1CrankNicolsonProposal.html", [
          [ "muq::SamplingAlgorithms::InfMALAProposal", "classmuq_1_1SamplingAlgorithms_1_1InfMALAProposal.html", null ]
        ] ],
        [ "muq::SamplingAlgorithms::InverseGammaProposal", "classmuq_1_1SamplingAlgorithms_1_1InverseGammaProposal.html", null ],
        [ "muq::SamplingAlgorithms::MALAProposal", "classmuq_1_1SamplingAlgorithms_1_1MALAProposal.html", null ],
        [ "muq::SamplingAlgorithms::MHProposal", "classmuq_1_1SamplingAlgorithms_1_1MHProposal.html", null ],
        [ "muq::SamplingAlgorithms::MixtureProposal", "classmuq_1_1SamplingAlgorithms_1_1MixtureProposal.html", null ],
        [ "muq::SamplingAlgorithms::SubsamplingMIProposal", "classmuq_1_1SamplingAlgorithms_1_1SubsamplingMIProposal.html", null ]
      ] ],
      [ "muq::Utilities::HDF5File", "classmuq_1_1Utilities_1_1HDF5File.html", null ]
    ] ],
    [ "std::exception", null, [
      [ "muq::Modeling::LinearOperatorTypeException", "classmuq_1_1Modeling_1_1LinearOperatorTypeException.html", null ],
      [ "muq::Utilities::LinearOperatorTypeException", "classmuq_1_1Utilities_1_1LinearOperatorTypeException.html", null ],
      [ "std::logic_error", null, [
        [ "muq::ExternalLibraryError", "classmuq_1_1ExternalLibraryError.html", null ],
        [ "muq::NotImplementedError", "classmuq_1_1NotImplementedError.html", null ],
        [ "muq::NotRegisteredError", "classmuq_1_1NotRegisteredError.html", null ],
        [ "std::length_error", null, [
          [ "muq::WrongSizeError", "classmuq_1_1WrongSizeError.html", null ]
        ] ]
      ] ]
    ] ],
    [ "muq::SamplingAlgorithms::ExpectedModPieceValue", "classmuq_1_1SamplingAlgorithms_1_1ExpectedModPieceValue.html", null ],
    [ "muq::Approximation::GaussianProcess", "classmuq_1_1Approximation_1_1GaussianProcess.html", [
      [ "muq::Approximation::StateSpaceGP", "classmuq_1_1Approximation_1_1StateSpaceGP.html", null ]
    ] ],
    [ "muq::Modeling::GeneralizedEigenSolver", "classmuq_1_1Modeling_1_1GeneralizedEigenSolver.html", [
      [ "muq::Modeling::LOBPCG", "classmuq_1_1Modeling_1_1LOBPCG.html", null ],
      [ "muq::Modeling::StochasticEigenSolver", "classmuq_1_1Modeling_1_1StochasticEigenSolver.html", null ]
    ] ],
    [ "GeneralLimiter", "classGeneralLimiter.html", null ],
    [ "muq::Utilities::H5Object", "classmuq_1_1Utilities_1_1H5Object.html", null ],
    [ "muq::Utilities::HDF5_Type< scalarType >", "structmuq_1_1Utilities_1_1HDF5__Type.html", null ],
    [ "muq::Utilities::HDF5_Type< bool >", "structmuq_1_1Utilities_1_1HDF5__Type_3_01bool_01_4.html", null ],
    [ "muq::Utilities::HDF5_Type< char >", "structmuq_1_1Utilities_1_1HDF5__Type_3_01char_01_4.html", null ],
    [ "muq::Utilities::HDF5_Type< double >", "structmuq_1_1Utilities_1_1HDF5__Type_3_01double_01_4.html", null ],
    [ "muq::Utilities::HDF5_Type< float >", "structmuq_1_1Utilities_1_1HDF5__Type_3_01float_01_4.html", null ],
    [ "muq::Utilities::HDF5_Type< int >", "structmuq_1_1Utilities_1_1HDF5__Type_3_01int_01_4.html", null ],
    [ "muq::Utilities::HDF5_Type< long >", "structmuq_1_1Utilities_1_1HDF5__Type_3_01long_01_4.html", null ],
    [ "muq::Utilities::HDF5_Type< long double >", "structmuq_1_1Utilities_1_1HDF5__Type_3_01long_01double_01_4.html", null ],
    [ "muq::Utilities::HDF5_Type< short >", "structmuq_1_1Utilities_1_1HDF5__Type_3_01short_01_4.html", null ],
    [ "muq::Utilities::HDF5_Type< unsigned >", "structmuq_1_1Utilities_1_1HDF5__Type_3_01unsigned_01_4.html", null ],
    [ "muq::Utilities::HDF5_Type< unsigned long >", "structmuq_1_1Utilities_1_1HDF5__Type_3_01unsigned_01long_01_4.html", null ],
    [ "muq::Utilities::HDF5_Type< unsigned short >", "structmuq_1_1Utilities_1_1HDF5__Type_3_01unsigned_01short_01_4.html", null ],
    [ "IndexScalarBasis", "classIndexScalarBasis.html", null ],
    [ "muq::Modeling::ODE::IntegratorOptions", "structmuq_1_1Modeling_1_1ODE_1_1IntegratorOptions.html", null ],
    [ "ODE::IntegratorOptions", "classODE_1_1IntegratorOptions.html", null ],
    [ "muq::Modeling::ODE::Interface", "structmuq_1_1Modeling_1_1ODE_1_1Interface.html", null ],
    [ "muq::Inference::KalmanFilter", "classmuq_1_1Inference_1_1KalmanFilter.html", null ],
    [ "muq::Inference::KalmanSmoother", "classmuq_1_1Inference_1_1KalmanSmoother.html", null ],
    [ "muq::Approximation::KarhunenLoeveBase", "classmuq_1_1Approximation_1_1KarhunenLoeveBase.html", [
      [ "muq::Approximation::KarhunenLoeveExpansion", "classmuq_1_1Approximation_1_1KarhunenLoeveExpansion.html", null ]
    ] ],
    [ "muq::Approximation::KarhunenLoeveFactory", "classmuq_1_1Approximation_1_1KarhunenLoeveFactory.html", null ],
    [ "muq::Modeling::LinearOperatorFactory< MatrixType >", "structmuq_1_1Modeling_1_1LinearOperatorFactory.html", null ],
    [ "muq::Utilities::LinearOperatorFactory< MatrixType >", "structmuq_1_1Utilities_1_1LinearOperatorFactory.html", null ],
    [ "muq::Modeling::LinearSDE", "classmuq_1_1Modeling_1_1LinearSDE.html", null ],
    [ "muq::Modeling::ODE::LinearSolverOptions", "structmuq_1_1Modeling_1_1ODE_1_1LinearSolverOptions.html", null ],
    [ "ODE::LinearSolverOptions", "classODE_1_1LinearSolverOptions.html", null ],
    [ "LyaponovSolver", "classLyaponovSolver.html", null ],
    [ "muq::Modeling::LyapunovSolver< ScalarType, FixedRows, FixedCols >", "classmuq_1_1Modeling_1_1LyapunovSolver.html", null ],
    [ "muq::Approximation::MatrixBlock< MatType >", "classmuq_1_1Approximation_1_1MatrixBlock.html", null ],
    [ "muq::SamplingAlgorithms::MCMCFactory", "classmuq_1_1SamplingAlgorithms_1_1MCMCFactory.html", null ],
    [ "MCMCProposal", null, [
      [ "MCSampleProposal", "classMCSampleProposal.html", null ]
    ] ],
    [ "muq::SamplingAlgorithms::MIComponentFactory", "classmuq_1_1SamplingAlgorithms_1_1MIComponentFactory.html", null ],
    [ "MIComponentFactory", null, [
      [ "MyMIComponentFactory", "classMyMIComponentFactory.html", null ],
      [ "MyMIComponentFactory", "classMyMIComponentFactory.html", null ],
      [ "MyMIComponentFactory", "classMyMIComponentFactory.html", null ]
    ] ],
    [ "muq::SamplingAlgorithms::MIInterpolation", "classmuq_1_1SamplingAlgorithms_1_1MIInterpolation.html", [
      [ "muq::SamplingAlgorithms::ConcatenatingInterpolation", "classmuq_1_1SamplingAlgorithms_1_1ConcatenatingInterpolation.html", null ]
    ] ],
    [ "MIInterpolation", null, [
      [ "MyInterpolation", "classMyInterpolation.html", null ],
      [ "MyInterpolation", "classMyInterpolation.html", null ],
      [ "MyInterpolation", "classMyInterpolation.html", null ],
      [ "MyInterpolation", "classMyInterpolation.html", null ],
      [ "MyInterpolation", "classMyInterpolation.html", null ],
      [ "MyInterpolation", "classMyInterpolation.html", null ]
    ] ],
    [ "muq::SamplingAlgorithms::MIMCMCBox", "classmuq_1_1SamplingAlgorithms_1_1MIMCMCBox.html", null ],
    [ "muq::Utilities::MultiIndexFactory", "classmuq_1_1Utilities_1_1MultiIndexFactory.html", null ],
    [ "muq::Utilities::MultiIndexLimiter", "classmuq_1_1Utilities_1_1MultiIndexLimiter.html", [
      [ "muq::Utilities::AndLimiter", "classmuq_1_1Utilities_1_1AndLimiter.html", null ],
      [ "muq::Utilities::AnisotropicLimiter", "classmuq_1_1Utilities_1_1AnisotropicLimiter.html", null ],
      [ "muq::Utilities::DimensionLimiter", "classmuq_1_1Utilities_1_1DimensionLimiter.html", null ],
      [ "muq::Utilities::MaxOrderLimiter", "classmuq_1_1Utilities_1_1MaxOrderLimiter.html", null ],
      [ "muq::Utilities::NoLimiter", "classmuq_1_1Utilities_1_1NoLimiter.html", null ],
      [ "muq::Utilities::OrLimiter", "classmuq_1_1Utilities_1_1OrLimiter.html", null ],
      [ "muq::Utilities::TotalOrderLimiter", "classmuq_1_1Utilities_1_1TotalOrderLimiter.html", null ],
      [ "muq::Utilities::XorLimiter", "classmuq_1_1Utilities_1_1XorLimiter.html", null ]
    ] ],
    [ "muq::Utilities::MultiIndexSet", "classmuq_1_1Utilities_1_1MultiIndexSet.html", null ],
    [ "muq::Modeling::NodeNameFinder", "structmuq_1_1Modeling_1_1NodeNameFinder.html", null ],
    [ "muq::Modeling::ODE::NonlinearSolverOptions", "structmuq_1_1Modeling_1_1ODE_1_1NonlinearSolverOptions.html", null ],
    [ "ODE::NonlinearSolverOptions", "classODE_1_1NonlinearSolverOptions.html", null ],
    [ "ODEBase", null, [
      [ "muq::Modeling::RootfindingIVP", "classmuq_1_1Modeling_1_1RootfindingIVP.html", null ]
    ] ],
    [ "muq::Modeling::ODEData", "classmuq_1_1Modeling_1_1ODEData.html", null ],
    [ "muq::Approximation::OptInfo", "structmuq_1_1Approximation_1_1OptInfo.html", null ],
    [ "muq::Utilities::OTF2TracerBase", "classmuq_1_1Utilities_1_1OTF2TracerBase.html", [
      [ "muq::Utilities::OTF2TracerDummy", "classmuq_1_1Utilities_1_1OTF2TracerDummy.html", null ]
    ] ],
    [ "ModPieceMemory.OtherModel", "classModPieceMemory_1_1OtherModel.html", null ],
    [ "ParallelizableMIComponentFactory", null, [
      [ "MyMIComponentFactory", "classMyMIComponentFactory.html", null ],
      [ "MyMIComponentFactory", "classMyMIComponentFactory.html", null ],
      [ "MyMIComponentFactory", "classMyMIComponentFactory.html", null ]
    ] ],
    [ "muq::Approximation::PCEFactory", "classmuq_1_1Approximation_1_1PCEFactory.html", null ],
    [ "PhysicistHermite", "classPhysicistHermite.html", null ],
    [ "ProbabilistHermite", "classProbabilistHermite.html", null ],
    [ "PyModPiece", null, [
      [ "BeamModel.BeamModel", "classBeamModel_1_1BeamModel.html", null ],
      [ "CustomModPiece.DiffusionEquation", "classCustomModPiece_1_1DiffusionEquation.html", null ],
      [ "ModPieceMemory.Test1", "classModPieceMemory_1_1Test1.html", null ],
      [ "pymuqModeling.PDEModPiece.PDEModPiece", "classpymuqModeling_1_1PDEModPiece_1_1PDEModPiece.html", null ]
    ] ],
    [ "PySwigObject", "structPySwigObject.html", null ],
    [ "muq::Approximation::Quadrature", "classmuq_1_1Approximation_1_1Quadrature.html", [
      [ "muq::Approximation::ClenshawCurtisQuadrature", "classmuq_1_1Approximation_1_1ClenshawCurtisQuadrature.html", null ],
      [ "muq::Approximation::ExponentialGrowthQuadrature", "classmuq_1_1Approximation_1_1ExponentialGrowthQuadrature.html", null ],
      [ "muq::Approximation::FullTensorQuadrature", "classmuq_1_1Approximation_1_1FullTensorQuadrature.html", null ],
      [ "muq::Approximation::GaussPattersonQuadrature", "classmuq_1_1Approximation_1_1GaussPattersonQuadrature.html", null ],
      [ "muq::Approximation::GaussQuadrature", "classmuq_1_1Approximation_1_1GaussQuadrature.html", null ],
      [ "muq::Approximation::SmolyakQuadrature", "classmuq_1_1Approximation_1_1SmolyakQuadrature.html", null ]
    ] ],
    [ "muq::Utilities::RandomGenerator", "classmuq_1_1Utilities_1_1RandomGenerator.html", null ],
    [ "muq::Utilities::RandomGeneratorTemporarySetSeed", "classmuq_1_1Utilities_1_1RandomGeneratorTemporarySetSeed.html", null ],
    [ "muq::SamplingAlgorithms::SampleCollection", "classmuq_1_1SamplingAlgorithms_1_1SampleCollection.html", [
      [ "muq::SamplingAlgorithms::MarkovChain", "classmuq_1_1SamplingAlgorithms_1_1MarkovChain.html", null ]
    ] ],
    [ "muq::SamplingAlgorithms::SamplingAlgorithm", "classmuq_1_1SamplingAlgorithms_1_1SamplingAlgorithm.html", [
      [ "muq::SamplingAlgorithms::GreedyMLMCMC", "classmuq_1_1SamplingAlgorithms_1_1GreedyMLMCMC.html", null ],
      [ "muq::SamplingAlgorithms::ImportanceSampling", "classmuq_1_1SamplingAlgorithms_1_1ImportanceSampling.html", null ],
      [ "muq::SamplingAlgorithms::MIMCMC", "classmuq_1_1SamplingAlgorithms_1_1MIMCMC.html", null ],
      [ "muq::SamplingAlgorithms::MonteCarlo", "classmuq_1_1SamplingAlgorithms_1_1MonteCarlo.html", null ],
      [ "muq::SamplingAlgorithms::SingleChainMCMC", "classmuq_1_1SamplingAlgorithms_1_1SingleChainMCMC.html", null ],
      [ "muq::SamplingAlgorithms::SLMCMC", "classmuq_1_1SamplingAlgorithms_1_1SLMCMC.html", null ]
    ] ],
    [ "muq::SamplingAlgorithms::SamplingState", "classmuq_1_1SamplingAlgorithms_1_1SamplingState.html", null ],
    [ "muq::SamplingAlgorithms::SamplingStateIdentity", "classmuq_1_1SamplingAlgorithms_1_1SamplingStateIdentity.html", null ],
    [ "muq::SamplingAlgorithms::SamplingStatePartialMoment", "classmuq_1_1SamplingAlgorithms_1_1SamplingStatePartialMoment.html", null ],
    [ "muq::SamplingAlgorithms::SaveSchedulerBase", "classmuq_1_1SamplingAlgorithms_1_1SaveSchedulerBase.html", [
      [ "muq::SamplingAlgorithms::ThinScheduler", "classmuq_1_1SamplingAlgorithms_1_1ThinScheduler.html", null ]
    ] ],
    [ "muq::Modeling::ScalarAlgebra", "classmuq_1_1Modeling_1_1ScalarAlgebra.html", null ],
    [ "muq::Utilities::SeedGenerator", "classmuq_1_1Utilities_1_1SeedGenerator.html", null ],
    [ "muq::Approximation::SeparableKarhunenLoeve", "classmuq_1_1Approximation_1_1SeparableKarhunenLoeve.html", null ],
    [ "muq::Utilities::shared_factory< T >", "structmuq_1_1Utilities_1_1shared__factory.html", null ],
    [ "muq::Approximation::SmolyakEstimator< EstimateType >", "classmuq_1_1Approximation_1_1SmolyakEstimator.html", null ],
    [ "muq::Approximation::SmolyakEstimator< Eigen::VectorXd >", "classmuq_1_1Approximation_1_1SmolyakEstimator.html", [
      [ "muq::Approximation::AdaptiveSmolyakQuadrature", "classmuq_1_1Approximation_1_1AdaptiveSmolyakQuadrature.html", null ]
    ] ],
    [ "muq::Approximation::SmolyakEstimator< std::shared_ptr< PolynomialChaosExpansion > >", "classmuq_1_1Approximation_1_1SmolyakEstimator.html", [
      [ "muq::Approximation::AdaptiveSmolyakPCE", "classmuq_1_1Approximation_1_1AdaptiveSmolyakPCE.html", null ]
    ] ],
    [ "muq::Approximation::SmolyakEstimator< EstimateType >::SmolyTerm", "structmuq_1_1Approximation_1_1SmolyakEstimator_1_1SmolyTerm.html", null ],
    [ "muq::Modeling::SundialsAlgebra", "classmuq_1_1Modeling_1_1SundialsAlgebra.html", null ],
    [ "muq::Modeling::SwigExtract", "classmuq_1_1Modeling_1_1SwigExtract.html", null ],
    [ "cereal::TemporaryBoostAny", "structcereal_1_1TemporaryBoostAny.html", null ],
    [ "cereal::TemporaryBoostAnyConst", "structcereal_1_1TemporaryBoostAnyConst.html", null ],
    [ "muq::SamplingAlgorithms::TransitionKernel", "classmuq_1_1SamplingAlgorithms_1_1TransitionKernel.html", [
      [ "muq::SamplingAlgorithms::DILIKernel", "classmuq_1_1SamplingAlgorithms_1_1DILIKernel.html", null ],
      [ "muq::SamplingAlgorithms::DRKernel", "classmuq_1_1SamplingAlgorithms_1_1DRKernel.html", null ],
      [ "muq::SamplingAlgorithms::DummyKernel", "classmuq_1_1SamplingAlgorithms_1_1DummyKernel.html", null ],
      [ "muq::SamplingAlgorithms::ISKernel", "classmuq_1_1SamplingAlgorithms_1_1ISKernel.html", null ],
      [ "muq::SamplingAlgorithms::MCKernel", "classmuq_1_1SamplingAlgorithms_1_1MCKernel.html", null ],
      [ "muq::SamplingAlgorithms::MHKernel", "classmuq_1_1SamplingAlgorithms_1_1MHKernel.html", [
        [ "muq::SamplingAlgorithms::GMHKernel", "classmuq_1_1SamplingAlgorithms_1_1GMHKernel.html", null ]
      ] ],
      [ "muq::SamplingAlgorithms::MIDummyKernel", "classmuq_1_1SamplingAlgorithms_1_1MIDummyKernel.html", null ],
      [ "muq::SamplingAlgorithms::MIKernel", "classmuq_1_1SamplingAlgorithms_1_1MIKernel.html", null ]
    ] ],
    [ "Cpp2Html.TutorialDocument", "classCpp2Html_1_1TutorialDocument.html", null ],
    [ "muq::Modeling::UpstreamEdgePredicate", "classmuq_1_1Modeling_1_1UpstreamEdgePredicate.html", null ],
    [ "muq::Modeling::UpstreamPredicate", "classmuq_1_1Modeling_1_1UpstreamPredicate.html", null ],
    [ "muq::Utilities::VectorLessThan< ScalarType >", "structmuq_1_1Utilities_1_1VectorLessThan.html", null ],
    [ "muq::Approximation::VectorSlice< VecType >", "classmuq_1_1Approximation_1_1VectorSlice.html", null ],
    [ "muq::Utilities::VectorSlice< VecType, ScalarType >", "classmuq_1_1Utilities_1_1VectorSlice.html", null ],
    [ "muq::Utilities::WaitBar", "classmuq_1_1Utilities_1_1WaitBar.html", null ],
    [ "muq::Modeling::WorkGraph", "classmuq_1_1Modeling_1_1WorkGraph.html", null ],
    [ "muq::Modeling::WorkGraphEdge", "classmuq_1_1Modeling_1_1WorkGraphEdge.html", null ],
    [ "muq::Modeling::WorkGraphNode", "classmuq_1_1Modeling_1_1WorkGraphNode.html", null ],
    [ "muq::Modeling::WorkPiece", "classmuq_1_1Modeling_1_1WorkPiece.html", [
      [ "muq::Approximation::IndexedScalarBasis", "classmuq_1_1Approximation_1_1IndexedScalarBasis.html", [
        [ "muq::Approximation::HermiteFunction", "classmuq_1_1Approximation_1_1HermiteFunction.html", null ],
        [ "muq::Approximation::Monomial", "classmuq_1_1Approximation_1_1Monomial.html", null ],
        [ "muq::Approximation::OrthogonalPolynomial", "classmuq_1_1Approximation_1_1OrthogonalPolynomial.html", [
          [ "muq::Approximation::Jacobi", "classmuq_1_1Approximation_1_1Jacobi.html", null ],
          [ "muq::Approximation::Laguerre", "classmuq_1_1Approximation_1_1Laguerre.html", null ],
          [ "muq::Approximation::Legendre", "classmuq_1_1Approximation_1_1Legendre.html", null ],
          [ "muq::Approximation::PhysicistHermite", "classmuq_1_1Approximation_1_1PhysicistHermite.html", null ],
          [ "muq::Approximation::ProbabilistHermite", "classmuq_1_1Approximation_1_1ProbabilistHermite.html", null ]
        ] ]
      ] ],
      [ "muq::Approximation::Regression", "classmuq_1_1Approximation_1_1Regression.html", null ],
      [ "muq::Modeling::ConstantPiece", "classmuq_1_1Modeling_1_1ConstantPiece.html", null ],
      [ "muq::Modeling::FenicsPiece", "classmuq_1_1Modeling_1_1FenicsPiece.html", null ],
      [ "muq::Modeling::IdentityPiece", "classmuq_1_1Modeling_1_1IdentityPiece.html", null ],
      [ "muq::Modeling::ModPiece", "classmuq_1_1Modeling_1_1ModPiece.html", [
        [ "muq::Approximation::BasisExpansion", "classmuq_1_1Approximation_1_1BasisExpansion.html", [
          [ "muq::Approximation::PolynomialChaosExpansion", "classmuq_1_1Approximation_1_1PolynomialChaosExpansion.html", null ]
        ] ],
        [ "muq::Approximation::LocalRegression", "classmuq_1_1Approximation_1_1LocalRegression.html", null ],
        [ "muq::Approximation::Regression::PoisednessConstraint", "classmuq_1_1Approximation_1_1Regression_1_1PoisednessConstraint.html", null ],
        [ "muq::Modeling::AffineOperator", "classmuq_1_1Modeling_1_1AffineOperator.html", null ],
        [ "muq::Modeling::CombineVectors", "classmuq_1_1Modeling_1_1CombineVectors.html", null ],
        [ "muq::Modeling::ConstantVector", "classmuq_1_1Modeling_1_1ConstantVector.html", null ],
        [ "muq::Modeling::CwiseUnaryOperator< T1, T2, T3 >", "classmuq_1_1Modeling_1_1CwiseUnaryOperator.html", null ],
        [ "muq::Modeling::DensityBase", "classmuq_1_1Modeling_1_1DensityBase.html", null ],
        [ "muq::Modeling::FlannCache", "classmuq_1_1Modeling_1_1FlannCache.html", null ],
        [ "muq::Modeling::GradientPiece", "classmuq_1_1Modeling_1_1GradientPiece.html", null ],
        [ "muq::Modeling::JacobianPiece", "classmuq_1_1Modeling_1_1JacobianPiece.html", null ],
        [ "muq::Modeling::LinearOperator", "classmuq_1_1Modeling_1_1LinearOperator.html", [
          [ "muq::Modeling::BlockDiagonalOperator", "classmuq_1_1Modeling_1_1BlockDiagonalOperator.html", null ],
          [ "muq::Modeling::BlockRowOperator", "classmuq_1_1Modeling_1_1BlockRowOperator.html", null ],
          [ "muq::Modeling::CompanionMatrix", "classmuq_1_1Modeling_1_1CompanionMatrix.html", null ],
          [ "muq::Modeling::ConcatenateOperator", "classmuq_1_1Modeling_1_1ConcatenateOperator.html", null ],
          [ "muq::Modeling::DiagonalOperator", "classmuq_1_1Modeling_1_1DiagonalOperator.html", null ],
          [ "muq::Modeling::GaussianOperator", "classmuq_1_1Modeling_1_1GaussianOperator.html", null ],
          [ "muq::Modeling::GaussNewtonOperator", "classmuq_1_1Modeling_1_1GaussNewtonOperator.html", null ],
          [ "muq::Modeling::HessianOperator", "classmuq_1_1Modeling_1_1HessianOperator.html", null ],
          [ "muq::Modeling::IdentityOperator", "classmuq_1_1Modeling_1_1IdentityOperator.html", null ],
          [ "muq::Modeling::KroneckerProductOperator", "classmuq_1_1Modeling_1_1KroneckerProductOperator.html", null ],
          [ "muq::Modeling::ProductOperator", "classmuq_1_1Modeling_1_1ProductOperator.html", null ],
          [ "muq::Modeling::SliceOperator", "classmuq_1_1Modeling_1_1SliceOperator.html", null ],
          [ "muq::Modeling::SparseLinearOperator", "classmuq_1_1Modeling_1_1SparseLinearOperator.html", null ],
          [ "muq::Modeling::SumOperator", "classmuq_1_1Modeling_1_1SumOperator.html", null ],
          [ "muq::Modeling::ZeroOperator", "classmuq_1_1Modeling_1_1ZeroOperator.html", null ],
          [ "muq::SamplingAlgorithms::AverageHessian", "classmuq_1_1SamplingAlgorithms_1_1AverageHessian.html", null ],
          [ "muq::SamplingAlgorithms::CSProjector", "classmuq_1_1SamplingAlgorithms_1_1CSProjector.html", null ],
          [ "muq::SamplingAlgorithms::LIS2Full", "classmuq_1_1SamplingAlgorithms_1_1LIS2Full.html", null ]
        ] ],
        [ "muq::Modeling::ModGraphPiece", "classmuq_1_1Modeling_1_1ModGraphPiece.html", null ],
        [ "muq::Modeling::MultiLogisticLikelihood", "classmuq_1_1Modeling_1_1MultiLogisticLikelihood.html", null ],
        [ "muq::Modeling::ODE", "classmuq_1_1Modeling_1_1ODE.html", null ],
        [ "muq::Modeling::OneStepCachePiece", "classmuq_1_1Modeling_1_1OneStepCachePiece.html", null ],
        [ "muq::Modeling::ProductPiece", "classmuq_1_1Modeling_1_1ProductPiece.html", null ],
        [ "muq::Modeling::PyModPiece", "classmuq_1_1Modeling_1_1PyModPiece.html", null ],
        [ "muq::Modeling::RandomVariable", "classmuq_1_1Modeling_1_1RandomVariable.html", null ],
        [ "muq::Modeling::ReplicateOperator", "classmuq_1_1Modeling_1_1ReplicateOperator.html", null ],
        [ "muq::Modeling::ScaleVector", "classmuq_1_1Modeling_1_1ScaleVector.html", null ],
        [ "muq::Modeling::SplitVector", "classmuq_1_1Modeling_1_1SplitVector.html", null ],
        [ "muq::Modeling::SumPiece", "classmuq_1_1Modeling_1_1SumPiece.html", null ],
        [ "muq::Optimization::CostFunction", "classmuq_1_1Optimization_1_1CostFunction.html", [
          [ "muq::Approximation::Regression::PoisednessCost", "classmuq_1_1Approximation_1_1Regression_1_1PoisednessCost.html", null ],
          [ "muq::Optimization::ModPieceCostFunction", "classmuq_1_1Optimization_1_1ModPieceCostFunction.html", null ]
        ] ],
        [ "muq::Utilities::LinearOperator", "classmuq_1_1Utilities_1_1LinearOperator.html", null ]
      ] ],
      [ "muq::Modeling::WorkGraphPiece", "classmuq_1_1Modeling_1_1WorkGraphPiece.html", null ],
      [ "muq::Optimization::Optimization", "classmuq_1_1Optimization_1_1Optimization.html", null ],
      [ "muq::Optimization::Optimizer", "classmuq_1_1Optimization_1_1Optimizer.html", [
        [ "muq::Optimization::NLoptOptimizer", "classmuq_1_1Optimization_1_1NLoptOptimizer.html", null ]
      ] ]
    ] ]
];