var dir_0a77aa6bce8c8bb59d26a4a1207bdc36 =
[
    [ "CwiseOperators", "dir_3176bd2a86865df2b1e451ace9a31103.html", "dir_3176bd2a86865df2b1e451ace9a31103" ],
    [ "Distributions", "dir_6e76de88d4e83ecd55e3725fc2531ed0.html", "dir_6e76de88d4e83ecd55e3725fc2531ed0" ],
    [ "Dolfin", "dir_e1b08df74c3867e3307b53ee2c789e56.html", "dir_e1b08df74c3867e3307b53ee2c789e56" ],
    [ "Flann", "dir_18439f4a67c6c0b1074703a486234782.html", "dir_18439f4a67c6c0b1074703a486234782" ],
    [ "LinearAlgebra", "dir_17356ec5b34ef9a4a20a8353d41bb99c.html", "dir_17356ec5b34ef9a4a20a8353d41bb99c" ],
    [ "Python", "dir_09f547f5a59e11a642f33cd766f1f010.html", "dir_09f547f5a59e11a642f33cd766f1f010" ],
    [ "CombineVectors.h", "CombineVectors_8h.html", [
      [ "CombineVectors", "classmuq_1_1Modeling_1_1CombineVectors.html", "classmuq_1_1Modeling_1_1CombineVectors" ]
    ] ],
    [ "ConstantPiece.h", "ConstantPiece_8h.html", [
      [ "ConstantPiece", "classmuq_1_1Modeling_1_1ConstantPiece.html", "classmuq_1_1Modeling_1_1ConstantPiece" ]
    ] ],
    [ "ConstantVector.h", "ConstantVector_8h.html", [
      [ "ConstantVector", "classmuq_1_1Modeling_1_1ConstantVector.html", "classmuq_1_1Modeling_1_1ConstantVector" ]
    ] ],
    [ "GradientPiece.h", "GradientPiece_8h.html", [
      [ "GradientPiece", "classmuq_1_1Modeling_1_1GradientPiece.html", "classmuq_1_1Modeling_1_1GradientPiece" ]
    ] ],
    [ "IdentityPiece.h", "IdentityPiece_8h.html", [
      [ "IdentityPiece", "classmuq_1_1Modeling_1_1IdentityPiece.html", "classmuq_1_1Modeling_1_1IdentityPiece" ]
    ] ],
    [ "JacobianPiece.h", "JacobianPiece_8h.html", null ],
    [ "LinearSDE.h", "LinearSDE_8h.html", [
      [ "LinearSDE", "classmuq_1_1Modeling_1_1LinearSDE.html", "classmuq_1_1Modeling_1_1LinearSDE" ]
    ] ],
    [ "ModGraphPiece.h", "ModGraphPiece_8h.html", "ModGraphPiece_8h" ],
    [ "ModPiece.h", "ModPiece_8h.html", null ],
    [ "MultiLogisticLikelihood.h", "MultiLogisticLikelihood_8h.html", [
      [ "MultiLogisticLikelihood", "classmuq_1_1Modeling_1_1MultiLogisticLikelihood.html", "classmuq_1_1Modeling_1_1MultiLogisticLikelihood" ]
    ] ],
    [ "NodeNameFinder.h", "NodeNameFinder_8h.html", "NodeNameFinder_8h" ],
    [ "ODE.h", "ODE_8h.html", [
      [ "ODE", "classmuq_1_1Modeling_1_1ODE.html", "classmuq_1_1Modeling_1_1ODE" ],
      [ "NonlinearSolverOptions", "structmuq_1_1Modeling_1_1ODE_1_1NonlinearSolverOptions.html", "structmuq_1_1Modeling_1_1ODE_1_1NonlinearSolverOptions" ],
      [ "LinearSolverOptions", "structmuq_1_1Modeling_1_1ODE_1_1LinearSolverOptions.html", "structmuq_1_1Modeling_1_1ODE_1_1LinearSolverOptions" ],
      [ "IntegratorOptions", "structmuq_1_1Modeling_1_1ODE_1_1IntegratorOptions.html", "structmuq_1_1Modeling_1_1ODE_1_1IntegratorOptions" ],
      [ "Interface", "structmuq_1_1Modeling_1_1ODE_1_1Interface.html", "structmuq_1_1Modeling_1_1ODE_1_1Interface" ]
    ] ],
    [ "ODEData.h", "ODEData_8h.html", [
      [ "ODEData", "classmuq_1_1Modeling_1_1ODEData.html", "classmuq_1_1Modeling_1_1ODEData" ]
    ] ],
    [ "OneStepCachePiece.h", "OneStepCachePiece_8h.html", [
      [ "OneStepCachePiece", "classmuq_1_1Modeling_1_1OneStepCachePiece.html", "classmuq_1_1Modeling_1_1OneStepCachePiece" ]
    ] ],
    [ "ProductPiece.h", "ProductPiece_8h.html", [
      [ "ProductPiece", "classmuq_1_1Modeling_1_1ProductPiece.html", "classmuq_1_1Modeling_1_1ProductPiece" ]
    ] ],
    [ "PyModPiece.h", "PyModPiece_8h.html", [
      [ "PyModPiece", "classmuq_1_1Modeling_1_1PyModPiece.html", "classmuq_1_1Modeling_1_1PyModPiece" ]
    ] ],
    [ "ReplicateOperator.h", "ReplicateOperator_8h.html", [
      [ "ReplicateOperator", "classmuq_1_1Modeling_1_1ReplicateOperator.html", "classmuq_1_1Modeling_1_1ReplicateOperator" ]
    ] ],
    [ "RootfindingIVP.h", "RootfindingIVP_8h.html", [
      [ "RootfindingIVP", "classmuq_1_1Modeling_1_1RootfindingIVP.html", "classmuq_1_1Modeling_1_1RootfindingIVP" ]
    ] ],
    [ "ScaleVector.h", "ScaleVector_8h.html", [
      [ "ScaleVector", "classmuq_1_1Modeling_1_1ScaleVector.html", "classmuq_1_1Modeling_1_1ScaleVector" ]
    ] ],
    [ "SplitVector.h", "SplitVector_8h.html", [
      [ "SplitVector", "classmuq_1_1Modeling_1_1SplitVector.html", "classmuq_1_1Modeling_1_1SplitVector" ]
    ] ],
    [ "SumPiece.h", "SumPiece_8h.html", null ],
    [ "WorkGraph.h", "WorkGraph_8h.html", [
      [ "WorkGraph", "classmuq_1_1Modeling_1_1WorkGraph.html", "classmuq_1_1Modeling_1_1WorkGraph" ]
    ] ],
    [ "WorkGraphEdge.h", "WorkGraphEdge_8h.html", [
      [ "WorkGraphEdge", "classmuq_1_1Modeling_1_1WorkGraphEdge.html", "classmuq_1_1Modeling_1_1WorkGraphEdge" ]
    ] ],
    [ "WorkGraphNode.h", "WorkGraphNode_8h.html", [
      [ "WorkGraphNode", "classmuq_1_1Modeling_1_1WorkGraphNode.html", "classmuq_1_1Modeling_1_1WorkGraphNode" ]
    ] ],
    [ "WorkGraphPiece.h", "WorkGraphPiece_8h.html", [
      [ "UpstreamPredicate", "classmuq_1_1Modeling_1_1UpstreamPredicate.html", "classmuq_1_1Modeling_1_1UpstreamPredicate" ],
      [ "UpstreamEdgePredicate", "classmuq_1_1Modeling_1_1UpstreamEdgePredicate.html", "classmuq_1_1Modeling_1_1UpstreamEdgePredicate" ],
      [ "DependentPredicate", "classmuq_1_1Modeling_1_1DependentPredicate.html", "classmuq_1_1Modeling_1_1DependentPredicate" ],
      [ "DependentEdgePredicate", "classmuq_1_1Modeling_1_1DependentEdgePredicate.html", "classmuq_1_1Modeling_1_1DependentEdgePredicate" ],
      [ "WorkGraphPiece", "classmuq_1_1Modeling_1_1WorkGraphPiece.html", "classmuq_1_1Modeling_1_1WorkGraphPiece" ]
    ] ],
    [ "WorkPiece.h", "WorkPiece_8h.html", "WorkPiece_8h" ]
];